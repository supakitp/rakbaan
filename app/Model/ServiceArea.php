<?php
class ServiceArea extends AppModel
{
	public $useDbConfig = 'rakbaan';

	public $virtualFields = array(
		'full_name' => 'REPLACE(CONCAT(name, " (", street,")"),"()","")',
	);
}
