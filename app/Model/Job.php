<?php
class Job extends AppModel
{
    public $useDbConfig = 'rakbaan';

    public $belongsTo = array(
        'JobStatus',
        'User' => array(
            'foreignKey' => 'engineer_id'
        )
    );
}
