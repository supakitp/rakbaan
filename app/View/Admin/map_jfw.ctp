<div class="card-body">
    <div class="col-sm-12">
        <div id='myMap' style='height: 100%;'></div>
        <script type='text/javascript'>
            function loadMapScenario() {
                var map = new Microsoft.Maps.Map(document.getElementById('myMap'), {
                    credentials: 'Aoy02u0QzfeL-v1a2iiozc05VWoLxR7H0gk9gmu16GbaaAOy6eHFYSCXGWK51C_G'
                });

                <?php

                $lat = "18.786879830434497";
                $lng = "98.97934913635254";

                if (isset($job)) {
                    if ($job['JobFromWeb']['latitude'] != "" && $job['JobFromWeb']['longitude'] != "") {
                ?>
                        try {
                            var location = new Microsoft.Maps.Location(<?php echo ($job['JobFromWeb']['latitude']); ?>, <?php echo ($job['JobFromWeb']['longitude']); ?>);
                            var pin = new Microsoft.Maps.Pushpin(location, {
                                text: "⌂",
                                title: "<?php echo 'บ้านลูกค้า'; ?>"
                            });
                            map.entities.push(pin);
                        } catch (err) {}
                <?php
                        $lat = $job['JobFromWeb']['latitude'];
                        $lng = $job['JobFromWeb']['longitude'];
                    }
                }
                ?>

                map.setView({
                    //center: new Microsoft.Maps.Location(map.getCenter()),
                    center: new Microsoft.Maps.Location(<?= $lat ?>, <?= $lng ?>),
                    zoom: 11
                });

            }
        </script>
        <script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?callback=loadMapScenario' async defer></script>

    </div>
</div>