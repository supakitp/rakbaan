<div class="container">
    <?php
    echo $this->Form->create('ItemType', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'));
    ?>
    <div class="card mb-3">
        <div class="card-header">
            <center>
                <h2>จัดการหมู่บ้าน</h2>
            </center>

            <div class="form-row">
                <?php
                if (!empty($Areas)) {
                    foreach ($Areas as $item) { ?>
                        <div class="col-sm-4 mt-2">
                            <?php
                            echo $this->Form->input(
                                'areaName' . $item['ServiceArea']['id'],
                                array(
                                    'id' => 'areaName' . $item['ServiceArea']['id'],
                                    'type' => 'text', 'label' => false,
                                    'class' => 'form-control',
                                    'value' => $item['ServiceArea']['name']
                                )
                            );
                            ?>
                        </div>
                        <div class="col-sm-4 mt-2">
                            <?php
                            echo $this->Form->input(
                                'streetName' . $item['ServiceArea']['id'],
                                array(
                                    'id' => 'streetName' . $item['ServiceArea']['id'],
                                    'type' => 'text', 'label' => false,
                                    'class' => 'form-control',
                                    'value' => $item['ServiceArea']['street']
                                )
                            );
                            ?>
                        </div>

                        <div class="col-sm-2 mt-2">
                            <a class="btn" onclick="edit(<?= $item['ServiceArea']['id'] ?>);" id="edit<?= $item['ServiceArea']['id'] ?>">
                                <i class="fas fa-wrench text-warning"></i>
                            </a>
                            <a class="btn" onclick="removeContent(<?= $item['ServiceArea']['id'] ?>);" id="removeContent<?= $item['ServiceArea']['id'] ?>">
                                <i class="fas fa-trash text-danger"></i>
                            </a>
                        </div>
                <? }
                } ?>
                <div class="col-sm-12 mt-2">
                    <a class="btn" onclick="addItem()">
                        <i class="fas fa-plus fa-2x text-success"></i>
                    </a>

                </div>
                <div id="addRow" class="col-sm-12"></div>

            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
</div>

<script type="text/javascript">
    $(function() {


    });
    var row = 1;

    function addItem() {
        var item = '';
        item += '<div class="mt-2 row" id="itemType_id' + row + '">';
        item += '<input class="form-control col-sm-4 ml-2" name="itemArea' + row + '" id="itemArea' + row + '" ">';
        item += '<input class="form-control col-sm-4 ml-2" name="itemStreet' + row + '" id="itemStreet' + row + '" ">';
        item += ' <a class="btn" onclick="confirmRow(' + row + ')" id="confirm' + row + '">';
        item += '<i class="fas fa-check text-success"></i>';
        item += '</a>';
        item += ' <a class="btn" onclick="removeRow(' + row + ')" id="remove' + row + '">';
        item += '<i class="fas fa-minus text-danger"></i>';
        item += '</a>';
        item += '</div>';

        $("#addRow").append(item);
        row++;
    }

    function confirmRow(i) {
        var valArea = $("input[name=itemArea" + i + "]").val();
        var valStreet = $("input[name=itemStreet" + i + "]").val();
        if (valArea != '' && valStreet != '') {
            $.ajax({
                url: "<?php echo $this->Html->url(array('controller' => 'Admin', 'action' => 'add_content')); ?>",
                type: "post",
                data: {
                    valueStreet: valStreet,
                    valueArea: valArea
                },
                success: function(response) {
                    var result = response.result;
                    console.log(result);
                    if (result.msg == true) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'บันทึกสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $("#itemArea" + i).prop("disabled", true);
                        $("#itemStreet" + i).prop("disabled", true);
                        $("#confirm" + i).hide();
                        $("#remove" + i).hide();
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'พบข้อผิดพลาด',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        } else {
            Swal.fire({
                icon: 'error',
                title: 'กรุณากรอกข้อมูล',
                showConfirmButton: false,
                timer: 1500
            })
        }
    }

    function removeRow(i) {
        // let i = row - 1;
        $('#itemType_id' + i).remove();
    }

    function edit(id) {
        var valArea = $("#areaName" + id).val();
        var valStreet = $("#streetName" + id).val();
        Swal.fire({
            title: 'กดยืนยันเพื่อแก้ไข?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "<?php echo $this->Html->url(array('controller' => 'Admin', 'action' => 'add_content')); ?>",
                    type: "post",
                    data: {
                        id: id,
                        valueArea: valArea,
                        valueStreet: valStreet
                    },
                    success: function(response) {
                        var result = response.result;
                        var energie = '';
                        if (result.msg == true) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'บันทึกสำเร็จ',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'พบข้อผิดพลาด',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        })

    }

    function removeContent(id) {
        Swal.fire({
            title: 'กดยืนยันเพื่อลบ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "<?php echo $this->Html->url(array('controller' => 'Admin', 'action' => 'delete_content')); ?>",
                    type: "post",
                    data: {
                        id: id,
                    },
                    success: function(response) {
                        var result = response.result;
                        var energie = '';
                        if (result.msg == true) {
                            Swal.fire({
                                icon: 'success',
                                title: 'ลบสำเร็จ',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            $("#areaName" + id).hide();
                            $("#streetName" + id).hide();
                            $("#edit" + id).hide();
                            $("#removeContent" + id).hide();
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'พบข้อผิดพลาด',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        })
    }
</script>