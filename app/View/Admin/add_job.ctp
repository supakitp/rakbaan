<script>
    //table manage
    function addRows(tblId, colId) {
        var table = document.getElementById(tblId);
        var rowCount = table.rows.length;
        var cellCount = table.rows[0].cells.length;
        var row = table.insertRow(rowCount);
        for (var i = 0; i <= cellCount; i++) {
            var cell = 'cell' + i;
            cell = row.insertCell(i);
            var copycel = document.getElementById(colId + i).innerHTML;
            cell.innerHTML = copycel;
        }
    }

    function deleteRows(tblId) {
        var table = document.getElementById(tblId);
        var rowCount = table.rows.length;
        if (rowCount > 1) {
            var row = table.deleteRow(rowCount - 1);
            rowCount--;
        }
    }
</script>

<div class="container">
    <div class="card">
        <div class="card-body">
            <?php

            //create form
            echo $this->Form->create('Job', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'));
            $jobDate = $defaultDate;
            $startTime = $defaultTime;
            $endTime = $defaultTime;

            //set id = 0 for cake insert
            if (isset($this->request->data['Job']) && $this->request->data['Job'] != null) {
                echo '<center><h1 class="mt-4">งานซ่อมเลขที่: ' . $this->request->data['Job']['job_num'] . '</h1></center>';
                echo $this->Form->input('id', array('type' => 'hidden', 'value' => $this->request->data['Job']['id']));
                echo $this->Form->input('active', array('type' => 'hidden', 'value' => $this->request->data['Job']['active']));
                echo $this->Form->input('latitude', array('id' => 'latitude', 'type' => 'hidden', 'value' => $this->request->data['Job']['latitude']));
                echo $this->Form->input('longitude', array('id' => 'longitude', 'type' => 'hidden', 'value' => $this->request->data['Job']['longitude']));
                $jobDate =  $this->request->data['Job']['job_date'];
                $startTime = $this->request->data['Job']['start_time'];
                $endTime = $this->request->data['Job']['end_time'];
            } else {
                echo '<center><h1 class="mt-4">งานซ่อม</h1></center>';
                echo $this->Form->input('id', array('type' => 'hidden', 'value' =>  0));
                echo $this->Form->input('active', array('type' => 'hidden', 'value' => 1));
                echo $this->Form->input('latitude', array('id' => 'latitude', 'type' => 'hidden', 'value' => ''));
                echo $this->Form->input('longitude', array('id' => 'longitude', 'type' => 'hidden', 'value' => ''));
            }

            //แปลงนาที
            $startTimeH = substr($startTime, 0, 2);
            $startTimeM = substr($startTime, 3, 2);
            $endTimeH = substr($endTime, 0, 2);
            $endTimeM = substr($endTime, 3, 2);

            //set checkRequired 
            ?>

            <div class="form-group">
                <label class="col-sm-12">
                    ประเภท:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('item_type_id', array(
                        'options' => $itemTypes,
                        'class' => 'form-control',
                        'label' => false
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">
                    วันที่:
                </label>
                <div class="col-sm-12">
                    <?php echo $this->Form->date('job_date', array(
                        'type' => 'text',
                        'class' => 'datepickerthai form-control',
                        'autocomplete' => 'off',
                        'data-provide' => 'datepicker',
                        'data-date-language' => 'th-th',
                        'value' => $jobDate,
                        'pattern' => '\d{1,2}/\d{1,2}/\d{4}'
                    )); ?>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="col-sm-12">
                            ตั้งแต่:
                        </label>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <?php
                                    echo $this->Form->input('start_time_h', array(
                                        'label' => false, 'type' => 'select', 'class' => 'form-control',
                                        'options' => array_combine(range(0, 23), range(0, 23)), 'value' => $startTimeH,
                                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                                    ));
                                    ?>
                                </div>
                                <div class="col-sm-1">
                                    :
                                </div>
                                <div class="col-sm-5">
                                    <?php
                                    echo $this->Form->input('start_time_m', array(
                                        'label' => false, 'type' => 'select', 'class' => 'form-control',
                                        'options' => array_combine(range(0, 60), range(0, 60)), 'value' => $startTimeM,
                                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-12">
                            ถึง:
                        </label>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <?php
                                    echo $this->Form->input('end_time_h', array(
                                        'label' => false, 'type' => 'select', 'class' => 'form-control',
                                        'options' => array_combine(range(0, 23), range(0, 23)), 'value' => $endTimeH,
                                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                                    ));
                                    ?>
                                </div>
                                <div class="col-sm-1">
                                    :
                                </div>
                                <div class="col-sm-5">
                                    <?php
                                    echo $this->Form->input('end_time_m', array(
                                        'label' => false, 'type' => 'select', 'class' => 'form-control',
                                        'options' => array_combine(range(0, 60), range(0, 60)), 'value' => $endTimeM,
                                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="col-sm-12">
                            <font color="red">* </font>ชื่อลูกค้า:
                        </label>
                        <div class="col-sm-12">
                            <?php
                            echo $this->Form->input('customer_name', array(
                                'label' => false, 'class' => 'form-control', 'required' => true,
                                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-sm-12">
                            <font color="red">* </font>นามสกุล:
                        </label>
                        <div class="col-sm-12">
                            <?php
                            echo $this->Form->input('customer_last_name', array(
                                'label' => false, 'class' => 'form-control', 'required' => true,
                                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    <font color="red">* </font>ที่อยู่:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('customer_address', array(
                        'label' => false, 'class' => 'form-control', 'required' => true,
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    <font color="red">* </font>โทร:
                    <button class="btn btn-info" type="button" onclick="phone_search();"><i class="fas fa-search"></i></button>
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('customer_phone', array(
                        'id' => 'customer_phone',
                        'label' => false, 'class' => 'form-control', 'required' => true, 'type' => 'text',
                        'onKeyDown' => 'if(this.value.length==20 && event.keyCode!=8) return false;',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                    <script>
                        $("#customer_phone").keydown(function() {
                            $("#customer_phone").val(this.value.match(/[0-9]*/));
                        });
                    </script>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    Facebook:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('customer_fb', array(
                        'label' => false, 'class' => 'form-control',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    Line:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('customer_line', array(
                        'label' => false, 'class' => 'form-control',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div id='printoutPanel'></div>
                <label class="col-sm-12 control-label">แผนที่:</label>
                <div class="col-sm-12">
                    <div id='myMap' style='width: 100%; height: 400px;'></div>
                    <script type='text/javascript'>
                        function loadMapScenario() {
                            var map = new Microsoft.Maps.Map(document.getElementById('myMap'), {
                                credentials: 'Aoy02u0QzfeL-v1a2iiozc05VWoLxR7H0gk9gmu16GbaaAOy6eHFYSCXGWK51C_G'
                            });
                            <?php
                            if (isset($this->request->data['Job']) && $this->request->data['Job']['latitude'] != null && $this->request->data['Job']['longitude'] != null) {
                            ?>
                                var location = new Microsoft.Maps.Location(<?php echo ($this->request->data['Job']['latitude']); ?>, <?php echo ($this->request->data['Job']['longitude']); ?>);
                                var pin = new Microsoft.Maps.Pushpin(location, {
                                    'draggable': false
                                });
                                map.entities.push(pin)

                                map.setView({
                                    center: new Microsoft.Maps.Location(<?php echo ($this->request->data['Job']['latitude']); ?>, <?php echo ($this->request->data['Job']['longitude']); ?>),
                                    zoom: 15
                                });
                            <?php
                            } else {
                            ?>
                                map.setView({
                                    center: new Microsoft.Maps.Location('18.786879830434497', '98.97934913635254'),
                                    zoom: 11
                                });
                            <?php
                            }
                            ?>

                            Microsoft.Maps.Events.addHandler(map, 'click', function(e) {
                                handleArgs('mapClick', e);
                            });

                            /*var a =  pushpin.getLocation();
                            document.getElementById('printoutPanel').innerHTML = pushpin.getLocation(); */
                            function handleArgs(id, e) {
                                for (var i = map.entities.getLength() - 1; i >= 0; i--) {
                                    var pushpin = map.entities.get(i);
                                    if (pushpin instanceof Microsoft.Maps.Pushpin) {
                                        map.entities.removeAt(i);
                                    }
                                }

                                var point = new Microsoft.Maps.Point(e.getX(), e.getY());
                                var locTemp = e.target.tryPixelToLocation(point);
                                var location = new Microsoft.Maps.Location(locTemp.latitude, locTemp.longitude);
                                var pin = new Microsoft.Maps.Pushpin(location, {
                                    'draggable': false
                                });
                                map.entities.push(pin);

                                document.getElementById('latitude').value = locTemp.latitude;
                                document.getElementById('longitude').value = locTemp.longitude;
                            };

                        }
                    </script>
                    <script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?callback=loadMapScenario' async defer></script>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    รายละเอียด:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('detail', array(
                        'label' => false, 'class' => 'form-control', 'type' => 'textarea',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="card">
                        <label class="col-sm-12">
                            ค่าใช้จ่าย:
                        </label>
                        <?php
                        $checkRequired = false;
                        if (isset($jobItems) && count($jobItems) > 0) {
                        ?>
                            <div class="row">
                                <label class="col-sm-5">

                                </label>
                                <label class="col-sm-3">
                                    ราคา/หน่วย
                                </label>
                                <label class="col-sm-2">
                                    จำนวน
                                </label>
                            </div>
                            <?php
                            //$checkRequired = false;
                            //pr($misEducationals);
                            foreach ($jobItems as $jobItem) {
                            ?>
                                <div class="form-group">
                                    <div class="row">
                                        <?php echo $this->Form->input('JobItem][id][]', array(
                                            'value' => $jobItem['JobItem']['id'],
                                            'type' => 'hidden', 'label' => false, 'class' => 'form-control'
                                        )); ?>
                                        <div class="col-sm-5"><?php echo $this->Form->input(
                                                                    'JobItem][item_other][]',
                                                                    array(
                                                                        'value' => $jobItem['JobItem']['item_other'],
                                                                        'label' => false,
                                                                        'class' => 'form-control',
                                                                        'required' => 'true'
                                                                    )
                                                                );
                                                                ?>
                                        </div>
                                        <div class="col-sm-3"><?php echo $this->Form->input(
                                                                    'JobItem][unit_price][]',
                                                                    array(
                                                                        'type' => 'number',
                                                                        'value' => $jobItem['JobItem']['unit_price'],
                                                                        'label' => false,
                                                                        'class' => 'form-control',
                                                                        'required' => 'true',
                                                                        'onClick' => 'this.select();'
                                                                    )
                                                                );
                                                                ?>
                                        </div>
                                        <div class="col-sm-2"><?php echo $this->Form->input(
                                                                    'JobItem][qty][]',
                                                                    array(
                                                                        'type' => 'number',
                                                                        'value' => $jobItem['JobItem']['qty'],
                                                                        'label' => false,
                                                                        'class' => 'form-control',
                                                                        'required' => 'true',
                                                                        'onClick' => 'this.select();'
                                                                    )
                                                                );
                                                                ?>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="<?php echo $this->Html->url(array(
                                                            'action' => 'deleteJobItem', $jobItem['JobItem']['id']
                                                        )); ?>" class="btn btn-danger" onclick="return confirm_click();">
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                        <?php
                            }
                        }
                        ?>

                        <hr />
                        <table class="table table-borderless" id="tblItem">
                            <tr>
                                <td id="colItem0">
                                    <?php echo $this->Form->input('JobItem][id][]', array(
                                        'value' => 0,
                                        'type' => 'hidden', 'label' => false, 'class' => 'form-control'
                                    )); ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="col-sm-12">
                                                รายการ
                                            </label>
                                            <?php
                                            echo $this->Form->input('JobItem][item_other][]', array(
                                                'label' => false, 'class' => 'form-control', 'required' => $checkRequired,
                                                'error' => array(
                                                    'attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
                                                )
                                            ));
                                            ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-12">
                                                ราคา/หน่วย
                                            </label>
                                            <?php
                                            echo $this->Form->input('JobItem][unit_price][]', array(
                                                'value' => '100', 'type' => 'number',
                                                'label' => false, 'class' => 'form-control', 'required' => $checkRequired,
                                                'onClick' => 'this.select();',
                                                'error' => array(
                                                    'attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
                                                )
                                            ));
                                            ?>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="col-sm-12">
                                                จำนวน
                                            </label>
                                            <?php
                                            echo $this->Form->input('JobItem][qty][]', array(
                                                'value' => '1', 'type' => 'number',
                                                'label' => false, 'class' => 'form-control', 'required' => $checkRequired,
                                                'onClick' => 'this.select();',
                                                'error' => array(
                                                    'attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
                                                )
                                            ));
                                            ?>
                                        </div>
                                        <!-- <div class="col-sm-3">
                                            <label class="col-sm-12">
                                                รวม
                                            </label>
                                            <?php
                                            echo $this->Form->input('JobItem][price][]', array(
                                                'value' => '100', 'type' => 'number',
                                                'label' => false, 'class' => 'form-control', 'required' => $checkRequired,
                                                'onClick' => 'this.select();',
                                                'error' => array(
                                                    'attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger')
                                                )
                                            ));
                                            ?>
                                        </div> -->
                                    </div>
                                </td>
                            </tr>
                        </table>

                        <hr />
                        <table class="table table-borderless">
                            <tr>
                                <td>
                                    <center>
                                        <button type="button" class="btn btn-info" onclick="addRows('tblItem','colItem')"> <i class=" fas fa-plus"></i> </button>
                                        <button type="button" class="btn btn-info" onclick="deleteRows('tblItem')"><i class="fas fa-minus"></i> </button>
                                    </center>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">
                    ผู้รับผิดชอบ:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('engineer_id', array(
                        'options' => $engineers,
                        'class' => 'form-control',
                        'label' => false
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">
                    สถานะ:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('job_status_id', array(
                        'options' => $jobStatuses,
                        'class' => 'form-control',
                        'label' => false
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">
                    หมายเหตุ:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('remark', array(
                        'label' => false, 'class' => 'form-control', 'type' => 'textarea',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">รูป:<font color="red"> (ได้สูงสุด 10 รูป)</font></label>
                <?php
                if (isset($jobImgs) && count($jobImgs) > 0)
                    foreach ($jobImgs as $img) {
                ?>
                    <div class="col-sm-4">
                        <div class="card">
                            <a href="<?= $this->webroot . 'img/job_img/' . $img['JobImg']['img'] ?>" target="_blank">
                                <img src="<?php echo $this->webroot . 'img/job_img/' . $img['JobImg']['img']; ?>" style="width:100%;" />
                            </a>
                            <a href="<?php echo $this->Html->url(array('action' => 'deleteImg', $img['JobImg']['id'])); ?>" class="btn btn-danger" onclick="return confirm_click();">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </div>
                    </div>
                    <br />
                <?php
                    }
                ?>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input(
                        'photo_tmp.',
                        array(
                            'type' => 'file', 'label' => false, 'multiple' => 'true', 'accept' => 'image/*'
                        )
                    );
                    ?>
                </div>
            </div>
        </div>

    </div>

    <div class="card-body">
        <div class="col-sm-12">
            <div class="form-group">
                <a href="<?php echo $this->Html->url(array('action' => 'work_management')); ?>" class="btn btn-danger btn-lg"><i class="fas fa-arrow-left"></i>
                    ย้อนกลับ</a>
                <button type="submit" class="btn btn-success btn-lg" style="float: right;"><i class="far fa-save"></i> บันทึก</button>
                <!-- <button type="submit" class="btn btn-success" style="float: right;">ต่อไป <i class="fas fa-arrow-right"></i></button> -->

            </div>
        </div>
    </div>

    <?php echo $this->Form->end(); ?>
</div>

<script>
    function confirm_click() {
        if (confirm(' กรุณายืนยันการทำรายการอีกครั้ง !!! ')) {
            return true;
        } else {
            return false;
        }
    }

    function phone_search() {
        var phone = document.getElementById('customer_phone').value;
        window.open('<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'customer_search')) ?>/' + phone);
    }
</script>