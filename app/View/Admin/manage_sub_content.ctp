<br>
<div class="container" style="background-color: white">
	<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<h3 class="panel-title">
				<center><div class="font_panel2">ประเภทงานย่อย</div></center>
			</h3>
	</div>


		<div class="col-md-12">
			<br>
			<div class="col-md-12 text-right">
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มประเภทงานย่อย</button>

				  <!-- Modal -->
				  <div class="modal fade" id="myModal" role="dialog">
				    <div class="modal-dialog modal-lg">
				    <form method="post" action="<?php echo $this->Html->url(array('action' => 'manage_sub_content',$item_type_id)); ?>">
				      <div class="modal-content">
				        <div class="modal-header">
				          
				          <h4 class="modal-title">เพิ่มประเภทงานย่อย</h4>
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				        </div>
				        <div class="modal-body">
				          <div class="row">

				          	<div class="col-md-12" style="text-align: left;">
				          		<label><font style="color: red">* </font>ชื่อประเภทงานย่อย</label>
				          		<input type="textbox" class="form-control" name="data[SubItemType][name]" placeholder="กรอกประเภทงานย่อย" required="true">
				          	</div>
				          	<input type="hidden" class="form-control" name="data[SubItemType][item_type_id]" value="<?=$item_type_id;?>" required="true">
				          </div>
				        </div>
				        <div class="modal-footer">
				        	<button type="submit" name="data[SubItemType][button]" value="1" class="btn btn-success" >บันทึก</button>
				          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
				        </div>
				    </div>
				     </form>
				    </div>
				  </div>
				</div>
			</div>
		</div>

		<div class="container">
			<br>
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th width="5%">ลำดับ</th>
						<th>ประเภทงานย่อย</th>
						<th width="15%"></th>
					</tr>
				</thead>
				<tbody>
					<?
					//debug($strategics);
					$i = 1;
					foreach ($subItemTypes as $subItemType) 
					{
					?>
					<tr>
						<td><?=$i;?></td>
						<td><a href="<?php echo $this->Html->url(array('action' => 'manage_item_content',$subItemType['SubItemType']['id'])); ?>"><?=$subItemType['SubItemType']['name'];?></a></td>
						<td>
							<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal<?=$subItemType['SubItemType']['id'];?>"><i class="fas fa-edit"></i></button>&nbsp;<a href="<?php echo $this->Html->url(array('action' => 'delete_sub_item_type',$subItemType['SubItemType']['id'],$subItemType['SubItemType']['item_type_id'])); ?>" class="btn btn-danger" OnClick="return chkdel();"><i class="far fa-trash-alt"></i></a></td>
					</tr>

					<!-- Modal -->
				  <div class="modal fade" id="myModal<?=$subItemType['SubItemType']['id'];?>" role="dialog">
				    <div class="modal-dialog modal-lg">
				    <form method="post" action="<?php echo $this->Html->url(array('action' => 'manage_sub_content',$item_type_id)); ?>">
				      <div class="modal-content">
				        <div class="modal-header">
				          
				          <h4 class="modal-title">แก้ไข </h4>
				          <button type="button" class="close" data-dismiss="modal">&times;</button>

				        </div>
				        <div class="modal-body">
				        

				          	<div class="col-md-12">
				          		<label><font style="color: red">* </font>ชื่อประเภทงานย่อย</label>
				          		<input type="textbox" class="form-control" name="data[SubItemType][name]" placeholder="กรอกประเภทงานย่อย" value="<?=$subItemType['SubItemType']['name'];?>" required="true">
				          	</div>


				          	<div class="col-md-12">
				          		<input type="hidden" class="form-control" name="data[SubItemType][id]" value="<?=$subItemType['SubItemType']['id'];?>" required="true">
				          	</div>
				          	
				         
				        </div>
				        <div class="modal-footer">
				        	<button type="submit" name="data[SubItemType][button]" value="2" class="btn btn-success" >บันทึก</button>
				          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
				        </div>
				    </div>
				     </form>
				    </div>
				  </div>
					<?
					$i++;
					}
					?>
				</tbody>
			</table>
		</div>

	</div> <!-- panel-body -->
	
</div>
<br>

<script language="JavaScript">
function chkdel(){if(confirm('  กรุณายืนยันการทำรายการอีกครั้ง !!!  ')){
    return true;
  }else{
    return false;
  }
  }
</script>