<div class="container">
    <div class="card">
        <div class="card-body">

            <div class="card-body">
                <center>
                    <h1 class="mt-4">จัดการงาน</h1>
                    <h4> <a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'index')); ?>">
                            <i class="fas fa-calendar-alt"></i> <?php echo $dateShow; ?></a></h4>
                </center>
                <div class="form-group">
                    <?php
                    echo $this->Form->create('status', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'));
                    ?>
                    <div class="card-header row">
                        <div class="col-sm-6">
                            <?php
                            echo $this->Form->input('job_status_id', array(
                                'options' => $jobStatuses,
                                'class' => 'form-control',
                                'empty' => 'แสดงทั้งหมด',
                                'onChange' => 'submit();',
                                'label' => false
                            ));
                            ?>
                        </div>
                        <div class="col-sm-6">
                            <a href="<?php echo $this->Html->url(array("controller" => "admin", "action" => "add_job")); ?>" class="btn btn-outline-success" style="float: right;">
                                <i class=" fas fa-plus-square"></i> เพิ่ม
                            </a>
                            <a style="float: right;">
                                &nbsp;
                            </a>
                            <a href="<?php echo $this->Html->url(array("controller" => "admin", "action" => "work_map")); ?>" class="btn btn-outline-info" target="_blank" style="float: right;">
                                <i class="fas fa-map-marked-alt"></i>
                            </a>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>

            <!-- <nav>
                <div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        ใหม่
                    </a>
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        กำลังดำเนินการ
                    </a>
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        เสร็จแล้ว
                    </a>
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        ยกเลิก
                    </a>
                </div>
            </nav> -->

            <?php
            foreach ($jobs as $job) {
                if ($job['Job']['job_status_id'] == 1) {
                    $sColor = 'black';
                } elseif ($job['Job']['job_status_id'] == 2) {
                    $sColor = 'red';
                } elseif ($job['Job']['job_status_id'] == 3) {
                    $sColor = 'green';
                } elseif ($job['Job']['job_status_id'] == 4) {
                    $sColor = 'lightgray';
                }
            ?>
                <div class="card mb-4">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6" style="color:<?= $sColor ?>;">
                                <b>
                                    <i class="fas fa-clock"></i> <?php echo  date('H:i', strtotime($job['Job']['start_time'])) . ' - ' . date('H:i', strtotime($job['Job']['end_time'])) .
                                                                        ' สถานะ: ' . $job['JobStatus']['name'];  ?>
                                </b>
                            </div>
                            <!-- <div class="col-sm-6">
                                <a href="<?php echo $this->Html->url(array('action' => 'deleteJob', $job['Job']['id'], $date)); ?>" class="btn btn-danger" onclick="return confirm_click();" style="float: right;">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </div> -->
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <h5><b><?php echo 'งานซ่อมเลขที่ ' . $job['Job']['job_num']; ?></b></h5>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><b>ชื่อลูกค้า:</b> <?php echo  $job['Job']['customer_name'] . ' ' . $job['Job']['customer_last_name']; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><b>ที่อยู่:</b> <?php echo  $job['Job']['customer_address']; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><b>โทร:</b>
                                <?php echo  '<a href="' . $this->Html->url(array('controller' => 'admin', 'action' => 'customer_search', $job['Job']['customer_phone'])) . '" target="_blank">' .
                                    $job['Job']['customer_phone'] . '</a>'; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><b>รายละเอียด:</b> <?php echo  $job['Job']['detail']; ?></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">
                                <b>ค่าใช้จ่าย:</b> <?php echo  number_format($job['Job']['total'], 2) . ' บาท' ?>
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">
                                <b>ผู้รับผิดชอบ:</b> <?php echo  $job['User']['name']; ?>
                                <?php
                                if ($job['Job']['check_in_user'] != null) {
                                    echo '<p style="color:green;"><i class="fas fa-check"></i>Check-in ' . date('H:i', strtotime($job['Job']['check_in'])) . '<br/>';
                                }
                                if ($job['Job']['check_out_user'] != null) {
                                    echo '<i class="fas fa-check"></i>Check-out ' . date('H:i', strtotime($job['Job']['check_out'])) . '</p>';
                                }
                                ?>
                            </label>
                        </div>
                        <div class="col-sm-12">
                            <a type="button" href="<?php echo $this->Html->url(array('action' => 'add_job', $job['Job']['id'])); ?>" class="btn btn-warning" style="color: white;">
                                <i class=" fas fa-wrench"></i> จัดการ
                            </a>
                            <a type="button" href="<?php echo $this->Html->url(array('action' => 'print_doc', $job['Job']['id'])); ?>" target="_blank" class="btn btn-info" style="color: white;">
                                <i class=" fas fa-print"></i> พิมพ์ใบแจ้งหนี้
                            </a>
                            <a type="button" href="<?php echo $this->Html->url(array('action' => 'print_receipt', $job['Job']['id'])); ?>" target="_blank" class="btn btn-success" style="color: white;">
                                <i class=" fas fa-print"></i> พิมพ์ใบเสร็จรับเงิน
                            </a>
                        </div>
                    </div>

                </div>
            <?php
            }
            ?>

        </div>
    </div>
</div>


<script>
    function confirm_click() {
        if (confirm('  กรุณายืนยันการทำรายการอีกครั้ง !!!  ')) {
            return true;
        } else {
            return false;
        }
    }
</script>