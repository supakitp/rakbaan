<div class="container">
    <div class="card">
        <div class="card-body">
            <center>
                <h1 class="mt-4">งาน</h1>
            </center>

            <?php
            //create form
            echo $this->Form->create('JobItem', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'));
            //set id = 0 for cake insert
            echo $this->Form->input('id', array('type' => 'hidden', 'value' => 0));
            ?>

            <div class="form-group">
                <label class="col-sm-12">
                    ประเภท:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('item_type_id', array(
                        'options' => $itemTypes,
                        'class' => 'form-control',
                        'label' => false
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    งาน:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('item_id', array(
                        'options' => $items,
                        'empty' => 'อื่นๆ',
                        'class' => 'form-control',
                        'label' => false
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('item_other', array(
                        'label' => false, 'class' => 'form-control',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2">
                    จำนวน:
                </label>
                <div class="col-sm-4">
                    <?php
                    echo $this->Form->input('qty', array(
                        'label' => false, 'class' => 'form-control', 'type' => 'number',  'value' => '1',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
                <label class="col-sm-2">
                    ราคา:
                </label>
                <div class="col-sm-4">
                    <?php
                    echo $this->Form->input('educational_institution', array(
                        'label' => false, 'class' => 'form-control', 'type' => 'number',  'value' => '1',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info" style="color: white;float: right;" name="btnSubmit">
                <i class="fas fa-plus"></i> เพิ่ม
            </button>
        </div>

        <hr />

        <div class="col-sm-12">
            <div class="form-group">
                <?php
                $nBackId = 0;
                if (isset($this->request->data['Job']) && $this->request->data['Job'] != null) $nBackId = $this->request->data['Job']['id']
                ?>
                <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'add_job', $jobId)); ?>" class="btn btn-danger"><i class="fas fa-arrow-left"></i>
                    ย้อนกลับ</a>
                <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'work_management', $jobId)); ?>" class="btn btn-success" style="float: right;"><i class="far fa-save"></i> เสร็จ</a>

            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
</div>