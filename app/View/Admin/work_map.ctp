<div class="card-body">
    <div class="col-sm-12">
        <div id='myMap' style='height: 100%;'></div>
        <script type='text/javascript'>
            function loadMapScenario() {
                var map = new Microsoft.Maps.Map(document.getElementById('myMap'), {
                    credentials: 'Aoy02u0QzfeL-v1a2iiozc05VWoLxR7H0gk9gmu16GbaaAOy6eHFYSCXGWK51C_G'
                });

                <?php

                $lat = "18.786879830434497";
                $lng = "98.97934913635254";

                if (isset($jobs) && count($jobs) > 0) {
                    foreach ($jobs as $job) {
                        if ($job['Job']['latitude'] != "" && $job['Job']['longitude'] != "") {
                ?>
                            try {
                                var location = new Microsoft.Maps.Location(<?php echo ($job['Job']['latitude']); ?>, <?php echo ($job['Job']['longitude']); ?>);
                                var pin = new Microsoft.Maps.Pushpin(location, {
                                    text: "⌂",
                                    title: "<?php echo ('งานเลขที่: ' . $job['Job']['job_num']); ?>"
                                });
                                map.entities.push(pin);
                                Microsoft.Maps.Events.addHandler(pin, 'click', function() {
                                    nextPage("<?php echo ($job['Job']['id']); ?>");
                                });
                            } catch (err) {}
                <?php
                            $lat = $job['Job']['latitude'];
                            $lng = $job['Job']['longitude'];
                        }
                    }
                }
                ?>

                map.setView({
                    //center: new Microsoft.Maps.Location(map.getCenter()),
                    center: new Microsoft.Maps.Location(<?= $lat ?>, <?= $lng ?>),
                    zoom: 11
                });

                function nextPage(id) {
                    window.location.assign("<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'add_job')); ?>/" + id);
                };

            }
        </script>
        <script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?callback=loadMapScenario' async defer></script>

    </div>
</div>