<style>
	/*
	@font-face {
		font-family: THSarabun;
		src: local('TH SarabunIT๙'), url(<?php echo $this->Html->url('/assets/THSarabunIT9/THSarabunIT๙.ttf'); ?>)
	}
	*/

	p.bw {
		width: 650px;
		word-wrap: break-word;
	}
</style>
<?php
echo $this->Html->script('html2pdf.bundle.min.js');
?>
<div class="container-fluid" style="padding: 25px 20px;">
	<table width="650" border="0" align="center" style="font-size: 20pt;">
		<tbody>
			<tr>
				<td>
					<center>
						<img src="<?php
									echo $this->webroot . 'img/logo.png';
									?>" style="width:70px" class="img-responsive" style="display: block; " />
					</center>
				</td>
			</tr>
			<tr>
				<td>
					<center>
						<b>ใบเสร็จรับเงิน</b><br />
						<p style="font-size: 14px;">
							เลขที่:<?= ' ' . $job['Job']['job_num'] ?><br />
							วันที่:<?= ' ' . $job['Job']['job_date'] ?><br /><br />
							ห้างหุ้นส่วนจำกัด รักษ์บ้าน(2564)<br />
							351/3 หมู่ 5 ตำบล ยางเนิ้ง อำเภอ สารภี จังหวัด เชียงใหม่ 50140 <br />
							โทร 065 4168668
						</p>
					</center>
				</td>
			</tr>
		</tbody>
	</table>
	<br />
	<table width="650" border="0" align="center" style="font-size: 14pt;">
		<tr>
			<td style="border-bottom: 1px dotted black">
				<b>คุณ</b> <?= $job['Job']['customer_name'] . ' ' . $job['Job']['customer_last_name']  ?>
			</td>
		</tr>
		<tr>
			<td style="border-bottom: 1px dotted black">
				<b>ที่อยู่</b> <?= $job['Job']['customer_address'] ?>
				<b>โทร</b> <?= $job['Job']['customer_phone'] ?>
			</td>
		</tr>
		</tbody>
	</table>
	<br /><br /><br />

	<table width="650" style="border-collapse:collapse; font-size: 14pt;" border="1" align="center">
		<thead style="font-size: 14px;">
			<tr>
				<th>รายการ</th>
				<th>ราคา/หน่วย</th>
				<th>จำนวน</th>
				<th>รวม</th>
			</tr>
		</thead>
		<tbody style="font-size: 12px;">
			<?php
			if (isset($jobItems) && count($jobItems) > 0) {
				foreach ($jobItems as $index => $item) {
			?>
					<tr>
						<td style="padding-left: 10px;border-bottom: 0;border-top: 0;">
							<?php
							echo ($index + 1 . '. ' . $item['JobItem']['item_other']);
							?>
						</td>
						<td style="text-align:right;padding-right: 10px;border-bottom: 0;border-top: 0;">
							<?php echo number_format($item['JobItem']['unit_price'], 2); ?>
						</td>
						<td style="text-align:right;padding-right: 10px;border-bottom: 0;border-top: 0;">
							<?php echo number_format($item['JobItem']['qty'], 0); ?>
						</td>
						<td style="text-align:right;padding-right: 10px;border-bottom: 0;border-top: 0;">
							<?php echo number_format($item['JobItem']['price'], 2); ?>
						</td>
					</tr>
			<?php
				}
			}
			?>
		</tbody>
		<tfoot style="font-size: 14px;">
			<!-- <tr>
					<td colspan="2" style="text-align:right;">
						Total Ex.Vat
					</td>
					<td>
					</td>
				</tr> -->
			<tr>
				<td colspan="3" style="text-align:right;padding-right: 10px;">
					Sub Total
				</td>
				<td style="text-align:right;padding-right: 10px;">
					<?php echo number_format($job['Job']['sub_total'], 2); ?>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="text-align:right;;padding-right: 10px;">
					Vat <?= $taxP ?>%
				</td>
				<td style=" text-align:right;padding-right: 10px;">
					<?php echo number_format($job['Job']['tax'], 2); ?>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="text-align:right;;padding-right: 10px;">
					Grand Total
				</td>
				<td style=" text-align:right;padding-right: 10px;">
					<?php echo number_format($job['Job']['total'], 2); ?>
				</td>
			</tr>
		</tfoot>
	</table>

	<br />
	<table width="650" border="0" align="center" style="font-size: 14px;">
		<tr>
			<td>
				หมายเหตุ:<?= $job['Job']['remark'] ?>
			</td>
		</tr>
	</table>
</div>


<script>
	var element = document.body;
	var opt = {
		margin: 0.5,
		filename: 'rakbaan<?php echo ('_' . $job['Job']['id']); ?>.pdf',
		image: {
			type: 'jpeg',
			quality: 1
		},
		html2canvas: {
			scale: 3
		},
		jsPDF: {
			unit: 'in',
			format: 'a4',
			orientation: 'portrait'
		}
	};

	html2pdf().set(opt).from(element).save();
</script>