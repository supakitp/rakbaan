<div class="container">
    <div class="card">
        <div class="card-body">
            <center>
                <h1 class="mt-4">งานที่เข้ามา</h1>
            </center>

            <?php
            foreach ($jobs as $index => $job) {
            ?>
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="form-group">
                            <div class="col-sm-12"><b><?= $index + 1 ?>. </b>
                                <?php
                                echo  'คุณ: ' . $job['JobFromWeb']['customer_name'] . ' ' . $job['ServiceArea']['full_name'];
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo 'โทร: <a href="' . $this->Html->url(array('controller' => 'admin', 'action' => 'customer_search', $job['JobFromWeb']['customer_phone'])) . '" target="_blank">' .
                                    $job['JobFromWeb']['customer_phone'] . '</a>'; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php
                                $arrDate = explode("-", date("Y-m-d", strtotime($job['JobFromWeb']['created'])));

                                $sDate = $arrDate['2'] . '/' . $arrDate['1'] . '/' . ($arrDate['0'] + 543) . ' ' . date("H:i:s", strtotime($job['JobFromWeb']['created']));
                                echo 'วันที่: ' . $sDate;
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><?php echo 'พิกัด: ' . $job['JobFromWeb']['latitude'] . ',' . $job['JobFromWeb']['longitude']; ?></div>
                        </div>
                        <div class="col-sm-12">
                            <a href="<?php echo $this->Html->url(array('action' => 'deleteJWF', $job['JobFromWeb']['id'])); ?>" class="btn btn-danger" onclick="return confirm_click();">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                            <a type="button" href="<?php echo $this->Html->url(array('action' => 'map_jfw', $job['JobFromWeb']['id'])); ?>" target="_blank" class="btn btn-info" style="color: white;">
                                <i class=" fas fa-print"></i> ดูแผนที่
                            </a>
                        </div>
                    </div>

                </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>

<script>
    function confirm_click() {
        if (confirm(' กรุณายืนยันการทำรายการอีกครั้ง !!! ')) {
            return true;
        } else {
            return false;
        }
    }
</script>