<div class="container">
    <div class="card">
        <div class="card-body">
            <center>
                <h1 class="mt-4">จัดการงาน</h1>
            </center>

            <?php
            //create form
            echo $this->Form->create('MisEducational', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'));
            //set id = 0 for cake insert
            echo $this->Form->input('id', array('type' => 'hidden', 'value' => 0));
            //set checkRequired
            ?>

            <div class="form-group">
                <label class="col-sm-12">
                    งานซ่อมเลขที่ 12
                </label>
            </div>

            <div class="form-group">
                <label class="col-sm-12">
                    วันที่:
                </label>
                <div class="col-sm-12">
                    <?php echo $this->Form->date('birthdate', array(
                        'type' => 'text',
                        'class' => 'datepickerthai form-control',
                        'autocomplete' => 'off',
                        'data-provide' => 'datepicker',
                        'data-date-language' => 'th-th',
                        'pattern' => '\d{1,2}/\d{1,2}/\d{4}'
                    )); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">
                    <font color="red">* </font>งานซ่อม:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('mis_level_educational_id', array(
                        'options' => '',
                        'class' => 'form-control',
                        'label' => false
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    <font color="red">* </font>ที่:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('degree', array(
                        'label' => false, 'class' => 'form-control',   'value' => 'หลังไทยวัสดุ',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    <font color="red">* </font>ชื่อลูกค้า:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('degree', array(
                        'label' => false, 'class' => 'form-control',   'value' => 'บอลคุง',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    <font color="red">* </font>โทร:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('major', array(
                        'label' => false, 'class' => 'form-control',   'value' => '0778956777',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    <font color="red">* </font>ค่าใช้จ่าย:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('educational_institution', array(
                        'label' => false, 'class' => 'form-control', 'value' => '100',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    <font color="red">* </font>ค่าแรง:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('educational_institution', array(
                        'label' => false, 'class' => 'form-control', 'value' => '500',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    <font color="red">* </font>รายละเอียด:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('educational_institution', array(
                        'label' => false, 'class' => 'form-control',   'value' => 'ท่อน้ำประปาหลังบ้านแตก',
                        'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    <font color="red">* </font>สถานะ:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('mis_level_educational_id', array(
                        'options' => '',
                        'class' => 'form-control',
                        'label' => false
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 control-label">รูป:<font color="red"> (ขนาดรูปที่เหมาะสมคือ 800x800 pixels)</font></label>
                <div class="row">
                    <div class="col-sm-12">
                        <a href="<?= $this->webroot . 'img/case1.jpg' ?>" target="_blank">
                            <img src="<?php $photo = $this->webroot . 'img/case1.jpg';
                                        echo $photo; ?>" alt="รูป" style="width:100%;" />
                        </a>
                        <?php
                        echo $this->Form->input('photo_tmp', array('type' => 'file', 'label' => false));
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div id='printoutPanel'></div>
                <label class="col-sm-12 control-label">แผนที่:</label>
                <div class="col-sm-12">
                    <div id='myMap' style='width: 100%; height: 400px;'></div>
                    <script type='text/javascript'>
                        function loadMapScenario() {
                            var map = new Microsoft.Maps.Map(document.getElementById('myMap'), {});

                            map.setView({
                                center: new Microsoft.Maps.Location('18.786879830434497', '98.97934913635254'),
                                zoom: 11
                            });

                            Microsoft.Maps.Events.addHandler(map, 'click', function(e) {
                                handleArgs('mapClick', e);
                            });

                            function handleArgs(id, e) {
                                for (var i = map.entities.getLength() - 1; i >= 0; i--) {
                                    var pushpin = map.entities.get(i);
                                    if (pushpin instanceof Microsoft.Maps.Pushpin) {
                                        map.entities.removeAt(i);
                                    }
                                }

                                var point = new Microsoft.Maps.Point(e.getX(), e.getY());
                                var locTemp = e.target.tryPixelToLocation(point);
                                var location = new Microsoft.Maps.Location(locTemp.latitude, locTemp.longitude);
                                var pin = new Microsoft.Maps.Pushpin(location, {
                                    'draggable': false
                                });
                                map.entities.push(pin);

                                document.getElementById('latitude').value = locTemp.latitude;
                                document.getElementById('longitude').value = locTemp.longitude;
                            };

                        }
                    </script>
                    <script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?key=AhMIwiW42OcYRiMq7TdU-55YKPEffo0A3E2kXYKTxz_TpuIZITJ2d4tAOq7R5xMm&callback=loadMapScenario' async defer></script>

                </div>
            </div>

            <div class="col-sm-12">
                <button type="submit" class="btn btn-lg btn-success" style="color: white;" value="1">
                    <i class=" far fa-save"></i> บันทึก
                </button>
            </div>
        </div>
    </div>
</div>