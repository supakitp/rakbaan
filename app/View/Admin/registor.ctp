<section class="vh-100 gradient-custom">
  <div class="container py-5 h-100">
    <div class="row justify-content-center">
      <div class="col-12 col-lg-9 col-xl-7">
        <div class="card shadow-2-strong card-registration" style="border-radius: 15px;">
          <div class="card-body p-4 p-md-5">
            <h3 class="mb-4 pb-2 pb-md-0 mb-md-5">Registration Form</h3>
            <form method="post">

              <div class="row">
                <div class="col-md-6 mb-4">

                  <div class="form-outline">
                    <label class="form-label" for="firstName">Username</label>
                    <input type="text" id="firstName" name="data[User][username]" class="form-control form-control-lg" required/>
                  </div>

                </div>
                <div class="col-md-6 mb-4">

                  <div class="form-outline">
                    <label class="form-label" for="lastName">Password</label>
                    <input type="text" id="lastName" name="data[User][password]" class="form-control form-control-lg" required/>
                    
                  </div>

                </div>
              </div>

              <div class="row">
                <div class="col-md-12 mb-12 d-flex align-items-center">

                  <div class="form-outline datepicker w-100">
                    <label for="birthdayDate" class="form-label">Name</label>
                    <input
                      type="text" name="data[User][name]"
                      class="form-control form-control-lg"
                      id="birthdayDate" required
                    />
                    
                  </div>

                </div>

                <div class="col-md-12 mb-12">
<br>
                  <h6 class="mb-2 pb-1">ประเภท: </h6>

                  <div class="form-check form-check-inline">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="data[User][user_type_id]"
                      id="femaleGender"
                      value="1"
                      checked
                    />
                    <label class="form-check-label" for="femaleGender">ผู้ดูแลระบบ</label>
                  </div>

                  <div class="form-check form-check-inline">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="data[User][user_type_id]"
                      id="maleGender"
                      value="2"
                    />
                    <label class="form-check-label" for="maleGender">วิศวกร</label>
                  </div>

                  <div class="form-check form-check-inline">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="data[User][user_type_id]"
                      id="otherGender"
                      value="3"
                    />
                    <label class="form-check-label" for="otherGender">สมาชิกทั่วไป</label>
                  </div>

                </div>
              </div>


              <div class="mt-4 pt-2">
                <input class="btn btn-primary btn-lg" type="submit" value="Submit" />
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>