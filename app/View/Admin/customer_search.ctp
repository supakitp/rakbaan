<style>
    .dataTables_filter {
        float: left !important;
    }
</style>
<div class="card">
    <div class="card-body">
        <div id="no-more-tables">
            <?php echo $this->Form->create('SearchCondition', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data')); ?>
            <p style="text-align: left;">
                ปี:
                <?php
                echo $this->Form->year(
                    'job_year',
                    date('Y') + 539,
                    date('Y') + 543,
                    array(
                        'class' => 'form-horizontal',
                        'value' => $defaultYear,
                        'onchange' => 'submit();'
                    )
                ); ?>
            </p>
            <table class="table table-striped table-bordered table-hover" id="myTable">
                <thead>
                    <tr>
                        <th>เลขที่งาน</th>
                        <th>ชื่อ</th>
                        <th>ที่อยู่-เบอร์โทร</th>
                        <th>Social</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($jobs) && count($jobs) > 0) {
                        foreach ($jobs as $job) {
                            echo ('<tr>
                            <td>
                            <a href="' . $this->Html->url(array('controller' => 'admin', 'action' => 'add_job', $job['Job']['id'])) . '" target="_blank">' .
                                $job['Job']['job_num'] . '
                            </a></td>
                            <td>' . $job['Job']['customer_name'] . ' ' . $job['Job']['customer_last_name']  . '</td>
                            <td>' . 'ที่อยู่:' . $job['Job']['customer_address'] . '</br>' . 'เบอร์โทร:' . $job['Job']['customer_phone'] . '</td>
                            <td>' . 'Facebook:' . $job['Job']['customer_fb'] . '</br>' . 'Line:' . $job['Job']['customer_line'] . '</td>
                            </tr>');
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            ordering: false,
            paging: false,
            language: {
                searchPlaceholder: "ค้นหา"
            }
        });
    });
</script>