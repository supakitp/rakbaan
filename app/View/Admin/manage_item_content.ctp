<br>
<div class="container" style="background-color: white">
	<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<h3 class="panel-title">
				<center><div class="font_panel2">รายการงาน</div></center>
			</h3>
	</div>


		<div class="col-md-12">
			<br>
			<div class="col-md-12 text-right">
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มงาน</button>

				  <!-- Modal -->
				  <div class="modal fade" id="myModal" role="dialog">
				    <div class="modal-dialog modal-lg">
				    <form method="post" action="<?php echo $this->Html->url(array('action' => 'manage_item_content',$sub_item_type_id)); ?>">
				      <div class="modal-content">
				        <div class="modal-header">
				          
				          <h4 class="modal-title">เพิ่มงาน</h4>
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				        </div>
				        <div class="modal-body">
				          <div class="row">

				          	<div class="col-md-12" style="text-align: left;">
				          		<label><font style="color: red">* </font>ชื่องาน</label>
				          		<input type="textbox" class="form-control" name="data[Item][name]" placeholder="กรอกงาน" required="true">
				          	</div>
				          	<div class="col-md-4" style="text-align: left;">
				          		<label><font style="color: red">* </font>ราคา</label>
				          		<input type="number" class="form-control" name="data[Item][price]" placeholder="กรอกราคา" required="true">
				          	</div>
				          	<div class="col-md-12" style="text-align: left;">
				          		<label>รายละเอียดการให้บริการ</label>
				          		<input type="textbox" class="form-control" name="data[Item][detail]" placeholder="กรอกรายละเอียดการให้บริการ">
				          	</div>
				          	<div class="col-md-12" style="text-align: left;">
				          		<label>หมายเหตุ</label>
				          		<input type="textbox" class="form-control" name="data[Item][note]" placeholder="กรอกหมายเหตุ">
				          	</div>
				          	<input type="hidden" class="form-control" name="data[Item][sub_item_type_id]" value="<?=$sub_item_type_id;?>" required="true">
				          </div>
				        </div>
				        <div class="modal-footer">
				        	<button type="submit" name="data[Item][button]" value="1" class="btn btn-success" >บันทึก</button>
				          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
				        </div>
				    </div>
				     </form>
				    </div>
				  </div>
				</div>
			</div>
		</div>

		<div class="container">
			<br>
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th width="5%">ลำดับ</th>
						<th>งาน</th>
						<th>ราคา</th>
						<th>รายละเอียดการให้บริการ</th>
						<th>หมายเหตุ</th>
						<th width="15%"></th>
					</tr>
				</thead>
				<tbody>
					<?
					//debug($strategics);
					$i = 1;
					foreach ($items as $item) 
					{
					?>
					<tr>
						<td><?=$i;?></td>
						<td><?=$item['Item']['name'];?></td>
						<td><?=number_format($item['Item']['price']);?></td>
						<td><?=$item['Item']['detail'];?></td>
						<td><?=$item['Item']['note'];?></td>
						<td>
							<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal<?=$item['Item']['id'];?>"><i class="fas fa-edit"></i></button>&nbsp;<a href="<?php echo $this->Html->url(array('action' => 'delete_item',$item['Item']['id'],$item['Item']['sub_item_type_id'])); ?>" class="btn btn-danger" OnClick="return chkdel();"><i class="far fa-trash-alt"></i></a></td>
					</tr>

					<!-- Modal -->
				  <div class="modal fade" id="myModal<?=$item['Item']['id'];?>" role="dialog">
				    <div class="modal-dialog modal-lg">
				    <form method="post" action="<?php echo $this->Html->url(array('action' => 'manage_item_content',$sub_item_type_id)); ?>">
				      <div class="modal-content">
				        <div class="modal-header">
				          
				          <h4 class="modal-title">แก้ไข </h4>
				          <button type="button" class="close" data-dismiss="modal">&times;</button>

				        </div>
				        <div class="modal-body">
				        

				          	<div class="col-md-12">
				          		<label><font style="color: red">* </font>ชื่องาน</label>
				          		<input type="textbox" class="form-control" name="data[Item][name]" placeholder="กรอกงาน" value="<?=$item['Item']['name'];?>" required="true">
				          	</div>
				          	<div class="col-md-4" style="text-align: left;">
				          		<label><font style="color: red">* </font>ราคา</label>
				          		<input type="number" class="form-control" name="data[Item][price]"  value="<?=$item['Item']['price'];?>" placeholder="กรอกราคา" required="true">
				          	</div>
				          	<div class="col-md-12" style="text-align: left;">
				          		<label>รายละเอียดการให้บริการ</label>
				          		<input type="textbox" class="form-control" name="data[Item][detail]"  value="<?=$item['Item']['detail'];?>" placeholder="กรอกรายละเอียดการให้บริการ">
				          	</div>
				          	<div class="col-md-12" style="text-align: left;">
				          		<label>หมายเหตุ</label>
				          		<input type="textbox" class="form-control" name="data[Item][note]"  value="<?=$item['Item']['note'];?>" placeholder="กรอกหมายเหตุ">
				          	</div>

				          	<div class="col-md-12">
				          		<input type="hidden" class="form-control" name="data[Item][id]" value="<?=$item['Item']['id'];?>" required="true">
				          	</div>
				          	
				         
				        </div>
				        <div class="modal-footer">
				        	<button type="submit" name="data[Item][button]" value="2" class="btn btn-success" >บันทึก</button>
				          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
				        </div>
				    </div>
				     </form>
				    </div>
				  </div>
					<?
					$i++;
					}
					?>
				</tbody>
			</table>
		</div>

	</div> <!-- panel-body -->
	
</div>
<br>

<script language="JavaScript">
function chkdel(){if(confirm('  กรุณายืนยันการทำรายการอีกครั้ง !!!  ')){
    return true;
  }else{
    return false;
  }
  }
</script>