<div class="container">
    <?php
    echo $this->Form->create('SlidePhoto', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'));
    ?>
    <div class="card">
        <div class="card-header">
            <center>
                <h2>Slide</h2>
            </center>
            <div class="form-group">
                <label class="col-sm-12">อัพรูป:<font color="red"> (ได้สูงสุด 10 รูป ต่อครั้ง)</font></label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input(
                        'photo_slide.',
                        array(
                            'type' => 'file', 'label' => false, 'multiple' => 'true', 'accept' => 'image/*'
                        )
                    );
                    ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-info"><i class="fas fa-arrow-alt-circle-up"></i> Upload</button>
                </div>
            </div>


            <div class="form-group">
                <?php
                if (isset($slides) && count($slides) > 0)
                    $i = 1;
                foreach ($slides as $index => $s) {
                    if ($i == 1) {
                        echo '<div class="row">';
                    }
                ?>
                    <div class="col-sm-3">
                        <div class="card">

                            <a href="<?= $this->webroot . 'img/slide/' . $s['SlidePhoto']['img'] ?>" target="_blank">
                                <img src="<?= $this->webroot . 'img/slide/' . $s['SlidePhoto']['img'] ?>" style="width:100%;" />
                            </a>

                            <a href="<?php echo $this->Html->url(array('action' => 'deleteSlide', $s['SlidePhoto']['id'])); ?>" class="btn btn-danger" onclick="return confirm_click();">
                                <i class="fas fa-trash-alt"></i>
                            </a>

                        </div>
                    </div>
                    <br />
                <?php
                    $i++;
                    //ขึ้น row ใหม่
                    if ($i > 4) {
                        //close row
                        echo '</div>';
                        $i = 1;
                    }
                    //close row ในกรณียังไม่เต็ม 4
                    elseif ($index + 1 == count($slides) && $i <= 4) {
                        echo '</div>';
                        $i = 1;
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>

    <br /><br />
    <?php
    echo $this->Form->create('WorkPhoto', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'));
    ?>
    <div class="card">
        <div class="card-header">
            <center>
                <h2>ผลงาน</h2>
            </center>
            <div class="form-group">
                <label class="col-sm-12">
                    ประเภทงาน:
                </label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input('item_type_id', array(
                        'options' => $itemTypes,
                        'class' => 'form-control',
                        'value' => $itemTypeId,
                        'onChange' => 'submit();',
                        'label' => false
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">อัพรูป:<font color="red"> (ได้สูงสุด 10 รูป ต่อครั้ง)</font></label>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->input(
                        'photo_tmp.',
                        array(
                            'type' => 'file', 'label' => false, 'multiple' => 'true', 'accept' => 'image/*'
                        )
                    );
                    ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-info"><i class="fas fa-arrow-alt-circle-up"></i> Upload</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?php
            if (isset($workPhotos) && count($workPhotos) > 0)
                $i = 1;
            foreach ($workPhotos as $index => $wPhoto) {
                if ($i == 1) {
                    echo '<div class="row">';
                }
            ?>
                <div class="col-sm-3">
                    <div class="card">

                        <a href="<?= $this->webroot . 'img/gallery/' . $wPhoto['WorkPhoto']['img'] ?>" target="_blank">
                            <img src="<?= $this->webroot . 'img/gallery/' . $wPhoto['WorkPhoto']['img'] ?>" style="width:100%;" />
                        </a>

                        <a href="<?php echo $this->Html->url(array('action' => 'deleteWorkPhoto', $wPhoto['WorkPhoto']['id'])); ?>" class="btn btn-danger" onclick="return confirm_click();">
                            <i class="fas fa-trash-alt"></i>
                        </a>

                    </div>
                </div>
                <br />
            <?php
                $i++;
                //ขึ้น row ใหม่
                if ($i > 4) {
                    //close row
                    echo '</div>';
                    $i = 1;
                }
                //close row ในกรณียังไม่เต็ม 4
                elseif ($index + 1 == count($workPhotos) && $i <= 4) {
                    echo '</div>';
                    $i = 1;
                }
            }
            ?>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>

</div>

<script>
    function confirm_click() {
        if (confirm(' กรุณายืนยันการทำรายการอีกครั้ง !!! ')) {
            return true;
        } else {
            return false;
        }
    }
</script>