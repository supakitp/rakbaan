
<div class="card">
    <div class="card-body">
        <div class="row">
            <!-- Button trigger modal -->
            
             <a class="btn btn-success" href="<?php echo $this->Html->url(array('controller' => 'Admin', 'action' => 'registor')); ?>">
              <i class="fas fa-user-plus"></i> เพิ่มพนักงาน
             </a>
        </div>
    </div>
</div>


<div class="card">
    <div class="card-body">

        <div id="no-more-tables">
            <table class="table table-striped table-bordered table-hover" id="myTable">
                <thead>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชื่อ</th>
                        <th>ระดับ</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?
                    $i =1;
                    foreach($users as $user)
                    {
                    ?>
                    <tr>
                        <th><?=$i;?></th>
                        <th><?=$user['User']['name'];?></th>
                        <th><?=$user['UserType']['name'];?></th>
                        <th>
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal<?=$user['User']['id'];?>"><i class="fas fa-edit"></i></button>&nbsp;<a href="<?php echo $this->Html->url(array('action' => 'delete_user',$user['User']['id'])); ?>" class="btn btn-danger" OnClick="return chkdel();"><i class="far fa-trash-alt"></i></a>
                        </th>
                    </tr>

                <!-- Modal -->
                  <div class="modal fade" id="myModal<?=$user['User']['id'];?>" role="dialog">
                    <div class="modal-dialog modal-lg">
                    <form method="post">
                      <div class="modal-content">
                        <div class="modal-header">
                          
                          <h4 class="modal-title">แก้ไข <?=$user['User']['name'];?></h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body">
                        
                                <input type="hidden" class="form-control" name="data[User][id]" value="<?=$user['User']['id'];?>">
                            <div class="col-md-12">
                                <label><font style="color: red">* </font>Username</label>
                                <input type="textbox" class="form-control" value="<?=$user['User']['name'];?>" readonly>
                            </div>
                            <div class="col-md-4" style="text-align: left;">
                                <label><font style="color: red">* </font>Password</label>
                                <input type="textbox" class="form-control" name="data[User][password]"  value="<?=$user['User']['password'];?>" placeholder="Password" required="true">
                            </div>
                            <div class="col-md-12" style="text-align: left;">
                                <label>Name</label>
                                <input type="textbox" class="form-control" name="data[Item][detail]"  value="<?=$user['User']['name'];?>" >
                            </div>
                            <div class="col-md-12" style="text-align: left;">
                                <label>ประเภท: </label><br>
                                  <div class="form-check form-check-inline">
                                    <input
                                      class="form-check-input"
                                      type="radio"
                                      name="data[User][user_type_id]"
                                      id="femaleGender"
                                      value="1"
                                      <?if($user['User']['user_type_id'] == 1){?>checked<?}?>
                                    />
                                    <label class="form-check-label" for="femaleGender">ผู้ดูแลระบบ</label>
                                  </div>

                                  <div class="form-check form-check-inline">
                                    <input
                                      class="form-check-input"
                                      type="radio"
                                      name="data[User][user_type_id]"
                                      id="maleGender"
                                      value="2"
                                      <?if($user['User']['user_type_id'] == 2){?>checked<?}?>
                                    />
                                    <label class="form-check-label" for="maleGender">วิศวกร</label>
                                  </div>

                                  <div class="form-check form-check-inline">
                                    <input
                                      class="form-check-input"
                                      type="radio"
                                      name="data[User][user_type_id]"
                                      id="otherGender"
                                      value="3"
                                      <?if($user['User']['user_type_id'] == 3){?>checked<?}?>
                                    />
                                    <label class="form-check-label" for="otherGender">สมาชิกทั่วไป</label>
                                  </div>
                            </div>


                         
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="data[Item][button]" value="2" class="btn btn-success" >บันทึก</button>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                     </form>
                    </div>
                  </div>

                    <?
                    $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            ordering: false,
            //paging: false,
            language: {
                searchPlaceholder: "ค้นหา"
            }
        });
    });
</script>

<script language="JavaScript">
function chkdel(){if(confirm('  กรุณายืนยันการทำรายการอีกครั้ง !!!  ')){
    return true;
  }else{
    return false;
  }
  }
</script>