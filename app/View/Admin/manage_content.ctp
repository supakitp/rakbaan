<br>
<div class="container" style="background-color: white">
	<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<h3 class="panel-title">
				<center><div class="font_panel2">ประเภทงาน</div></center>
			</h3>
	</div>


		<div class="col-md-12">
			<br>
			<div class="col-md-12 text-right">
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มประเภทงาน</button>

				  <!-- Modal -->
				  <div class="modal fade" id="myModal" role="dialog">
				    <div class="modal-dialog modal-lg">
				    <form method="post" action="<?php echo $this->Html->url(array('action' => 'manage_content')); ?>" enctype="multipart/form-data">
				      <div class="modal-content">
				        <div class="modal-header">
				          
				          <h4 class="modal-title">เพิ่มประเภทงาน</h4>
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				        </div>
				        <div class="modal-body">
				          <div class="row">

				          	<div class="col-md-12" style="text-align: left;">
				          		<label><font style="color: red">* </font>ชื่อประเภทงาน</label>
				          		<input type="textbox" class="form-control" name="data[ItemType][name]" placeholder="กรอกชื่อประเภทงาน" required="true">
				          	</div>

				          	<div class="col-md-12" style="text-align: left;">
				          		<br>
				          		<label><font style="color: red">* </font>ภาพประกอบ</label>
				          		<input type="file" class="form-control" name="data[ItemType][file_raw]" accept="image/*" required="true">
				          	</div>
				          	
				          </div>
				        </div>
				        <div class="modal-footer">
				        	<button type="submit" name="data[ItemType][button]" value="1" class="btn btn-success" >บันทึก</button>
				          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
				        </div>
				    </div>
				     </form>
				    </div>
				  </div>
				</div>
			</div>
		</div>

		<div class="container">
			<br>
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th width="5%">ลำดับ</th>
						<th>ประเภทงาน</th>
						<th width="15%"></th>
					</tr>
				</thead>
				<tbody>
					<?
					//debug($strategics);
					$i = 1;
					foreach ($itemTypes as $itemType) 
					{
					?>
					<tr>
						<td><?=$i;?></td>
						<td><a href="<?php echo $this->Html->url(array('action' => 'manage_sub_content',$itemType['ItemType']['id'])); ?>"><?=$itemType['ItemType']['name'];?></a></td>
						<td>
							<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal<?=$itemType['ItemType']['id'];?>"><i class="fas fa-edit"></i></button>&nbsp;<a href="<?php echo $this->Html->url(array('action' => 'delete_item_type',$itemType['ItemType']['id'])); ?>" class="btn btn-danger" OnClick="return chkdel();"><i class="far fa-trash-alt"></i></a></td>
					</tr>

					<!-- Modal -->
				  <div class="modal fade" id="myModal<?=$itemType['ItemType']['id'];?>" role="dialog">
				    <div class="modal-dialog modal-lg">
				    <form method="post" action="<?php echo $this->Html->url(array('action' => 'manage_content')); ?>" enctype="multipart/form-data">
				      <div class="modal-content">
				        <div class="modal-header">
				          
				          <h4 class="modal-title">แก้ไข </h4>
				          <button type="button" class="close" data-dismiss="modal">&times;</button>

				        </div>
				        <div class="modal-body">
				        

				          	<div class="col-md-12">
				          		<label><font style="color: red">* </font>ชื่อประเภทงาน</label>
				          		<input type="textbox" class="form-control" name="data[ItemType][name]" placeholder="กรอกชื่อประเภทงาน" value="<?=$itemType['ItemType']['name'];?>" required="true">
				          	</div>

				          	<div class="col-md-12" style="text-align: left;">
				          		<br>
				          		<label>ภาพประกอบ</label>
				          		<input type="file" class="form-control" name="data[ItemType][file_raw]" accept="image/*">
				          	</div>


				          	<div class="col-md-12">
				          		<input type="hidden" class="form-control" name="data[ItemType][id]" value="<?=$itemType['ItemType']['id'];?>" required="true">
				          	</div>
				          	
				         
				        </div>
				        <div class="modal-footer">
				        	<button type="submit" name="data[ItemType][button]" value="2" class="btn btn-success" >บันทึก</button>
				          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
				        </div>
				    </div>
				     </form>
				    </div>
				  </div>
					<?
					$i++;
					}
					?>
				</tbody>
			</table>
		</div>

	</div> <!-- panel-body -->
	
</div>
<br>

<script language="JavaScript">
function chkdel(){if(confirm('  กรุณายืนยันการทำรายการอีกครั้ง !!!  ')){
    return true;
  }else{
    return false;
  }
  }
</script>