





<div class="container">
    <?php
    echo $this->Form->create('ItemType', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'));
    ?>
    <div class="card mb-3">
        <div class="card-header">
            <center>
                <h2>บริการ</h2>
            </center>

            <div class="form-row">
                <?php
                if (!empty($itemType)) {
                    foreach ($itemType as $item) { ?>
                        <!-- <label class="col-sm-12">อัพรูป:<font color="red"> (ได้สูงสุด 10 รูป ต่อครั้ง)</font></label> -->
                        <div class="col-sm-10 mt-2">
                            <?php
                            echo $this->Form->input(
                                'objName' . $item['ItemType']['id'],
                                array(
                                    'id' => 'objId' . $item['ItemType']['id'],
                                    'type' => 'text', 'label' => false,
                                    'class' => 'form-control',
                                    'value' => $item['ItemType']['name']
                                )
                            );
                            ?>
                        </div>

                        <div class="col-sm-2 mt-2">
                            <a class="btn" onclick="edit(<?= $item['ItemType']['id'] ?>);" id="edit<?= $item['ItemType']['id'] ?>">
                                <i class="fas fa-wrench text-warning"></i>
                            </a>
                            <a class="btn" onclick="removeContent(<?= $item['ItemType']['id'] ?>);" id="removeContent<?= $item['ItemType']['id'] ?>">
                                <i class="fas fa-trash text-danger"></i>
                            </a>
                        </div>
                <? }
                } ?>
                <div class="col-sm-12 mt-2">
                    <a class="btn" onclick="addItem()">
                        <i class="fas fa-plus fa-2x text-success"></i>
                    </a>

                </div>
                <div id="addRow" class="col-sm-12"></div>

            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
</div>

<script type="text/javascript">
    $(function() {


    });
    var row = 1;

    function addItem() {
        var item = '';
        item += '<div class="mt-2 row" id="itemType_id' + row + '">';
        item += '<input class="form-control col-sm-8 ml-2" name="itemType' + row + '" id="itemtype' + row + '" ">';
        item += ' <a class="btn" onclick="confirmRow(' + row + ')" id="confirm' + row + '">';
        item += '<i class="fas fa-check text-success"></i>';
        item += '</a>';
        item += ' <a class="btn" onclick="removeRow(' + row + ')" id="remove' + row + '">';
        item += '<i class="fas fa-minus text-danger"></i>';
        item += '</a>';
        item += '</div>';

        $("#addRow").append(item);
        row++;
    }

    function confirmRow(i) {
        var valItem = $("input[name=itemType" + i + "]").val();
        if (valItem != '') {
            $.ajax({
                url: "<?php echo $this->Html->url(array('controller' => 'Admin', 'action' => 'add_content')); ?>",
                type: "post",
                data: {
                    value: valItem
                },
                success: function(response) {
                    var result = response.result;
                    if (result.msg == true) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'บันทึกสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $("#itemtype" + i).prop("disabled", true);
                        $("#confirm" + i).hide();
                        $("#remove" + i).hide();
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'พบข้อผิดพลาด',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        } else {
            Swal.fire({
                icon: 'error',
                title: 'กรุณากรอกข้อมูล',
                showConfirmButton: false,
                timer: 1500
            })
        }
    }

    function removeRow(i) {
        // let i = row - 1;
        $('#itemType_id' + i).remove();
    }

    function edit(id) {
        var valItem = $("#objId" + id).val();
        Swal.fire({
            title: 'กดยืนยันเพื่อแก้ไข?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "<?php echo $this->Html->url(array('controller' => 'Admin', 'action' => 'add_content')); ?>",
                    type: "post",
                    data: {
                        id: id,
                        value: valItem
                    },
                    success: function(response) {
                        var result = response.result;
                        var energie = '';
                        if (result.msg == true) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'บันทึกสำเร็จ',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'พบข้อผิดพลาด',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        })

    }
    function removeContent(id) {
        Swal.fire({
            title: 'กดยืนยันเพื่อลบ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "<?php echo $this->Html->url(array('controller' => 'Admin', 'action' => 'delete_content')); ?>",
                    type: "post",
                    data: {
                        id: id,
                    },
                    success: function(response) {
                        var result = response.result;
                        var energie = '';
                        if (result.msg == true) {
                            Swal.fire({
                                icon: 'success',
                                title: 'ลบสำเร็จ',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            $("#objId" + id).hide();
                            $("#edit" + id).hide();
                            $("#removeContent" + id).hide();
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'พบข้อผิดพลาด',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        })
    }
</script>