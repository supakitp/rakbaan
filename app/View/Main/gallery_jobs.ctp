<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>

<style type="text/css">
   body {
      margin: 0;
      padding: 0;
   }

   /* .container{
      width:90%
      margin:10px auto;
   } */
   .portfolio-menu {
      text-align: center;
   }

   .portfolio-menu ul li {
      display: inline-block;
      margin: 0;
      list-style: none;
      padding: 10px 15px;
      cursor: pointer;
      -webkit-transition: all 05s ease;
      -moz-transition: all 05s ease;
      -ms-transition: all 05s ease;
      -o-transition: all 05s ease;
      transition: all .5s ease;
   }

   .portfolio-item {
      /*width:100%;*/
   }

   .portfolio-item .item {
      /*width:303px;*/
      float: left;
      margin-bottom: 10px;
   }
</style>
<div class="container">
   <div class="row">
      <div class="col-lg-12 text-center my-2">
         <h4><?= $get_item_type['ItemType']['name']; ?></h4>
      </div>
   </div>

   <div class="portfolio-item row">

            <?
            foreach($workPhotos as $workPhoto)
            {
            ?>
               <?php $photo = 'https://rak-baan.com/img/gallery/' .$workPhoto['WorkPhoto']['img']; ?>
               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm">
                  <a href="<?php echo $photo;?>" class="fancylight popup-btn" data-fancybox-group="light"> 
                  <img class="img-fluid" src="<?php echo $photo = $this->webroot . 'img/gallery/' .$workPhoto['WorkPhoto']['img']; ?>" alt="">
                  </a>
               </div>
            <?
            }
            ?>
            
            <!--<div class="item selfie col-lg-3 col-md-4 col-6 col-sm">
               <a href="https://image.freepik.com/free-photo/two-smiling-girls-take-selfie-their-phones-posing-with-lollipops_8353-5600.jpg" class="fancylight popup-btn" data-fancybox-group="light">
               <img class="img-fluid" src="https://image.freepik.com/free-photo/two-smiling-girls-take-selfie-their-phones-posing-with-lollipops_8353-5600.jpg" alt="">
               </a>
            </div>-->
         </div> 
</div>


<script type="text/javascript">
   // $('.portfolio-item').isotope({
   //     itemSelector: '.item',
   //     layoutMode: 'fitRows'
   //  });
   $('.portfolio-menu ul li').click(function() {
      $('.portfolio-menu ul li').removeClass('active');
      $(this).addClass('active');

      var selector = $(this).attr('data-filter');
      $('.portfolio-item').isotope({
         filter: selector
      });
      return false;
   });
   $(document).ready(function() {
      var popup_btn = $('.popup-btn');
      popup_btn.magnificPopup({
         type: 'image',
         gallery: {
            enabled: true
         }
      });
   });
</script>