<style>
    hr {
        margin-top: 0 !important;
        margin-bottom: 0 !important;
        border: 0;
        border-top: 10px solid rgba(0, 0, 0, .1);
    }

    .form-step-button {
        width: 40%;
    }

    .btn-rounded {
        border-radius: 100px !important;
    }
</style>
<?
//debug($get_item_type);
?>
<div class="container">
    <div class="card mb-2">
        <div class="card-header d-flex justify-content-center">
            <h3><?= $get_item_type['ItemType']['name']; ?></h3>
        </div>
        <div class="card-body">
            <div class="d-flex justify-content-center">
                <img class=" img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/item_types/' . $get_item_type['ItemType']['photo']; ?>" style="width: 50%;">
            </div>

            <!--<div class="form-group">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">รายละเอียด <span class="text-danger">(หากมี)</span></label>
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="">
                </div>
            </div>
            <div class="mb-3">
                <label for="formFile" class="form-label">ภาพประกอบ <span class="text-danger">(หากมี)</span>
                    กรุณาถ่าย และอัดโหลดรูปภาพอย่างน้อย 1 รูป เพื่อให้ทีมช่างผู้เชี่ยวชาญของเราได้วิเคราะห์ปัญหาได้อย่างละเอียดและรวดเร็วที่สุด</label>
                <input class="form-control" type="file" id="formFile">
            </div>-->
            <!-- <hr>
            <div>
                <p><strong>รายละเอียดมาตรฐานบริการ</strong></p>
                <p>1) สำรวจพื้นที่หน้างานหาจุดรอยรั่วซึมบริเวณหลังคา</p>
                <p>*แก้ปัญหาหลังคารั่วซ้ำซากได้อย่างตรงจุด*</p>
                <p>2) เสนอแนวทางการแก้ไข พร้อมจัดทำใบเสนอราคาค่าบริการ</p>
                <p>3) รับประกันงานซ่อมหลังคานาน 60 วัน</p>
                <p>ค่าสำรวจหน้างานปกติ 1,500 บาท</p>
                <p>🔥 โปรโมชั่นพิเศษ ลดทันที 500 บาท เหลือเพียง 1,000 บาท เท่านั้น!&nbsp;🔥</p>
            </div>
            <hr> -->
            <div>
                <br />
                <!-- <p><strong>Company Job Listing</strong></p> -->
                <table class="table">
                    <thead>
                        <tr>
                            <th>หมวดหมู่</th>
                            <th>ราคา</th>
                            <th>รายละเอียดการให้บริการ</th>
                            <th>หมายเหตุ</th>
                        </tr>
                    </thead>
                    <?= $output; ?>
                </table>

            </div>

        </div>
        <!-- <div class="form-group mt-5 row justify-content-around">
            <div class="form-step-button">
                <a class="btn btn-rounded btn-block btn-danger" href="<?php echo $this->Html->url(array('controller' => 'main', 'action' => '/')); ?>">
                    ย้อนกลับ
                </a>
            </div>
            <div class="form-step-button"><button type="button" class="btn btn-rounded btn-block btn-success" onclick="submit1()">ถัดไป</button></div>
        </div> -->
    </div>
</div>

<script>
    function submit1() {
        Swal.fire(
            'บันทึกสำเร็จ',
            '',
            'success'
        )
    }
</script>