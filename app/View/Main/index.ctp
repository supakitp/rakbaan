<style>
    div a:hover {
        opacity: 0.5;
    }

    #backToTop {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Fixed/sticky position */
        bottom: 20px;
        /* Place the button at the bottom of the page */
        right: 30px;
        /* Place the button 30px from the right */
        z-index: 99;
        /* Make sure it does not overlap */
        border: none;
        /* Remove borders */
        outline: none;
        /* Remove outline */
        background-color: red;
        /* Set a background color */
        color: white;
        /* Text color */
        cursor: pointer;
        /* Add a mouse pointer on hover */
        padding: 15px;
        /* Some padding */
        border-radius: 20px;
        /* Rounded corners */
        font-size: 25px;
        /* Increase font size */
    }

    #backToTop:hover {
        background-color: #555;
        /* Add a dark-grey background on hover */
    }

    .notice {
        background-color: #130501;
        padding: 10px;
        text-align: center;
        color: #fff;
    }

    .content1 {
        position: relative;
    }

    .centered {
        position: absolute;
        top: 30%;
        left: 60%;
        transform: translate(-50%, -50%);
    }

    .repair1 {
        box-shadow: 5px 10px #B9B9B9;
    }

    hr {
        margin-top: 0 !important;
        margin-bottom: 0 !important;
        border: 0;
        border-top: 2px solid rgba(0, 0, 0, .1);
    }

    @media only screen and (max-width: 600px) {
        .centered {
            top: 50% !important;
            left: 50% !important;
        }

        .rp-content {
            width: 80% !important;
        }
    }

    #services-container .service-list img {
        width: 30px;
        height: 30px;
        border-radius: .5rem;
        -o-object-fit: cover;
        object-fit: cover;
    }

    #services-container .service-list {
        border: 1px solid #e0e0e0;
        padding: 1.125rem .875rem;
        border-radius: .5rem;
        -webkit-transition: border-color .3s;
        transition: border-color .3s;
    }

    a {
        color: #3c3935 !important;
        text-decoration: none !important;
        background-color: transparent !important;
    }

    .video-background {
        background: #ffffff;

        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: -99;
    }

    .video-foreground,
    .video-background iframe {
        position: absolute;
        background-size: cover;
        top: 0;
        left: 0;

        width: 100%;
        height: 100%;
        pointer-events: none;

    }
</style>

<div class="notice">
    ยินดีต้อนรับสู่ รักษ์บ้านโฮมเซอร์วิส<br />Welcome to Rak Baan Home Service
</div>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php
        if (isset($slides) && count($slides) > 0) {
            foreach ($slides as $index => $s) {
                $sActive = '';
                if ($index == 0) {
                    $sActive = 'active';
                }
        ?>
                <li data-target="#carouselExampleIndicators" data-slide-to="<?= $index ?>" class="<?= $sActive ?>"></li>
        <?php
            }
        }
        ?>
    </ol>

    <div class="carousel-inner">
        <?php
        if (isset($slides) && count($slides) > 0) {
            foreach ($slides as $index => $s) {
                $sActive = '';
                if ($index == 0) {
                    $sActive = 'active';
                }
        ?>
                <div class="carousel-item <?= $sActive ?>">
                    <img class="d-block w-100" src="<?php echo $this->webroot . 'img/slide/' . $s['SlidePhoto']['img']; ?>" alt="" style="max-height:800px">
                </div>
        <?php
            }
        }
        ?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<script type="text/javascript">
    $('.carousel').carousel({
        interval: 1000
    })
</script>

<br />

<div class="col-sm-12">
    <div class="container-fluid">
        <!-- <div class="row"> -->

        <!-- <iframe width="100%" height="315" src="https://www.youtube.com/embed/ijWziIHY26k?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay=1; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
        <!-- style="min-height:600px" -->

        <!-- </div> -->

        <div class="col-sm-12 mobileShow">
            <div class="row">
                <img class="img-responsive" src="<?php echo $this->webroot . 'img/footM2.gif'; ?>" style="width: 100%;">
            </div>
        </div>

        <!-- <div class="col-sm-12">
        <div class="row">
            <img class="img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/S__19292206.jpg'; ?>" style="width: 100%;">
        </div>
    </div> -->

        <div class="card-body content1" id="about">


            <!-- <img class="img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/bg_index.jpg'; ?>" style="width: 100%;height:100%">

        <div class="centered card repair1 rp-content" style="width: 40%;">
            <div class="col-sm-12">
                <div class="card-title text-center">
                    <h5 class="pt-2">บริการแจ้งซ่อม</h5>
                </div>
                <hr>
                <div class="card-body">
                    testS
                    <div id="submit" class="btn btn-success" onclick="submit1()">
                        แจ้งซ่อม
                    </div>
                </div>
            </div>

        </div> -->
        </div>

        <div data-aos="fade-up" data-aos-anchor-placement="top-center">
            <div class="col-sm-12" style="background-color: #FFD17D;z-index: 1;">
                <div class="card-body">
                    <h2>ประวัติความเป็นมาของห้างหุ้นส่วนจำกัด รักษ์บ้าน (2564)</h2>
                    <hr>
                    <br />
                    <div class="row">
                        <div class="col-sm-4">
                            <img class="img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/shutterstock_1759422665.jpg'; ?>" style="width: 100%;">
                        </div>
                        <div class="col-sm-8">

                            &nbsp &nbsp &nbsp ห้างหุ้นส่วนจำกัด รักษ์บ้าน (2564) หรือ Rak Baan Home Service เป็นบริษัทที่ให้บริการติดตั้ง ซ่อมแซม ดูแลรักษา เกี่ยวกับงานไฟฟ้า ประปา แอร์และอื่นๆ ภายในบ้านพักอาศัยที่อยู่ในบางเขตพื้นที่ของจังหวัดเชียงใหม่ โดยเน้นเรื่องของความสะอาด ปลอดภัย และการบริการที่ดี จากบุคลากรที่มีประสบการณ์ด้านงานช่างและมีใจรักในการให้บริการ
                            <br />
                            &nbsp &nbsp &nbsp ห้างหุ้นส่วนจำกัด รักษ์บ้าน (2564) หรือ Rak Baan Home Service ก่อตั้งในปี 2564 จากการรวมตัวของหุ้นส่วนในฐานะเจ้าของบ้านเช่นกัน และ มีความเข้าใจเป็นอย่างดี เวลาบ้านพักอาศัยมีปัญหาต่างๆเจ้าของบ้านมีความกังวลเพราะไม่มั่นใจว่าจะได้ช่างที่มีมาตรฐาน ความปลอดภัย และ บริการที่ดี ซึ่งปัญหาเหล่านี้จะหมดไป เพราะทาง Rak Baan Home Service จะให้บริการงานซ่อมแซม แก้ไขต่างๆเพื่อให้บ้านลูกค้าทุกท่านน่าอยู่ตลอดไป
                        </div>
                    </div>
                    <br />
                </div>
            </div>


            <br />

            <div class="col-sm-12">
                <div class="card-body">
                    <h2>พันธกิจในการดำเนินงานของห้างหุ้นส่วนจำกัด รักษ์บ้าน (2564)</h2>
                    <hr>

                    <p>เข้าใจ ใส่ใจ ดูแล คือ คติประจำตัวของห้างหุ้นส่วนจำกัด รักษ์บ้าน (2564) หรือ Rak Baan Home Service ซึ่งเราจะเป็นบริษัทที่ (เข้าใจ) ความต้องการของลูกค้าทางด้านงานซ่อมแซมดูแลรักษาอุปกรณ์ของใช้ทั่วไปภายในบ้านพักอาศัย และ (ใส่ใจ) ให้บริการที่เป็นเลิศ ด้วยการ (ดูแล) ที่เปรียบเสมือนบ้านของตัวเอง</p>

                    <p>ห้างหุ้นส่วนจำกัด รักษ์บ้าน (2564) หรือ Rak Baan Home Service มีจุดมุ่งหมายที่ให้บริการโดยใช้มาตรฐานด้าน ความสะอาด ปลอดภัย และบริการที่มีคุณภาพแก่ลูกค้าพร้อมทั้งพัฒนาศักยภาพบุคลากรของบริษัทอย่างต่อเนื่อง และมีแนวคิดที่ให้พนักงานของบริษัททุกคนมีส่วนร่วมในการสร้างนโยบายการดำเนินงานทางธุรกิจควบคู่ไปกับบรรยากาศการทำงานที่เป็นสุข และส่งมอบทัศนคติที่ดีในการให้บริการไปยังลูกค้าของบริษัท</p>

                </div>
                <div class="row1-container">

                    <div class="box red mobileShow">
                        <h2>พันธกิจ </h2>
                        <p>Rak Baan Home Service </p>
                        <img src="<?php echo $photo = $this->webroot . 'img/logo.png'; ?>" style="width: 50px;" alt="">
                    </div>


                    <div class="box box-down cyan">
                        <h2>เข้าใจ</h2>
                        <p>ความต้องการของลูกค้าทางด้านงานซ่อมแซมดูแลรักษาอุปกรณ์ของใช้ทั่วไปภายในบ้านพักอาศัย</p>
                        <img src="<?php echo $photo = $this->webroot . 'img/logo.png'; ?>" style="width: 50px;" alt="">
                    </div>

                    <div class="box red mobileHide">
                        <h2>พันธกิจ </h2>
                        <p>Rak Baan Home Service </p>
                        <img src="<?php echo $photo = $this->webroot . 'img/logo.png'; ?>" style="width: 50px;" alt="">

                    </div>

                    <div class="row2-container mobileShow">
                        <div class="box orange">
                            <h2>ใส่ใจ</h2>
                            <p>ให้บริการที่เป็นเลิศ</p>
                            <img src="<?php echo $photo = $this->webroot . 'img/logo.png'; ?>" style="width: 50px;" alt="">
                        </div>
                    </div>

                    <div class="box box-down blue">
                        <h2>ดูแล</h2>
                        <p>เปรียบเสมือนบ้านของตัวเอง</p>
                        <img src="<?php echo $photo = $this->webroot . 'img/logo.png'; ?>" style="width: 50px;" alt="">
                    </div>
                </div>
                <div class="row2-container mobileHide">
                    <div class="box orange">
                        <h2>ใส่ใจ</h2>
                        <p>ให้บริการที่เป็นเลิศ</p>
                        <img src="<?php echo $photo = $this->webroot . 'img/logo.png'; ?>" style="width: 50px;" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="row">
        <div class="container col-md-12">
            <div class="wrapper">
                <div class="card"><i class="fal fa-arrow-right">งานไฟฟ้า (ซ่อม,เปลี่ยน )</i></div>
            </div>
            <div class="wrapper">
                <div class="card"><i class="fal fa-arrow-right">งานแอร์ (งานล้าง,ซ่อม,เปลี่ยน )</i></div>
            </div>
            <div class="wrapper">
                <div class="card"><i class="fal fa-arrow-right">งานสุขภัณฑ์ (งานซ่อม,เปลี่ยน)</i></div>
            </div>
            <div class="wrapper">
                <div class="card"><i class="fal fa-arrow-right">งานไฟฟ้า (ติดตั้ง)</i></div>
            </div>
            <div class="wrapper">
                <div class="card"><i class="fal fa-arrow-right">งานประปา (ติดตั้ง)</i></div>
            </div>
            <div class="wrapper">
                <div class="card"><i class="fal fa-arrow-right">อื่นๆ</i></div>
            </div>
        </div>
    </div>-->
        <br>
        <div class="col-sm-12" id="team">
        </div>
        <div data-aos="fade-up" data-aos-anchor-placement="top-center">
            <div class="col-sm-12">
                <div class="card-body">
                    <h2>ทีมงาน</h2>
                    <hr>
                    <div class="row">
                        <img class="img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/team.jpg'; ?>" style="width: 100%;height:100%">
                    </div>
                    <br />
                    <div class="row">
                        <img class="img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/team2.jpg'; ?>" style="width: 100%;height:100%">
                    </div>
                </div>
            </div>
        </div>

        <br />
        <div class="col-sm-12" id="service">
        </div>

        <!-- MObile -->
        <!-- <div class="mobileHide" data-aos="fade-up" data-aos-anchor-placement="top-center">
            <div class="col-sm-12" class="mobileHide">
                <div class="card-body" style="background-image: url('<?php echo $photo = $this->webroot . 'img/S__2105351.jpg'; ?>');color:white;width: 100%;">
                    <h2>ผลงาน</h2>
                    <div class="row">
                        <img class="img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/S__2105349.jpg'; ?>" style="width: 100%;">
                    </div>
                    <div class="row" style="padding-top: 100px;">


                        <?
                        $i = 1;
                        foreach ($get_item_types as $item) {
                            if ($i == 1) {
                        ?>
                                <div class="col-sm-3">
                                    <div class="card-body">
                                        &nbsp;
                                    </div>
                                </div>
                            <?
                            }
                            if ($i == 4) {
                            ?>
                                <div class="col-sm-4">
                                    <div class="card-body">
                                        &nbsp;
                                    </div>
                                </div>
                            <?
                            }
                            ?>
                            <div class="col-sm-3" <? if ($i >= 4) { ?> style="padding-top: 120px;" <? } ?>>
                                <div class="card-body">
                                    <a href="<?php echo $this->Html->url(array('controller' => 'main', 'action' => 'gallery_jobs', $item['ItemType']['id'])); ?>">
                                        <img class=" img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/item_types/' . $item['ItemType']['photo']; ?>" style="width: 320px;height:200px;border: 5px solid black;">

                                    </a>


                                </div>
                            </div>
                        <?
                            $i++;
                        }
                        ?>

                        <div class="col-sm-12">
                            <div class="card-body">
                                <br /><br /><br /><br />
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div> -->



        <div data-aos="fade-up" data-aos-anchor-placement="top-center">
            <div class="col-sm-12">
                <div class="card-body" style="background-color: black;color:white;">
                    <h2 style="color:white;">ผลงาน</h2>
                    <div class="row">


                        <?
                        $i = 1;
                        foreach ($get_item_types as $item) {
                            if ($i == 4 and count($get_item_types) == 5) {
                        ?>
                                <div class="col-sm-2">
                                    <div class="card-body">
                                        &nbsp;
                                    </div>
                                </div>
                            <?
                            }
                            ?>
                            <div class="col-sm-4">
                                <div class="card-body">
                                    <a href="<?php echo $this->Html->url(array('controller' => 'main', 'action' => 'gallery_jobs', $item['ItemType']['id'])); ?>">
                                        <img class=" img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/item_types/' . $item['ItemType']['photo']; ?>" style="width: 100%;height:100%;">
                                    </a>
                                    <center>
                                        <?= $item['ItemType']['name']; ?>
                                    </center>
                                </div>
                            </div>
                        <?
                            $i++;
                        }
                        ?>



                    </div>

                </div>
            </div>
        </div>




        <div data-aos="fade-up" data-aos-anchor-placement="top-center">
            <div class="col-sm-12 mb-2">
                <div class="card-body">
                    <h2>ตรวจสอบอัตราค่าบริการ</h2>
                    <p>(โปรดคลิ๊กที่หมวดงานด้านล่าง)</p>
                    <hr />
                    <div class="row justify-content-center mt-2">
                        <div class="col-xl-7 col-lg-9">
                            <div id="services-container">
                                <?php
                                foreach ($get_item_types as $item) {
                                ?>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'main', 'action' => 'service', $item['ItemType']['id'])); ?>">
                                        <div class="service-list mb-3">
                                            <div class="row align-items-center">
                                                <div class="col-auto"> <img class=" img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/item_types/' . $item['ItemType']['photo']; ?>" style="width: 100%;">
                                                </div>
                                                <div class="col text-left">
                                                    <h3 class="service-title mb-0"><?= $item['ItemType']['name']; ?></h3>
                                                    <!-- <span class="starting-from"><?= number_format($item['ItemType']['price'], 2); ?></span> -->
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br /> <br />
        <hr width="100%" />
        <?php
        echo $this->Form->create('JobFromWeb', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'));
        echo $this->Form->input('latitude', array('id' => 'latitude', 'type' => 'hidden', 'value' => ''));
        echo $this->Form->input('longitude', array('id' => 'longitude', 'type' => 'hidden', 'value' => ''));
        ?>
        <div class="col-sm-12 mb-2" id="contact" data-aos="fade-up" data-aos-anchor-placement="top-center">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <img class="img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/S__2105353.jpg'; ?>" style="width: 100%">
                    </div>
                    <div class="col-sm-6">
                        <h2>ติดต่อเรา</h2>
                        <i class=" fas fa-home"></i> ห้างหุ้นส่วนจำกัด รักษ์บ้าน(2564)<br />
                        <i class="fas fa-address-card"></i> 351/3 หมู่ 5 ตำบล ยางเนิ้ง อำเภอ สารภี จังหวัด เชียงใหม่ 50140<br />
                        <i class="fas fa-globe-asia"></i> www.rak-baan.com<br />
                        <i class="far fa-envelope"></i> rakbaan2564@gmail.com<br />
                        <i class="fas fa-phone-square-alt"></i> 065 4168668<br />
                        <a href="https://www.facebook.com/%E0%B8%A3%E0%B8%B1%E0%B8%81%E0%B8%A9%E0%B9%8C%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%99-%E0%B9%82%E0%B8%AE%E0%B8%A1%E0%B9%80%E0%B8%8B%E0%B8%AD%E0%B8%A3%E0%B9%8C%E0%B8%A7%E0%B8%B4%E0%B8%AA-108435501654659/" target="_blank"><i class="fab fa-facebook-square"></i> รักษ์บ้าน โฮมเซอร์วิส</a><br />
                        <a href="https://www.instagram.com/rak_baan_home_service2564/" target="_blank"><i class="fab fa-instagram"></i> rak_baan_home_service2564</a><br />
                        <a><i class="fab fa-line"></i> 0654168668</a>
                        <br /> <br />
                        <hr width="100%" color="#FFD17D" />
                        <br />


                        <!-- <center>
                            <h2>ตรวจเช็คพื้นที่ให้บริการ</h2>
                        </center> -->
                        <div class="form-group">
                            <label class="col-sm-12">
                                <h2>พื้นที่ให้บริการ:(หรือพื้นที่ใกล้เคียง) </h2>
                            </label>
                            <div class="col-sm-12">
                                <?php
                                echo $this->Form->input('service_area_id', array(
                                    'options' => $serviceAreas,
                                    'class' => 'form-control',
                                    'onChange' => 'submit();',
                                    'label' => false
                                ));
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">
                                ชื่อลูกค้า:
                            </label>
                            <div class="col-sm-12">
                                <?php
                                echo $this->Form->input('customer_name', array(
                                    'label' => false, 'class' => 'form-control', 'required' => true,
                                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                                ));
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">
                                เบอร์โทร:
                            </label>
                            <div class="col-sm-12">
                                <?php
                                echo $this->Form->input('customer_phone', array(
                                    'label' => false, 'class' => 'form-control', 'required' => true, 'type' => 'text', 'id' => 'customer_phone',
                                    'onKeyDown' => 'if(this.value.length==20 && event.keyCode!=8) return false;',
                                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                                ));
                                ?>
                            </div>
                        </div>
                        <script>
                            $("#customer_phone").keydown(function() {
                                $("#customer_phone").val(this.value.match(/[0-9]*/));
                            });
                        </script>

                        <div class="form-group">
                            <div id='printoutPanel'></div>
                            <label class="col-sm-12 control-label">แผนที่:</label>
                            <div class="col-sm-12">
                                <div id='myMap' style='width: 100%; height: 400px;'></div>
                                <script type='text/javascript'>
                                    function loadMapScenario() {
                                        var map = new Microsoft.Maps.Map(document.getElementById('myMap'), {
                                            credentials: 'Aoy02u0QzfeL-v1a2iiozc05VWoLxR7H0gk9gmu16GbaaAOy6eHFYSCXGWK51C_G'
                                        });

                                        map.setView({
                                            center: new Microsoft.Maps.Location('18.786879830434497', '98.97934913635254'),
                                            zoom: 11
                                        });

                                        Microsoft.Maps.Events.addHandler(map, 'click', function(e) {
                                            handleArgs('mapClick', e);
                                        });

                                        /*var a =  pushpin.getLocation();
                                        document.getElementById('printoutPanel').innerHTML = pushpin.getLocation(); */
                                        function handleArgs(id, e) {
                                            for (var i = map.entities.getLength() - 1; i >= 0; i--) {
                                                var pushpin = map.entities.get(i);
                                                if (pushpin instanceof Microsoft.Maps.Pushpin) {
                                                    map.entities.removeAt(i);
                                                }
                                            }

                                            var point = new Microsoft.Maps.Point(e.getX(), e.getY());
                                            var locTemp = e.target.tryPixelToLocation(point);
                                            var location = new Microsoft.Maps.Location(locTemp.latitude, locTemp.longitude);
                                            var pin = new Microsoft.Maps.Pushpin(location, {
                                                'draggable': false
                                            });
                                            map.entities.push(pin);

                                            document.getElementById('latitude').value = locTemp.latitude;
                                            document.getElementById('longitude').value = locTemp.longitude;
                                        };

                                    }
                                </script>
                                <script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?callback=loadMapScenario' async defer></script>

                            </div>
                        </div>
                        <button id="submit" type="submit" class="btn btn-success" style="float: right;">
                            ส่ง
                        </button>
                    </div>
                    <br />
                </div>

                <!-- <h2>พื้นที่ให้บริการ</h2>
                        <img class="img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/map.jpg'; ?>" style="width: 50%;float: left;"> -->
            </div>
        </div>

        <div class="mobileHide">
            <iframe width="100%" height="600px" src="https://www.youtube.com/embed/L-RxBg2Gbh0?controls=0&showinfo=0&rel=0&mute=1&loop=1&vq=hd1080" frameborder="0"></iframe>
        </div>

        <div class="mobileShow">
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/L-RxBg2Gbh0?controls=0&showinfo=0&rel=0&mute=1&loop=1&vq=hd1080" frameborder="0"></iframe>
        </div>

        <button id="backToTop" type="button" class="btn btn-outline-danger" onclick="topFunction()"><i class="fas fa-arrow-up"></i></button>
    </div>
</div>

<script>
    AOS.init();

    function submit1() {
        Swal.fire(
            'เรียบร้อย!',
            'ทางทีมงานจะรีบติดต่อกลับโดยเร็วที่สุด'
        )
    }
    //Get the button:
    mybutton = document.getElementById("backToTop");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
</script>
<!-- <div class="container-fulid">
    <div class="animation-box">
        <div class="first-text">
            <img class="img-rounded img-responsive" src="<?php echo $photo = $this->webroot . 'img/Logo2.png'; ?>" style="width: 200px;">
            <div class="indexH1">RAK BAAN HOME SERVICE</div>
            <div class="indexH2">Website Information</div>
            <div class="indexH3">รักษ์บ้าน</div>
        </div>
    </div>
</div> -->