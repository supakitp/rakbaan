<label for="appt">Choose a time for your meeting:</label>

<input type="time" id="appt" name="appt" placeholder="hrs:mins" required>

<small>Office hours are 9am to 6pm</small>
