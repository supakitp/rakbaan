<!DOCTYPE html>
<html lang="en" class="h-100">
<style>
	body {
		font-family: 'Kanit',
			sans-serif !important;
	}
</style>

<head>
	<?php echo $this->Html->charset(); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta content="RAK BAAN HOME SERVICE Website Information" name="author" />
	<meta content="<?php echo $keywords;
					?>" name="keywords" />
	<meta content="<?php echo $keywordsTh;
					?>" name="keywords" lang="th" />
	<meta content="<?php echo $description;
					?>" name="description" />

	<?php
	echo $this->Html->meta('favicon.ico', '/img/logo.png', array('type' => 'icon'));
	//echo $this->Html->css('agri');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>

	<title>
		<?php echo $title_for_layout; ?>
	</title>

	<?php //echo $this->Html->css('hover-animation'); 
	?>

	<?php echo $this->Html->css('card-feature'); ?>

	<!-- effect ตัวหนังสือ -->
	<?php echo $this->Html->css('fadeText'); ?>
	<!-- close effect ตัวหนังสือ -->

	<!-- sb nav แถบข้างหน้า user -->
	<?php echo $this->Html->css('sb-nav'); ?>
	<!-- close sb nav แถบข้างหน้า user -->

	<!-- DatepickerThai -->
	<?php echo $this->Html->css('/assets/DatepickerThai/css/datepicker'); ?>
	<!-- close DatepickerThai -->

	<!-- Bootstrap core CSS -->

	<?php echo $this->Html->css('/assets/bootstrap-4.5.3/dist/css/bootstrap.min.css'); ?>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script>
		window.jQuery || document.write('<script src="../assets/bootstrap-4.5.3/dist/js/vendor/jquery.slim.min.js"><\/script>')
	</script>
	<?php echo $this->Html->script('/assets/bootstrap-4.5.3/dist/js/bootstrap.bundle.min.js'); ?>
	<!-- close bootstrap -->

	<!-- Textbox Search -->
	<?php echo $this->Html->css('/assets/textbox-search/jquery-ui'); ?>
	<!-- js นี้เอาใส่ข้างล่างแล้ว address ไม่รัน -->
	<?php echo $this->Html->script('/assets/textbox-search/jquery.min.js'); ?>
	<?php echo $this->Html->script('/assets/textbox-search/jquery-ui.js'); ?>
	<!-- close Textbox search -->

	<!-- Datatable -->
	<?php
	echo $this->Html->css('/assets/DataTables/datatables.min.css');
	echo $this->Html->script('/assets/DataTables/datatables.min.js');
	?>
	<!-- clsoe Datatable -->

	<?php echo $this->Html->css('/assets/fontawesome-5.15.1/css/all.css'); ?>

	<!-- font -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Kanit&display=swap" rel="stylesheet">

	<!-- sweetalert -->
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

	<!-- aos animate -->
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

	<!--  ปฏิทิน -->
	<?php echo $this->Html->css('/assets/fullcalendar-4.4.2/packages/core/main.css'); ?>
	<?php echo $this->Html->css('/assets/fullcalendar-4.4.2/packages/daygrid/main.css'); ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
	<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script> -->
	<?php echo $this->Html->css('/assets/fullcalendar-4.4.2/packages/timegrid/main.css'); ?>
	<?php echo $this->Html->css('/assets/fullcalendar-4.4.2/packages/list/main.css'); ?>

	<?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/core/main.js'); ?>
	<?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/daygrid/main.js'); ?>


	<?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/core/locales/th.js'); ?>
	<?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/timegrid/main.js'); ?>
	<?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/interaction/main.js'); ?>
	<?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/list/main.js'); ?>

	<!-- format เวลา -->
	<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" type="text/css" rel="stylesheet" /> -->
	<?php echo $this->Html->css('/assets/TimingField/timingfield.css'); ?>
	<?php echo $this->Html->script('/assets/TimingField/timingfield.js'); ?>


	<style type="text/css">
		#calendar {
			width: 800px;
			margin: auto;
		}

		:root {
			--fc-border-color: black;
			--fc-daygrid-event-dot-width: 20px;
		}

		/*Ball 2020-10-20*/
		@media screen and (max-width: 997px) {

			.container,
			.form-control,
			.card,
			.card-body,
			.table tr td,
			.table tr th,
			.modal,
			.modal-header,
			.modal-body,
			.text {
				font-size: 12px;
			}

			.h1,
			.h2,
			.h3,
			.h4,
			.card-header h1,
			.card-header h2,
			.card-header h3,
			.card-header h4,
			.card-header h5,
			.card-body h1,
			.card-body h2,
			.card-body h3,
			.card-body h4,
			.card-body h5,
			.mt-4 {
				font-size: 20px;
			}

			.mobileHide {
				display: none;
				position: relative;
			}


		}

		@media screen and (min-width: 997px) {
			.mobileShow {
				display: none;
			}
		}

		input[type=number]::-webkit-inner-spin-button,
		input[type=number]::-webkit-outer-spin-button {
			-webkit-appearance: none;
			-moz-appearance: none;
			appearance: none;
			margin: 0;
		}
	</style>
</head>



<body style="<?php echo $this->Html->style(array('margin-bottom' => '0')); ?>" class="d-flex flex-column h-100">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-54232567-5"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-54232567-5');
	</script>

	<header>
		<nav class="navbar navbar-expand-lg navbar-light  bg-transparent justify-content-between">
			<a class="navbar-brand" href="<?php echo $this->Html->url(array('controller' => 'main', 'action' => 'index')); ?>">
				<img src="<?php echo $this->Html->url('/img/logo.png'); ?>" style="max-height:80px;" class="d-inline-block align-top" alt="" loading="lazy" />
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- <div class="collapse show navbar-collapse" id="navbarSupportedContent"> -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<!--li class="nav-item active"-->
					<li class="nav-item">
						<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'main', 'action' => 'index')); ?>"><i class="fas fa-home"></i> Home
							<span class="sr-only">(current)</span></a>
					</li>
					<?php
					if (isset($isIndex) && $isIndex == 1) {
					?>
						<li class="nav-item">
							<a class="nav-link" href="#about">
								<i class="fas fa-info-circle"></i> เกียวกับ</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#team">
								<i class="fas fa-user"></i> ทีมงาน</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#service">
								<i class="fas fa-tools"></i> บริการ</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#contact">
								<i class="fas fa-phone-alt"></i> ติดต่อ</a>
						</li>
					<?
					}
					$loginUser = $this->Session->read('loginUser');
					//debug($loginUser);
					if ($loginUser != null) {
					?>
						<!-- <li class="nav-item">
							<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'index')); ?>">
								<i class="fas fa-calendar-alt"></i> ปฏิทิน</a>
						</li> -->


						<?php
						if ($loginUser['User']['user_type_id'] == 1 || $loginUser['User']['user_type_id'] == 2) {
						?>
							<!-- <li class="nav-item">
								<div class="input-group">
									<div class="form-outline">
										<input type="search" id="form1" class="form-control" />
										<label class="form-label" for="form1">ค้นหา</label>
									</div>
									<button type="button" class="btn btn-info">
										<i class="fas fa-search"></i>
									</button>
								</div>
							</li> -->
							<?php if ($loginUser['User']['user_type_id'] == 1 && (!isset($isIndex) || $isIndex == 0)) { ?>
								<li class="nav-item">
									<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'manage_area')); ?>">
										<?php echo '<i class="fas fa-tools"></i> ' . 'จัดการหมู่บ้าน'; ?>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'choose_photo')); ?>">
										<?php echo '<i class="fas fa-images"></i> ' . 'จัดการรูป'; ?>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'manage_content')); ?>">
										<?php echo '<i class="fas fa-tools"></i> ' . 'จัดการหมวดหมู่'; ?>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'job_from_web')); ?>" target="_blank">
										<?php echo '<i class="fas fa-arrow-down"></i> ' . 'งานที่เข้ามา'; ?>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'customer_search')); ?>">
										<?php echo '<i class="fas fa-search"></i> ' . 'ข้อมูลลูกค้า'; ?>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'list_users')); ?>">
										<?php echo '<i class="fas fa-users-cog"></i> ' . 'พนักงาน'; ?>
									</a>
								</li>
							<? } ?>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'index')); ?>">
									<?php echo '<i class="fas fa-user-circle"></i> ' . $loginUser['User']['name']; ?>
								</a>
							</li>
						<?php
						}
						?>


						<li class="nav-item">
							<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'Main', 'action' => 'logout')); ?>">
								<i class="fas fa-power-off"></i> Logout</a>
						</li>
					<?php
					} else {
					?>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'Main', 'action' => 'login')); ?>">
								<i class="fas fa-sign-in-alt"></i> Login</a>
						</li>
						<!-- <li class="nav-item">
							<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'Admin', 'action' => 'work_management')); ?>">
								<i class="fas fa-sign-in-alt"></i> Admin</a>
						</li> -->
					<?php
					} ?>
					<!-- <li class="nav-item">
						<a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'index')); ?>">
							<i class="fas fa-user-clock"></i> ช่าง</a>
					</li> -->
				</ul>
			</div>

		</nav>
	</header>
	<main role=" main" class="flex-shrink-0">
		<!-- flash -->
		<!-- set sweetalert2 -->
		<?php
		$alertMessage = $this->Session->flash();
		if ($alertMessage) {
			if (!isset($alertType)) {
				$alertType = $this->Session->read('alertType');
		?>
				<script>
					Swal.fire({
						position: 'center',
						icon: '<?= $alertType ?>',
						title: '<?= $alertMessage; ?>',
						showConfirmButton: false,
						timer: 1500
					})
				</script>
				<!-- <div class="alert alert-<?php echo $alertType; ?>" role="alert" style="text-align: center;">
					<?php echo $alert; ?>
				</div> -->
		<?php

			}
		}
		?>
		<!-- close flash -->

		<!-- content -->
		<?php echo $this->fetch('content'); ?>
		<!-- close content -->
	</main>
	<footer class="footer mt-auto py-3" style="<?php echo $this->Html->style(array('background-color' => 'black')); ?>">
		<div class="container">
			<div class="col-sm-12 pt-2">
				<div class="card" style="background-color:black;">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-3">
								<img src="<?php echo $this->Html->url('/img/logo3.png'); ?>" width="100%" class="d-inline-block align-top" alt="" loading="lazy" />
							</div>
							<div class="col-sm-9">
								<p class="text-muted" style="<?php echo $this->Html->style(array('color' => '#FFF !important')); ?>">
									<i class="fas fa-home"></i> RAK BAAN (2564) Limited Partnership<br />
									351/3 Moo 5 Yang Noeng, Saraphi District, Chiang Mai 50140<br />
									<i class="fas fa-home"></i> ห้างหุ้นส่วนจำกัด รักษ์บ้าน(2564)<br />
									351/3 หมู่ 5 ตำบล ยางเนิ้ง อำเภอ สารภี จังหวัด เชียงใหม่ 50140<br />
									<i class="fas fa-globe-asia"></i> www.rak-baan.com<br />
									<i class="far fa-envelope"></i> rakbaan2564@gmail.com<br />
									<i class="fas fa-phone-square-alt"></i> 065 4168668<br />
									<a href="https://www.facebook.com/%E0%B8%A3%E0%B8%B1%E0%B8%81%E0%B8%A9%E0%B9%8C%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%99-%E0%B9%82%E0%B8%AE%E0%B8%A1%E0%B9%80%E0%B8%8B%E0%B8%AD%E0%B8%A3%E0%B9%8C%E0%B8%A7%E0%B8%B4%E0%B8%AA-108435501654659/" target="_blank" class="text-white"><i class="fab fa-facebook-square"></i> รักษ์บ้าน โฮมเซอร์วิส</a><br />
									<a href="https://www.instagram.com/rak_baan_home_service2564/" target="_blank" class="text-white"><i class="fab fa-instagram"></i> rak_baan_home_service2564</a><br />
									<a class="text-white"><i class="fab fa-line"></i> 0654168668</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</footer>



	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->


	<!-- DatepickerThai -->
	<?php echo $this->Html->script('/assets/DatepickerThai/bootstrap-datepicker.js'); ?>
	<?php echo $this->Html->script('/assets/DatepickerThai/bootstrap-datepicker.th.js'); ?>
	<?php echo $this->Html->script('/assets/DatepickerThai/bootstrap-datepicker-thai.js'); ?>
	<!-- close DatepickerThai -->

	<!-- fontawesome -->
	<?php echo $this->Html->script('/assets/fontawesome-5.15.1/js/all.js'); ?>
	<!-- close fontawesome -->

	<!-- sb nav แถบข้างหน้า user -->
	<?php echo $this->Html->script('sb-nav'); ?>
	<!-- close sb nav แถบข้างหน้า user -->
</body>

</html>