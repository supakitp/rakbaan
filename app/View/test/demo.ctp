<!DOCTYPE html>
<html lang='en'>

<head>

  <!--   ส่วนที่เพิ่มเข้ามาใหม่-->

  <?php echo $this->Html->css('/assets/fullcalendar-4.4.2/packages/core/main.css'); ?>
  <?php echo $this->Html->css('/assets/fullcalendar-4.4.2/packages/daygrid/main.css'); ?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
  <?php echo $this->Html->css('/assets/fullcalendar-4.4.2/packages/timegrid/main.css'); ?>
  <?php echo $this->Html->css('/assets/fullcalendar-4.4.2/packages/list/main.css'); ?>

  <?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/core/main.js'); ?>
  <?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/daygrid/main.js'); ?>


  <?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/core/locales/th.js'); ?>
  <?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/timegrid/main.js'); ?>
  <?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/interaction/main.js'); ?>
  <?php echo $this->Html->script('/assets/fullcalendar-4.4.2/packages/list/main.js'); ?>




  <style type="text/css">
    #calendar {
      width: 800px;
      margin: auto;
    }
  </style>

</head>

<body style="padding-top: 20px;">

  <div id='calendar'></div>



  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
  <script type="text/javascript">
 $(function(){
     // กำหนด element ที่จะแสดงปฏิทิน
   var calendarEl = $("#calendar")[0];
 
     // กำหนดการตั้งค่า
   var calendar = new FullCalendar.Calendar(calendarEl, {
       plugins: [ 'interaction','dayGrid', 'timeGrid', 'list' ], // plugin ที่เราจะใช้งาน
       defaultView: 'dayGridMonth', // ค้าเริ่มร้นเมื่อโหลดแสดงปฏิทิน
       header: {
           left: 'prev,next today',
           center: 'title',
           right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
       },  
       events: { // เรียกใช้งาน event จาก json ไฟล์ ที่สร้างด้วย php
           url: 'events.php?gData=1',
           error: function() {
 
           }
       },              
       eventLimit: true, // allow "more" link when too many events
       locale: 'th',    // กำหนดให้แสดงภาษาไทย
       firstDay: 0, // กำหนดวันแรกในปฏิทินเป็นวันอาทิตย์ 0 เป็นวันจันทร์ 1
       showNonCurrentDates: false, // แสดงที่ของเดือนอื่นหรือไม่
       eventTimeFormat: { // รูปแบบการแสดงของเวลา เช่น '14:30' 
           hour: '2-digit',
           minute: '2-digit',
           meridiem: false
       }
   });
 
    // แสดงปฏิทิน 
   calendar.render();  
 
 });
 </script>


</body>

</html>