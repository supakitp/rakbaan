
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="card mb-2">
                <div class="card-header">
                    <div class="">
                        <h3 class="text-muted">ปฏิทินงานช่าง <i class="far fa-calendar-alt"></i></h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div id='calendar' class="col-sm-12"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {

        // กำหนด element ที่จะแสดงปฏิทิน
        var calendarEl = $("#calendar")[0];
        // กำหนดการตั้งค่า
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'], // plugin ที่เราจะใช้งาน
            defaultView: 'dayGridMonth', // ค้าเริ่มร้นเมื่อโหลดแสดงปฏิทิน
            selectable: true,
            dateClick: function(info) {
                // alert('Clicked on: ' + info.dateStr);
                sendDate(info.dateStr);
                // change the day's background color just for fun
                // info.dayEl.style.backgroundColor = 'red';
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMont'
            },
            events: { // เรียกใช้งาน event จาก json ไฟล์ ที่สร้างด้วย php
                url: "<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'load_data')); ?>",
                // data: {
                // },
                // success: function(response) {
                //     var result = response.result;
                //     $.each(result.item, function(key, value) {
                //         item = new Option(value, key);
                //         $('#item_id').append(item);
                //         $("#item_select").css("display", "block");
                //     });
                // },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            },
            eventLimit: true, // allow "more" link when too many events
            locale: 'th', // กำหนดให้แสดงภาษาไทย
            firstDay: 0, // กำหนดวันแรกในปฏิทินเป็นวันอาทิตย์ 0 เป็นวันจันทร์ 1
            showNonCurrentDates: false, // แสดงที่ของเดือนอื่นหรือไม่
            eventTimeFormat: { // รูปแบบการแสดงของเวลา เช่น '14:30' 
                hour: '2-digit',
                minute: '2-digit',
                meridiem: false
            }
        });

        // แสดงปฏิทิน 
        calendar.render();
    });

    function sendDate(date) {
        console.log(date);
        // window.location.replace("<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'get_date')); ?>" + '/' + date);
        $.ajax({
            url: "<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'get_date')); ?>",
            type: "post",
            data: {
                value: date
            },
            // Callback handler that will be called on success
            success: function(response, textStatus, jqXHR) {
                // Log a message to the console
                console.log("Hooray, it worked!");
            },
            // Callback handler that will be called on failure
            error: function(jqXHR, textStatus, errorThrown) {
                // Log the error to the console
                console.error(
                    "The following error occurred: " +
                    textStatus, errorThrown
                );
            }
        });
    }
</script>