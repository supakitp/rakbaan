<div class="container">
    <div class="card">
        <div class="card-body">

            <div class="card-body">
                <center>
                    <h1 class="mt-4">รายการงาน (ช่าง)</h1>
                    <h4> <a class="nav-link" href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'index')); ?>">
                            <i class="fas fa-calendar-alt"></i> <?php echo $dateShow; ?></a></h4>
                </center>
                <div class="form-group">
                    <?php
                    echo $this->Form->create('status', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'));
                    ?>
                    <div class="card-header row">
                        <div class="col-sm-4">
                            <?php
                            echo $this->Form->input('job_status_id', array(
                                'options' => $jobStatuses,
                                'class' => 'form-control',
                                'empty' => 'แสดงทั้งหมด',
                                'onChange' => 'submit();',
                                'label' => false
                            ));
                            ?>
                        </div>

                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>

            <!-- <nav>
                <div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        ใหม่
                    </a>
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        กำลังดำเนินการ
                    </a>
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        เสร็จแล้ว
                    </a>
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        ยกเลิก
                    </a>
                </div>
            </nav> -->

            <?php
            foreach ($jobs as $job) {
                if ($job['Job']['job_status_id'] == 1) {
                    $sColor = 'black';
                } elseif ($job['Job']['job_status_id'] == 2) {
                    $sColor = 'red';
                } elseif ($job['Job']['job_status_id'] == 3) {
                    $sColor = 'green';
                } elseif ($job['Job']['job_status_id'] == 4) {
                    $sColor = 'lightgray';
                }
            ?>
                <div class="card mb-4">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6" style="color:<?= $sColor ?>;">
                                <b>
                                    <i class="fas fa-clock"></i> <?php echo  date('H:i', strtotime($job['Job']['start_time'])) . ' - ' . date('H:i', strtotime($job['Job']['end_time'])) .
                                                                        ' สถานะ: ' . $job['JobStatus']['name'];  ?>
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-10">
                                    <h5><b><?php echo 'งานซ่อมเลขที่ ' . $job['Job']['job_num']; ?></b></h5>
                                </div>
                                <div class="col-sm-2 d-flex justify-content-end">
                                    <?php if ($job['Job']['check_out'] != null) { ?>
                                        <i class="fas fa-check fa-2x text-success"></i>
                                    <? } else { ?>
                                        <div class="spinner-border text-danger" role="status">
                                            <span class="visually-hidden"></span>
                                        </div>
                                    <? } ?>
                                </div>

                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><b>ชื่อลูกค้า:</b> <?php echo  $job['Job']['customer_name']; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><b>โทร:</b> <?php echo  $job['Job']['customer_phone']; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><b>ที่:</b> <?php echo  $job['Job']['customer_address']; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><b>รายละเอียด:</b> <?php echo  $job['Job']['detail']; ?></div>
                        </div>
                        <div class="col-sm-12">
                            <a type="button" href="<?php echo $this->Html->url(array('action' => 'work_detail', $job['Job']['id'])); ?>" class="btn btn-warning" style="color: white;">
                                <i class=" fas fa-wrench"></i> ดำเนินการ
                            </a>
                            <a type="button" href="<?php echo $this->Html->url(array('controller' => 'Admin', 'action' => 'print_doc', $job['Job']['id'])); ?>" target="_blank" class="btn btn-info" style="color: white;">
                                <i class=" fas fa-print"></i> พิมพ์ใบแจ้งหนี้
                            </a>
                            <a type="button" href="<?php echo $this->Html->url(array('controller' => 'Admin', 'action' => 'print_receipt', $job['Job']['id'])); ?>" target="_blank" class="btn btn-success" style="color: white;">
                                <i class=" fas fa-print"></i> พิมพ์ใบเสร็จรับเงิน
                            </a>
                        </div>
                    </div>

                </div>
            <?php
            }
            ?>

        </div>
    </div>
</div>


<script>
    function confirm_click() {
        if (confirm('  กรุณายืนยันการทำรายการอีกครั้ง !!!  ')) {
            return true;
        } else {
            return false;
        }
    }
</script>