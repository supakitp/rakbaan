<div class="container">
    <div class="card mb-2">
        <card class="card-header">
            <center class="text-muted">
                <h1>รายการงาน</h1>
                <h4><i class="far fa-clock"></i> 2021-10-17</h4>
            </center>

        </card>
        <div class="card-body">

            <br /><br />


            <nav>
                <div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        ใหม่
                    </a>
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        กำลังดำเนินการ
                    </a>
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        เสร็จแล้ว
                    </a>
                    <a class="nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
                        ยกเลิก
                    </a>
                </div>
            </nav>


            <div class="card mb-4">
                <div class="card-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-10">
                                <h5><b>12. งานซ่อม:</b> ท่อน้ำประปา</h5>
                            </div>
                            <div class="col-sm-2 d-flex justify-content-end">
                                <div class="spinner-border text-warning" role="status">
                                    <span class="visually-hidden"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>ที่:</b> หลังไทยวัสดุ</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>ชื่อลูกค้า:</b> บอลคุง</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>โทร:</b> 0778956777</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>รายละเอียด:</b> ท่อน้ำประปาหลังบ้านแตก</div>
                    </div>

                    <div class="col-sm-12">
                        <a type="button" href="./work_detail" class="btn btn-warning" style="color: white;">
                            <i class=" fas fa-wrench"></i> จัดการ
                        </a>
                        <button type="button" class="btn btn-success" style="color: white;">
                            <i class=" fas fa-print"></i> พิมพ์ใบเสร็จ
                        </button>
                    </div>
                </div>

            </div>

            <div class="card mb-4">
                <div class="card-body">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-10">
                                <h5><b>12. งานซ่อม:</b> ท่อน้ำประปา</h5>
                            </div>
                            <div class="col-sm-2 d-flex justify-content-end">
                                <i class="fas fa-check fa-2x text-success"></i>
                                <!-- <div class="spinner-border text-warning" role="status">
                                    <span class="visually-hidden"></span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>ที่:</b> หลังไทยวัสดุ</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>ชื่อลูกค้า:</b> บอลคุง</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>โทร:</b> 0778956777</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>รายละเอียด:</b> ท่อน้ำประปาหลังบ้านแตก</div>
                    </div>

                    <div class="col-sm-12">
                        <a type="button" href="./work_detail" class="btn btn-warning" style="color: white;">
                            <i class=" fas fa-wrench"></i> จัดการ
                        </a>
                        <button type="button" class="btn btn-success" style="color: white;">
                            <i class=" fas fa-print"></i> พิมพ์ใบเสร็จ
                        </button>
                    </div>
                </div>

            </div>
            <div class="card mb-4">
                <div class="card-body">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-10">
                                <h5><b>12. งานซ่อม:</b> ท่อน้ำประปา</h5>
                            </div>
                            <div class="col-sm-2 d-flex justify-content-end">
                                <div class="spinner-border text-warning" role="status">
                                    <span class="visually-hidden"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>ที่:</b> หลังไทยวัสดุ</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>ชื่อลูกค้า:</b> บอลคุง</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>โทร:</b> 0778956777</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12"><b>รายละเอียด:</b> ท่อน้ำประปาหลังบ้านแตก</div>
                    </div>

                    <div class="col-sm-12">
                        <a type="button" href="./work_detail" class="btn btn-warning" style="color: white;">
                            <i class=" fas fa-wrench"></i> จัดการ
                        </a>
                        <button type="button" class="btn btn-success" style="color: white;">
                            <i class=" fas fa-print"></i> พิมพ์ใบเสร็จ
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<script>
    function confirm_click() {
        if (confirm('  กรุณายืนยันการทำรายการอีกครั้ง !!!  ')) {
            return true;
        } else {
            return false;
        }
    }
</script>