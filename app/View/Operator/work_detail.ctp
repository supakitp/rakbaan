<div class="container">
    <div class="card">
        <div class="card-body">
            <center>
                <h1 class="mt-4">งานเลขที่ <?= $jobs['Job']['job_num'] ?></h1>
                <h4 class="text-muted">( <?php echo $dateThai; ?> )</h4>
            </center>
            <hr>
            <?php
            //create form
            echo $this->Form->create('Job', array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'));
            //set id = 0 for cake insert
            echo $this->Form->input('id', array('type' => 'hidden', 'value' => 0));
            //set checkRequired

            //map
            echo $this->Form->input('latitude', array('id' => 'latitude', 'type' => 'hidden', 'value' => $jobs['Job']['latitude']));
            echo $this->Form->input('longitude', array('id' => 'longitude', 'type' => 'hidden', 'value' => $jobs['Job']['longitude']));
            ?>
            <div class="form-group">
                <label class="col-sm-12">
                    วันที่: <?php echo $dateThai; ?>
                </label>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    เวลา: <?php echo $timeFormat; ?>
                </label>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    งานซ่อม: <?php if (!empty($itemType)) {
                                    echo $itemType['ItemType']['name'];
                                } else {
                                    echo 'ไม่มีข้อมูล';
                                } ?>
                </label>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    รายละเอียดงานซ่อม: <?php echo $jobs['Job']['detail'] ?>
                </label>
            </div>
            <div class="form-group">
                <div id='printoutPanel'></div>
                <label class="col-sm-12 control-label">แผนที่:</label>
                <div class="col-sm-12">
                    <div id='myMap' style='width: 100%; height: 200px;'></div>
                    <script type='text/javascript'>
                        function loadMapScenario() {
                            var map = new Microsoft.Maps.Map(document.getElementById('myMap'), {});

                            <?php
                            if (isset($jobs) && $jobs['Job']['latitude'] != null && $jobs['Job']['longitude'] != null) {
                            ?>
                                var location = new Microsoft.Maps.Location(<?php echo ($jobs['Job']['latitude']); ?>, <?php echo ($jobs['Job']['longitude']); ?>);
                                var pin = new Microsoft.Maps.Pushpin(location, {
                                    'draggable': false
                                });
                                map.entities.push(pin)

                                map.setView({
                                    center: new Microsoft.Maps.Location(<?php echo ($jobs['Job']['latitude']); ?>, <?php echo ($jobs['Job']['longitude']); ?>),
                                    zoom: 15
                                });
                            <?php
                            } else {
                            ?>
                                map.setView({
                                    center: new Microsoft.Maps.Location('18.786879830434497', '98.97934913635254'),
                                    zoom: 11
                                });
                            <?php
                            }
                            ?>

                            // Microsoft.Maps.Events.addHandler(map, 'click', function(e) {
                            //     handleArgs('mapClick', e);
                            // });

                            function handleArgs(id, e) {
                                for (var i = map.entities.getLength() - 1; i >= 0; i--) {
                                    var pushpin = map.entities.get(i);
                                    if (pushpin instanceof Microsoft.Maps.Pushpin) {
                                        map.entities.removeAt(i);
                                    }
                                }

                                var point = new Microsoft.Maps.Point(e.getX(), e.getY());
                                var locTemp = e.target.tryPixelToLocation(point);
                                var location = new Microsoft.Maps.Location(locTemp.latitude, locTemp.longitude);
                                var pin = new Microsoft.Maps.Pushpin(location, {
                                    'draggable': false
                                });
                                map.entities.push(pin);

                                document.getElementById('latitude').value = locTemp.latitude;
                                document.getElementById('longitude').value = locTemp.longitude;
                            };

                        }
                    </script>
                    <script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?key=AhMIwiW42OcYRiMq7TdU-55YKPEffo0A3E2kXYKTxz_TpuIZITJ2d4tAOq7R5xMm&callback=loadMapScenario' async defer></script>

                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-12">
                    ชื่อลูกค้า: <?= $jobs['Job']['customer_name'] . ' ' . $jobs['Job']['customer_last_name']  ?>
                </label>
            </div>
            <div class="form-group">
                <label class="col-sm-12">
                    ที่อยู่: <?= $jobs['Job']['customer_address'] ?>
                </label>
            </div>
            <?php if (!empty($jobs['Job']['customer_fb'])) { ?>
                <div class="form-group">
                    <label class="col-sm-12">
                        Facebook: <?= $jobs['Job']['customer_fb'] ?>
                    </label>
                </div>
            <? } ?>
            <?php if (!empty($jobs['Job']['customer_line'])) { ?>
                <div class="form-group">
                    <label class="col-sm-12">
                        Line: <?= $jobs['Job']['customer_line'] ?>
                    </label>
                </div>
            <? } ?>
            <div class="form-group">
                <label class="col-sm-12">
                    เบอร์โทร: <?= $jobs['Job']['customer_phone'] ?>
                </label>
            </div>

            <div class="form-group">
                <label class="col-sm-12">
                    ค่าใช้จ่ายที่ต้องเก็บ: <?= $jobs['Job']['total'] ?>
                </label>
            </div>
            <!-- <div class="form-group">
                <label class="col-sm-12">
                    รายละเอียด: <?= $jobs['Job']['customer_name'] ?>
                </label>
            </div> -->
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <button type="button" class="btn btn-lg btn-success checkIn" style="color: white;" id="checkIn">
                            <i class="fas fa-check"></i> Check-in
                        </button>
                    </div>


                    <div class="col-sm-6">
                        <button type="button" class="btn btn-lg btn-success checkIn" style="color: white;" id="checkOut">
                            <i class="fas fa-check"></i> Check-out
                        </button>
                    </div>
                </div>
            </div>
            <hr>
            <h3>ก่อนทำ</h3>
            <?php
            if (isset($jobImgs2) && count($jobImgs2) > 0)
                foreach ($jobImgs2 as $img) {
            ?>
                <div class="form-group">
                    <label class="col-sm-12">รูป:</label>
                    <div class="col-sm-4">
                        <div class="card">
                            <a href="<?= $this->webroot . 'img/job_img/' . $img['JobImg']['img'] ?>" target="_blank">
                                <img src="<?php echo $this->webroot . 'img/job_img/' . $img['JobImg']['img']; ?>" style="width:100%;" />
                            </a>
                            <a href="<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'deleteImg', $img['JobImg']['id'])); ?>" class="btn btn-danger" onclick="return confirm_click();">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </div>
                    </div>
                    <br />
                </div>
            <?php
                }
            ?>
            <div class="form-group">
                <label class="col-sm-12 control-label">อัพโหลดรูป: <span class="text-danger">(ได้สูงสุด 10 รูป)</span></label>
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        echo $this->Form->input('photo_tmp1.', array(
                            'id' => 'photo_tmp', 'type' => 'file',
                            'label' => false, 'class' => 'form-control',
                            'multiple' => 'true', 'accept' => 'image/*'
                        ));
                        ?>
                    </div>
                    <div class="col-sm-12 mt-2">
                        <button type="submit" class="btn btn-lg btn-warning" style="color: white;" value="1" name="btn">
                            <i class=" far fa-save"></i> Upload
                        </button>
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-12">
                <button type="button" class="btn btn-lg btn-success" style="color: white;" id="save">
                    <i class=" far fa-save"></i> เสร็จสิ้น
                </button>
            </div> -->
            <hr>
            <h3>หลังทำ</h3>
            <?php
            if (isset($jobImgs3) && count($jobImgs3) > 0)
                foreach ($jobImgs3 as $img) {
            ?>
                <div class="form-group">
                    <label class="col-sm-12">รูป:</label>
                    <div class="col-sm-4">
                        <div class="card">
                            <a href="<?= $this->webroot . 'img/job_img/' . $img['JobImg']['img'] ?>" target="_blank">
                                <img src="<?php echo $this->webroot . 'img/job_img/' . $img['JobImg']['img']; ?>" style="width:100%;" />
                            </a>
                            <a href="<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'deleteImg', $img['JobImg']['id'])); ?>" class="btn btn-danger" onclick="return confirm_click();">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </div>
                    </div>
                    <br />
                </div>
            <?php
                }
            ?>
            <div class="form-group">
                <label class="col-sm-12 control-label">อัพโหลดรูป: <span class="text-danger">(ได้สูงสุด 10 รูป)</span></label>
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        echo $this->Form->input('photo_tmp2.', array(
                            'id' => 'photo_tmp', 'type' => 'file',
                            'label' => false, 'class' => 'form-control',
                            'multiple' => 'true', 'accept' => 'image/*',
                        ));
                        ?>
                    </div>
                    <div class="col-sm-12 mt-2">
                        <button type="submit" class="btn btn-lg btn-warning" style="color: white;" value="2" name="btn">
                            <i class=" far fa-save"></i> Upload
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
<script type="text/javascript">
    var jobId = <?= json_encode($jobs['Job']['id']) ?>;
    var chkCheckin = <?= json_encode($jobs['Job']['check_in']) ?>;
    var chkCheckout = <?= json_encode($jobs['Job']['check_out']) ?>;
    $(function() {
        // $("#save").prop("disabled", true);
        if (chkCheckin != null) {
            $("#checkIn").prop("disabled", true);
        }
        if (chkCheckout != null) {
            $("#checkOut").prop("disabled", true);
        }
        $("#checkIn").click(function() {
            Swal.fire({
                title: 'กดยืนยันเพื่อทำการเช็คอิน',
                text: "",
                icon: 'warning',
                position: 'center',
                showCancelButton: 'true',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'ยกเลิก',
                confirmButtonText: 'ยืนยัน'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'check_in')); ?>",
                        type: "post",
                        data: {
                            id: jobId,
                        },
                        // Callback handler that will be called on success
                        success: function(response, textStatus, jqXHR) {
                            var result = response.result;
                            $("#checkIn").prop("disabled", true);
                            if (result.msg == true) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'เช็คอินสำเร็จ',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'พบข้อผิดพลาด',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                        },
                        // Callback handler that will be called on failure
                        error: function(jqXHR, textStatus, errorThrown) {
                            // Log the error to the console
                            console.error(
                                "The following error occurred: " +
                                textStatus, errorThrown
                            );
                        }
                    });
                }
            })
        });
        $("#checkOut").click(function() {
            Swal.fire({
                title: 'กดยืนยันเพื่อทำการเช็คเอ้าท์',
                text: "",
                icon: 'warning',
                position: 'center',
                showCancelButton: 'true',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'ยกเลิก',
                confirmButtonText: 'ยืนยัน'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'check_out')); ?>",
                        type: "post",
                        data: {
                            id: jobId,
                        },
                        // Callback handler that will be called on success
                        success: function(response, textStatus, jqXHR) {
                            var result = response.result;
                            // console.log(result);
                            $("#checkOut").prop("disabled", true);
                            if (result.msg == true) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'เช็คเอ้าท์สำเร็จ',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'พบข้อผิดพลาด',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                        },
                        // Callback handler that will be called on failure
                        error: function(jqXHR, textStatus, errorThrown) {
                            // Log the error to the console
                            console.error(
                                "The following error occurred: " +
                                textStatus, errorThrown
                            );
                        }
                    });
                }
            })
        });
        // $('#photo_tmp').change(function() {
        //     window.location.replace("<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'save_photo')); ?>" + '/' + jobId);
        // });
        $("#save").click(function() {
            var form_data = new FormData();
            console.log($('#photo_tmp')[0]);
            form_data.append('file', $('#photo_tmp')[0]);
            $.ajax({
                url: "<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'save_photo')); ?>",
                type: "post",
                data: form_data,
                // Callback handler that will be called on success
                success: function(response, textStatus, jqXHR) {
                    var result = response.result;
                    // if (result.msg == true) {
                    //     Swal.fire({
                    //         position: 'center',
                    //         icon: 'success',
                    //         title: 'อัพโหลดรูปสำเร็จ',
                    //         showConfirmButton: false,
                    //         timer: 1500
                    //     })
                    // } else {
                    //     Swal.fire({
                    //         icon: 'error',
                    //         title: 'พบข้อผิดพลาด',
                    //         showConfirmButton: false,
                    //         timer: 1500
                    //     })
                    // }
                },
                // Callback handler that will be called on failure
                error: function(jqXHR, textStatus, errorThrown) {
                    // Log the error to the console
                    console.error(
                        "The following error occurred: " +
                        textStatus, errorThrown
                    );
                }
            });
        });
    });

    function checkIn() {
        // if (chkUser == 1) {
        //     window.location.replace("<?php echo $this->Html->url(array('controller' => 'Admin', 'action' => 'work_management')); ?>" + '/' + date);
        // }
        // if (chkUser == 2) {
        //     window.location.replace("<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'work_management')); ?>" + '/' + date);
        // }
        // $.ajax({
        //     url: "<?php echo $this->Html->url(array('controller' => 'Operator', 'action' => 'get_date')); ?>",
        //     type: "post",
        //     data: {
        //         value: date
        //     },
        //     // Callback handler that will be called on success
        //     success: function(response, textStatus, jqXHR) {
        //         // Log a message to the console
        //         console.log("Hooray, it worked!");
        //     },
        //     // Callback handler that will be called on failure
        //     error: function(jqXHR, textStatus, errorThrown) {
        //         // Log the error to the console
        //         console.error(
        //             "The following error occurred: " +
        //             textStatus, errorThrown
        //         );
        //     }
        // });
    }
</script>