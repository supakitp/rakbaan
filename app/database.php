<?php

/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Database configuration class.
 *
 * You can specify multiple configurations for production, development and testing.
 *
 * datasource => The name of a supported datasource; valid options are as follows:
 *  Database/Mysql - MySQL 4 & 5,
 *  Database/Sqlite - SQLite (PHP5 only),
 *  Database/Postgres - PostgreSQL 7 and higher,
 *  Database/Sqlserver - Microsoft SQL Server 2005 and higher
 *
 * You can add custom database datasources (or override existing datasources) by adding the
 * appropriate file to app/Model/Datasource/Database. Datasources should be named 'MyDatasource.php',
 *
 *
 * persistent => true / false
 * Determines whether or not the database should use a persistent connection
 *
 * host =>
 * the host you connect to the database. To add a socket or port number, use 'port' => #
 *
 * prefix =>
 * Uses the given prefix for all the tables in this database. This setting can be overridden
 * on a per-table basis with the Model::$tablePrefix property.
 *
 * schema =>
 * For Postgres/Sqlserver specifies which schema you would like to use the tables in.
 * Postgres defaults to 'public'. For Sqlserver, it defaults to empty and use
 * the connected user's default schema (typically 'dbo').
 *
 * encoding =>
 * For MySQL, Postgres specifies the character encoding to use when connecting to the
 * database. Uses database default not specified.
 *
 * unix_socket =>
 * For MySQL to connect via socket specify the `unix_socket` parameter instead of `host` and `port`
 *
 * settings =>
 * Array of key/value pairs, on connection it executes SET statements for each pair
 * For MySQL : http://dev.mysql.com/doc/refman/5.6/en/set-statement.html
 * For Postgres : http://www.postgresql.org/docs/9.2/static/sql-set.html
 * For Sql Server : http://msdn.microsoft.com/en-us/library/ms190356.aspx
 *
 * flags =>
 * A key/value array of driver specific connection options.
 */
class DATABASE_CONFIG
{

	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'user',
		'password' => 'password',
		'database' => 'database_name',
		'prefix' => '',
		//'encoding' => 'utf8',
	);

	public $test = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'user',
		'password' => 'password',
		'database' => 'test_database_name',
		'prefix' => '',
		//'encoding' => 'utf8',
	);

	public $mis = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'mis',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $educationsystem = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'educationsystem',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $travel = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'travel',
		'prefix' => '',
		'encoding' => 'utf8',
	);
	public $time = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'time',
		'prefix' => '',
		'encoding' => 'utf8',
	);
	public $thailand_db = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'thailand_db',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $e_action_plan = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'e_action_plan',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $edoc = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'edoc',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $salary_paper_requestion = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'salary_paper_requestion',
		'prefix' => '',
		'encoding' => 'utf8',
	);
	public $salary = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => '202.28.24.230',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'salary',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $academic_work_publication = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'academic_work_publication',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $reserve_car = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'reserve_car',
		'prefix' => '',
		'encoding' => 'utf8',
	);
	public $reserve_meeting_room = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'reserve_meeting_room',
		'prefix' => '',
		'encoding' => 'utf8',
	);
	public $reserve_student_lab = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'reserve_student_lab',
		'prefix' => '',
		'encoding' => 'utf8',
	);
	public $machinery = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'machinery',
		'prefix' => '',
		'encoding' => 'utf8',
	);
	public $pdo_course = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'pdo_course',
		'encoding' => 'utf8',
		'prefix' => ''
	);
	public $pdo_staff = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'pdo_staff',
		'encoding' => 'utf8',
		'prefix' => ''
	);
	public $pdo_research = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'agdb4077',
		'database' => 'pdo_research',
		'encoding' => 'utf8',
		'prefix' => ''
	);
}
