<?php
App::uses('AppController', 'Controller');
class OperatorController extends AppController
{
    public $components = array('Cookie');

    private    $title_for_layout = 'RAK BAAN HOME SERVICE Website Information, ห้างหุ้นส่วนจำกัด รักษ์บ้าน';
    private    $keywords = 'RAK BAAN HOME SERVICE Website Information';
    private    $keywordsTh = 'ห้างหุ้นส่วนจำกัด รักษ์บ้าน';
    private    $description = 'ห้างหุ้นส่วนจำกัด รักษ์บ้าน';

    private $taxPercent = 7;

    public $uses = array('User', 'Job', 'Item', 'ItemType', 'JobStatus', 'JobImg', 'JobItem');

    public $layout = 'Main';

    public function beforeFilter()
    {

        $this->set(array(
            'title_for_layout' => $this->title_for_layout,
            'keywords' => $this->keywords,
            'keywordsTh' => $this->keywordsTh,
            'description' => $this->description
        ));

        date_default_timezone_set('Asia/Bangkok');
    }

    public function afterFilter()
    {
        $alertType = $this->Session->read('alertType');
        if (isset($alertType)) {
            $this->Session->delete('alertType');
        }
    }
    function resizeImage($newWidth, $targetFile, $originalFile)
    {
        $info = getimagesize($originalFile);
        $mime = $info['mime'];

        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw new Exception('Unknown image type.');
        }

        $img = $image_create_func($originalFile);
        list($width, $height) = getimagesize($originalFile);

        $newHeight = ($height / $width) * $newWidth;
        $tmp = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        if (file_exists($targetFile)) {
            unlink($targetFile);
        }
        //$image_save_func($tmp, "$targetFile.$new_image_ext");
        $image_save_func($tmp, "$targetFile");
    }

    //convert date to default format
    public function DateEng($datetime)
    {
        $arrDate = explode("/", $datetime);
        $strYear = $arrDate['2'] - 543; // ปี
        $strMonth = $arrDate['1']; // เดือน
        $strDay = $arrDate['0']; // วันที่
        return "$strYear-$strMonth-$strDay";
    }
    //convert date to Thai format
    public function DateThai($datetime)
    {
        $arrDate = explode("-", $datetime);
        $strYear = $arrDate['0'] + 543; // ปี
        $strMonth = $arrDate['1']; // เดือน
        $strDay = $arrDate['2']; // วันที่
        return "$strDay/$strMonth/$strYear";
    }

    public function index()
    {
        //$this->redirect(array('controller' => 'Main', 'action' => 'member'));

        /*
		$api = new ApiCmu();
		debug($api->getPersonalInfo('1529900445442'));
		*/
    }
    public function save_photo()
    {

        debug($this->request->data);
        exit;

        // $jobId = $this->request->data['id'];
        // $Jobs = $this->Job->find('first', array('conditions' => array('Job.id' => $jobId)));
        // $msg = "";
        debug($this->request->data);
        exit;

        // if ($this->request->is(array('ajax'))) {
        //     // the order of these three lines is very important !!!
        //     $resultJ = json_encode(array('result' => array(
        //         'msg' => $msg,
        //     )));
        //     $this->response->type('json');
        //     $this->response->body($resultJ);
        //     return $this->response;
        // }
    }
    public function check_in()
    {
        $id = $this->request->data['id'];
        if ($this->request->data) {
            $user = $this->Session->read('loginUser');
            $this->request->data['Job']['id'] =  $id;
            if (!empty($user)) {
                $this->request->data['Job']['check_in_user'] =  $user['User']['id'];
            }
            $this->request->data['Job']['check_in'] =  date("Y-m-d H:i:s");
            // $this->request->data['Job']['job_status_id'] = 2;
            if ($this->Job->save($this->request->data)) {
                $msg = true;
            } else {
                $msg = false;
            }
        }
        if ($this->request->is(array('ajax'))) {
            // the order of these three lines is very important !!!
            $resultJ = json_encode(array('result' => array(
                'msg' => $msg,
            )));
            $this->response->type('json');
            $this->response->body($resultJ);
            return $this->response;
        }
    }
    public function check_out()
    {
        $id = $this->request->data['id'];
     
        if ($this->request->data) {
            $user = $this->Session->read('loginUser');
            $this->request->data['Job']['id'] =  $id;
            if (!empty($user)) {
                $this->request->data['Job']['check_out_user'] =  $user['User']['id'];
            }
            $this->request->data['Job']['check_out'] =  date("Y-m-d H:i:s");
            // $this->request->data['Job']['job_status_id'] = 3;
            if ($this->Job->save($this->request->data)) {
                $msg = true;
            } else {
                $msg = false;
            }
            // $data = $this->request->data;
        }
  
        if ($this->request->is(array('ajax'))) {
            // the order of these three lines is very important !!!
            $resultJ = json_encode(array('result' => array(
                'msg' => $msg,
                // 'data' => $data
            )));
            $this->response->type('json');
            $this->response->body($resultJ);
            return $this->response;
        }
    }
    public function work_management($date = null)
    {
        $aryCondition = array();
        if ($this->request->data) {
            if (isset($this->request->data['status']['job_status_id']) && $this->request->data['status']['job_status_id'] != '') {
                $aryCondition += array('Job.job_status_id' => $this->request->data['status']['job_status_id']);
            }
        }

        $jobStatuses = $this->JobStatus->find('list', array(
            'fields' => array('id', 'name'),
            'conditions' => array()
        ));
        $this->set(compact('jobStatuses'));

        if ($date == null) {
            $dateFromSession = $this->Session->read('adminCurDate');
            if ($dateFromSession != null) {
                $date = $dateFromSession;
            } else {
                $date = date("Y-m-d");
            }
        }
        $this->set(compact('date'));
        $this->Session->write('adminCurDate', $date);

        $jobs = $this->Job->find('all', array(
            'conditions' => array(
                'Job.active' => '1',
                'Job.job_status_id' => array('2', '3'),
                'Job.job_date' => $date,
                $aryCondition
            ),
            'order' => 'start_time ASC'
        ));
        $this->set(compact('jobs'));

        $dateShow = $this->DateThai($date);
        $this->set(compact('dateShow'));
    }
    public function deleteImg($id = null)
    {

        $jobImg = $this->JobImg->findById($id);

        $jobId = $jobImg['JobImg']['job_id'];

        $file = new File(WWW_ROOT . 'img/job_img/' . $jobImg['JobImg']['img']);
        if ($file->exists()) {
            $file->delete();
        }
        if ($this->JobImg->delete($id)) {
            $this->Session->write('alertType', 'success');
            $this->Session->setFlash(' ลบรูปสำเร็จ ');
            $this->redirect(array('controller' => 'Operator', 'action' => 'work_detail', $jobId));
        }
    }
    public function work_detail($id = null)
    {
        $jobs = $this->Job->findByid($id);
        $this->set(compact('jobs'));
        $timeFormat = date('H:i', strtotime($jobs['Job']['start_time'])) . ' - ' . date('H:i', strtotime($jobs['Job']['end_time']));
        $this->set(compact('timeFormat'));
        $dateThai = $this->DateThai($jobs['Job']['job_date']);
        $this->set(compact('dateThai'));
        $itemType = $this->ItemType->find('first', array('conditions' => array('ItemType.id' => $jobs['Job']['item_type_id'])));
        $this->set(compact('itemType'));
        $jobImgs2 = $this->JobImg->find('all', array('conditions' => array('job_id' => $id, 'img_type_id' => 2)));
        $this->set(compact('jobImgs2'));
        $jobImgs3 = $this->JobImg->find('all', array('conditions' => array('job_id' => $id, 'img_type_id' => 3)));
        $this->set(compact('jobImgs3'));
        $msg = '';
        if ($this->request->data) {
            if (!empty($this->request->data['btn'])) {
                if ($this->request->data['btn'] == 1) {
                    foreach ($this->request->data['Job']['photo_tmp1'] as $index => $img) {
                        $folderToSaveFiles = WWW_ROOT . 'img/job_img/';
                        $ext = pathinfo($img['name'], PATHINFO_EXTENSION);
                        $arr_ext = array('jpg', 'jpeg', 'gif', 'png', 'JPG', 'JPEG', 'GIF', 'PNG');
                        //check file type
                        if (in_array($ext, $arr_ext)) {

                            $newJobImg = $this->JobImg->findById(0);
                            $newJobImg['JobImg']['job_id'] = $id;
                            $newJobImg['JobImg']['img_type_id'] = 2;
                            //$newJobImg['JobImg']['img'] = $newName;
                            $this->JobImg->clear();
                            $jobImgId = $this->JobImg->getLastInsertId();
                            $newName = $jobs['Job']['item_type_id'] . '/' . $id . '_' . $jobImgId . '.' . $ext;
                            if (move_uploaded_file($img['tmp_name'], $folderToSaveFiles . $newName)) {
                                $this->resizeImage(800, $folderToSaveFiles . $newName, $folderToSaveFiles . $newName);
                                $newJobImg['JobImg']['img'] = $newName;
                                $newJobImg['JobImg']['id'] = $jobImgId;
                                //$this->JobImg->clear();
                                if ($this->JobImg->save($newJobImg)) {
                                    $msg = true;
                                } else {
                                    $msg = false;
                                }
                                // $this->request->data['MisEmployee']['photo'] = $newName;
                                // $this->MisEmployee->save($this->request->data);
                            }
                        }
                    }
                } else {

                    foreach ($this->request->data['Job']['photo_tmp2'] as $index => $img) {
                        $folderToSaveFiles = WWW_ROOT . 'img/job_img/';
                        $ext = pathinfo($img['name'], PATHINFO_EXTENSION);
                        $arr_ext = array('jpg', 'jpeg', 'gif', 'png', 'JPG', 'JPEG', 'GIF', 'PNG');

                        //check file type
                        if (in_array($ext, $arr_ext)) {

                            $newJobImg = $this->JobImg->findById(0);
                            $newJobImg['JobImg']['job_id'] = $id;
                            $newJobImg['JobImg']['img_type_id'] = 3;
                            //$newJobImg['JobImg']['img'] = $newName;
                            $this->JobImg->clear();
                            $this->JobImg->save($newJobImg);
                            $jobImgId = $this->JobImg->getLastInsertId();
                            $newName =  $jobs['Job']['item_type_id'] . '_' . $id . '_' . $jobImgId . '.' . $ext;
                            if (move_uploaded_file($img['tmp_name'], $folderToSaveFiles . $newName)) {
                                $this->resizeImage(800, $folderToSaveFiles . $newName, $folderToSaveFiles . $newName);
                                $newJobImg['JobImg']['img'] = $newName;
                                $newJobImg['JobImg']['id'] = $jobImgId;
                                //$this->JobImg->clear();
                                if ($this->JobImg->save($newJobImg)) {
                                    $msg = true;
                                } else {
                                    $msg = false;
                                }
                                // $this->request->data['MisEmployee']['photo'] = $newName;
                                // $this->MisEmployee->save($this->request->data);
                            }
                        }
                    }
                }
            }
            if ($msg == true) {
                $this->Session->write('alertType', 'success');
                $this->Session->setFlash(' อัพโหลดรูปสำเร็จ ');
                // $this->redirect(array('controller' => 'admin', 'action' => 'add_item', $jobId));
                $this->redirect(array('controller' => 'Operator', 'action' => 'work_detail', $id));
            } else {
                $this->Session->write('alertType', 'warning');
                $this->Session->setFlash(' พบข้อผิดพลาด ');
                // $this->redirect(array('controller' => 'admin', 'action' => 'add_item', $jobId));
                $this->redirect(array('controller' => 'Operator', 'action' => 'work_detail', $id));
            }
        }
    }
    public function load_data()
    {
        $this->layout = '';
        $json_data = array();
        $arr_color_demo = array(
            "1" => "#ffd149",
            "2" => "#fa42a2",
            "3" => "#61c419",
            "4" => "#ff8e25",
            "5" => "#44c5ed",
            "6" => "#ca5ec9",
            "7" => "#ff0000"
        );

        $arr_events = array();
        $key = null;
        for ($i = 1; $i <= 7; $i++) {
            $demo_start_date = date("Y-m-01");
            if (is_null($key)) {
                $key = 0;
            } else {
                $key++;
            }
            $json_data[$key] = array(
                "id" => $i,
                "groupId" => '',
                "start" => '',
                "title" => "test",
                "url" =>  'Operator/dashboard',
                "textColor" => "#FFFFFF",
                "backgroundColor" => $arr_color_demo[$i],
                "borderColor" => "transparent",
            );
            if ($i == 2) {
                $json_data[$key]["end"] = date("Y-m-d", strtotime($demo_start_date . " +3 day"));
            }
            if ($i == 3) {
                $json_data[$key]["start"] = date("Y-m-d", strtotime($demo_start_date . " +4 day"));
            }
            if ($i == 4) {
                $json_data[$key]["start"] = date("Y-m-d", strtotime($demo_start_date . " +6 day"));
                $json_data[$key]["end"] = date("Y-m-d", strtotime($demo_start_date . " +6 day"));
            }
            if ($i == 5) {
                $json_data[$key]["start"] = date("Y-m-d", strtotime($demo_start_date . " +7 day"));
                $json_data[$key]["end"] = date("Y-m-d", strtotime($demo_start_date . " +9 day"));
            }
        }


        if (isset($json_data)) {
            $json = json_encode($json_data);

            if (isset($_GET['callback']) && $_GET['callback'] != "") {
                echo $_GET['callback'] . "(" . $json . ");";
            } else {
                echo $json;
            }
        }
        // // แปลง array เป็นรูปแบบ json string  
        // if ($this->request->is(array('ajax'))) {
        //     // the order of these three lines is very important !!!
        //     $resultJ = json_encode(array('result' => array(
        //         'item' => $json_data,
        //     )));

        //     $this->response->type('json');
        //     $this->response->body($resultJ);
        //     return $this->response;
        // }
    }
    public function dashboard()
    {
    }
    public function edit_work()
    {
    }
} // Class
