<?php
App::uses('AppController', 'Controller');

class MainController extends AppController
{
	public $components = array('Cookie');

	private	$title_for_layout = 'RAK BAAN HOME SERVICE Website Information, ห้างหุ้นส่วนจำกัด รักษ์บ้าน';
	private	$keywords = 'RAK BAAN HOME SERVICE Website Information';
	private	$keywordsTh = 'ห้างหุ้นส่วนจำกัด รักษ์บ้าน';
	private	$description = 'รับซ่อมแซมบ้าน เชียงใหม่, ซ่อมแซน, บ้าน, เชียงใหม่, ช่าง, รักบ้าน, รักษ์บ้าน, ติดตั้ง, ไฟฟ้า, ประปา, แอร์, โฮมเซอร์วิส, เซอร์วิส, รับซ่อมแซมบ้าน, ซ่อมแซมบ้าน';

	private $encrypt_key = 'rakbaanNMN23165635654*lloaugoishx/-';

	public $uses = array('User', 'JobFromWeb', 'ItemType', 'Item', 'WorkPhoto', 'SlidePhoto', 'SubItemType', 'ServiceArea');

	public $layout = 'Main';

	public function beforeFilter()
	{

		$this->set(array(
			'title_for_layout' => $this->title_for_layout,
			'keywords' => $this->keywords,
			'keywordsTh' => $this->keywordsTh,
			'description' => $this->description
		));

		date_default_timezone_set('Asia/Bangkok');




		/*-$ck_user = $this->Cookie->read('ck_user');
		if(isset($ck_user))
		{
			debug($ck_user);
		}-*/
	}

	public function afterFilter()
	{
		$alertType = $this->Session->read('alertType');
		if (isset($alertType)) {
			$this->Session->delete('alertType');
		}
	}

	//convert date to default format
	public function DateEng($datetime)
	{
		$arrDate = explode("/", $datetime);
		$strYear = $arrDate['2'] - 543; // ปี
		$strMonth = $arrDate['1']; // เดือน
		$strDay = $arrDate['0']; // วันที่
		return "$strYear-$strMonth-$strDay";
	}
	//convert date to Thai format
	public function DateThai($datetime)
	{
		$arrDate = explode("-", $datetime);
		$strYear = $arrDate['0'] + 543; // ปี
		$strMonth = $arrDate['1']; // เดือน
		$strDay = $arrDate['2']; // วันที่
		return "$strDay/$strMonth/$strYear";
	}

	public function login()
	{
		$this->layout = '';

		$check_ss = $this->Session->read('loginUser');

		if (isset($check_ss)) {
			$this->redirect(array('controller' => 'Admin', 'action' => 'index'));
		} else {
			//get cookie
			$rbCookie = $this->Cookie->read('rakbaan_cookie');
			if (isset($rbCookie) && $rbCookie != null) {
				$check_user = $this->User->find('first', array(
					'conditions' =>
					array(
						'User.login_token' => $rbCookie
					)
				));
				if (count($check_user) > 0) {
					$this->Session->write('loginUser', $check_user);
					$this->redirect(array('controller' => 'Admin', 'action' => 'index'));
				}
			}
		}

		if ($this->request->is('post')) {

			$get_user = $this->User->find('first', array('conditions' => array('User.username' => $this->request->data['User']['username'], 'User.password' => $this->request->data['User']['password'])));

			if ($get_user) {
				if ($get_user['User']['login_token'] == null) {
					$get_user['User']['login_token'] = md5($this->encrypt_key . $get_user['User']['id']);
					$this->User->save($get_user);
				}

				$this->Cookie->write('rakbaan_cookie', $get_user['User']['login_token'], true, '1 year');

				$this->Session->write('loginUser', $get_user);
				$this->redirect(array('controller' => 'Admin', 'action' => 'index'));
			} else {
				$this->Session->write('alertType', 'Warning');
				$this->Session->setFlash('Username หรือ Password ผิดพลาด');
				$this->redirect(array('controller' => 'Main', 'action' => 'index'));
			}
		}
		//$this->redirect(array('controller' => 'Main', 'action' => 'member'));


	}

	public function logout()
	{
		$this->layout = '';

		$this->Cookie->delete('rakbaan_cookie');

		$this->Session->delete('loginUser');
		$this->redirect(array('controller' => 'Main', 'action' => 'index'));
		//$this->redirect(array('controller' => 'Main', 'action' => 'member'));

		/*
		$api = new ApiCmu();
		debug($api->getPersonalInfo('1529900445442'));
		*/
	}

	public function index()
	{
		$get_item_types = $this->ItemType->find('all', array(
			'conditions' => array('ItemType.active' => 1),
		));

		$slides = $this->SlidePhoto->find('all', array(
			'order' => 'id DESC'
		));
		$this->set(compact('slides'));

		$isIndex = 1;
		$this->set(compact('isIndex', 'get_item_types'));

		$serviceAreas = $this->ServiceArea->find('list', array(
			'fields' => array('id', 'full_name')
		));
		$this->set(compact('serviceAreas'));

		if ($this->request->data) {
			if ($this->JobFromWeb->save($this->request->data['JobFromWeb'])) {
				$this->Session->write('alertType', 'success');
				$this->Session->setFlash(' ทางทีมงานจะรีบติดต่อกลับโดยเร็วที่สุด ');
				$this->redirect(array('controller' => 'main', 'action' => 'index'));
			}
		}
	}

	public function gallery_jobs($item_type_id = null)
	{
		$get_item_type = $this->ItemType->find('first', array('conditions' => array('ItemType.id' => $item_type_id)));

		$workPhotos = $this->WorkPhoto->find('all', array(
			'conditions' => array('WorkPhoto.item_type_id' => $item_type_id),
			'order' => 'id DESC'
		));

		$this->set(compact('get_item_type', 'workPhotos'));
	}

	public function service($item_type_id = null)
	{
		$get_item_type = $this->ItemType->find('first', array('conditions' => array('ItemType.id' => $item_type_id)));

		$get_sub_item_types = $this->SubItemType->find('all', array('conditions' => array('SubItemType.item_type_id' => $item_type_id)));

		$output = '';
		$temp_detail = '';
		$temp_note = '';

		foreach ($get_sub_item_types as $get_sub_item_type) {

			$items = $this->Item->find('all', array('conditions' => array('Item.sub_item_type_id' => $get_sub_item_type['SubItemType']['id'])));
			foreach ($items as $item) {
				if ($item_type_id == 2 and $item['Item']['id'] >= 16 and $item['Item']['id'] < 28) {
					$temp_detail .= $item['Item']['detail'] . '<br>';
					$temp_note .= $item['Item']['note'] . '<br>';
				}
			}
		}


		foreach ($get_sub_item_types as $get_sub_item_type) {
			$output .= '<tr><td colspan="4">' . $get_sub_item_type['SubItemType']['name'] . '</td></tr>';
			$items = $this->Item->find('all', array('conditions' => array('Item.sub_item_type_id' => $get_sub_item_type['SubItemType']['id'])));
			foreach ($items as $item) {
				$output .= '<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-' . $item['Item']['name'] . '</td>';
				$output .= '	<td>' . number_format($item['Item']['price']) . ' -</td>';

				if ($item_type_id == 2) {
					if ($item['Item']['id'] == 16) {
						$output .= '	<td  rowspan="12" style="">' . $temp_detail . '</td>';
						$output .= '	<td  rowspan="12">' . $temp_note . '</td>';
					} elseif ($item['Item']['id'] > 27) {
						$output .= '	<td>' . $item['Item']['detail'] . '</td>';
						$output .= '	<td>' . $item['Item']['note'] . '</td>';
					}
				} else {
					$output .= '	<td>' . $item['Item']['detail'] . '</td>';
					$output .= '	<td>' . $item['Item']['note'] . '</td>';
				}



				$output .= '</tr>';
			}
		}



		$this->set(compact('get_item_type', 'get_sub_item_types', 'output'));
	}
	public function test()
	{
		$this->layout = '';
		//$this->redirect(array('controller' => 'Main', 'action' => 'member'));

		/*
		$api = new ApiCmu();
		debug($api->getPersonalInfo('1529900445442'));
		*/
	}
} // Class
