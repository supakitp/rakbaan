<?php
App::uses('AppController', 'Controller');
App::uses('File', 'Utility');
class AdminController extends AppController
{
	public $components = array('Cookie');

	private	$title_for_layout = 'RAK BAAN HOME SERVICE Website Information, ห้างหุ้นส่วนจำกัด รักษ์บ้าน';
	private	$keywords = 'RAK BAAN HOME SERVICE Website Information';
	private	$keywordsTh = 'ห้างหุ้นส่วนจำกัด รักษ์บ้าน';
	private	$description = 'ห้างหุ้นส่วนจำกัด รักษ์บ้าน';

	private $taxPercent = 7;

	public $uses = array(
		'User', 'JobFromWeb', 'Job', 'Item', 'ItemType', 'JobStatus', 'JobImg', 'JobItem',
		'WorkPhoto', 'SlidePhoto', 'UserType', 'SubItemType', 'ServiceArea', 'Customer'
	);

	public $layout = 'Main';

	public function beforeFilter()
	{

		$this->set(array(
			'title_for_layout' => $this->title_for_layout,
			'keywords' => $this->keywords,
			'keywordsTh' => $this->keywordsTh,
			'description' => $this->description
		));

		date_default_timezone_set('Asia/Bangkok');

		$loginUser = $this->Session->read('loginUser');
		//debug($loginUser);
		if ($loginUser == null) {
			$this->Session->write('alertType', 'danger');
			$this->Session->setFlash(' โปรดทำการ Login ');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
		}
	}

	public function afterFilter()
	{
		$alertType = $this->Session->read('alertType');
		if (isset($alertType)) {
			$this->Session->delete('alertType');
		}
	}

	//convert date to default format
	public function DateEng($datetime)
	{
		$arrDate = explode("/", $datetime);
		$strYear = $arrDate['2'] - 543; // ปี
		$strMonth = $arrDate['1']; // เดือน
		$strDay = $arrDate['0']; // วันที่
		return "$strYear-$strMonth-$strDay";
	}
	//convert date to Thai format
	public function DateThai($datetime)
	{
		$arrDate = explode("-", $datetime);
		$strYear = $arrDate['0'] + 543; // ปี
		$strMonth = $arrDate['1']; // เดือน
		$strDay = $arrDate['2']; // วันที่
		return "$strDay/$strMonth/$strYear";
	}

	function resizeImage($newWidth, $targetFile, $originalFile)
	{
		$info = getimagesize($originalFile);
		$mime = $info['mime'];

		switch ($mime) {
			case 'image/jpeg':
				$image_create_func = 'imagecreatefromjpeg';
				$image_save_func = 'imagejpeg';
				$new_image_ext = 'jpg';
				break;

			case 'image/png':
				$image_create_func = 'imagecreatefrompng';
				$image_save_func = 'imagepng';
				$new_image_ext = 'png';
				break;

			case 'image/gif':
				$image_create_func = 'imagecreatefromgif';
				$image_save_func = 'imagegif';
				$new_image_ext = 'gif';
				break;

			default:
				throw new Exception('Unknown image type.');
		}

		$img = $image_create_func($originalFile);
		list($width, $height) = getimagesize($originalFile);

		$newHeight = ($height / $width) * $newWidth;
		$tmp = imagecreatetruecolor($newWidth, $newHeight);
		imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

		if (file_exists($targetFile)) {
			unlink($targetFile);
		}
		//$image_save_func($tmp, "$targetFile.$new_image_ext");
		$image_save_func($tmp, "$targetFile");
	}
	function timeToSeconds($time = null)
	{
		$arr = explode(':', $time);
		if (count($arr) == 3) {
			return $arr[0] * 3600 + $arr[1] * 60 + $arr[2];
		} else {
			return $arr[0] * 60 + $arr[1];
		}
	}

	public function registor()
	{
		if ($this->request->is('post')) {
			$this->request->data['User']['active'] = 1;
			$this->User->save($this->request->data);
			$this->redirect(array('controller' => 'admin', 'action' => 'list_users'));
		}
	}

	public function list_users()
	{
		$users = $this->User->find('all', array('conditions' => array(), 'order' => 'User.id DESC'));
		$this->set(compact('users'));

		if ($this->request->is('post')) {

			$this->User->save($this->request->data);
			$this->redirect(array('controller' => 'admin', 'action' => 'list_users'));
		}
	} // fn list_users

	public function delete_user($id = null)
	{

		$this->User->delete($id);
		$this->redirect(array('controller' => 'admin', 'action' => 'list_users'));
	} // fn delete_user

	public function index()
	{
		$this->Session->delete('adminCurDate');
		//$this->redirect(array('controller' => 'Main', 'action' => 'member'));

		/*
		$api = new ApiCmu();
		debug($api->getPersonalInfo('1529900445442'));
		*/

		// $this->redirect(array('controller' => 'admin', 'action' => 'work_management'));
		$this->clearOldPhoto();
	}

	public function work_management($date = null)
	{
		$aryCondition = array();
		if ($this->request->data) {
			if (isset($this->request->data['status']['job_status_id']) && $this->request->data['status']['job_status_id'] != '') {
				$aryCondition += array('Job.job_status_id' => $this->request->data['status']['job_status_id']);
			}
		}

		$jobStatuses = $this->JobStatus->find('list', array(
			'fields' => array('id', 'name')
		));
		$this->set(compact('jobStatuses'));

		if ($date == null) {
			$dateFromSession = $this->Session->read('adminCurDate');
			if ($dateFromSession != null) {
				$date = $dateFromSession;
			} else {
				$date = date("Y-m-d");
			}
		}
		$this->set(compact('date'));
		$this->Session->write('adminCurDate', $date);

		$jobs = $this->Job->find('all', array(
			'conditions' => array(
				'Job.active' => '1',
				'Job.job_date' => $date,
				$aryCondition
			),
			'order' => 'start_time ASC'
		));
		$this->set(compact('jobs'));

		$dateShow = $this->DateThai($date);
		$this->set(compact('dateShow'));
	}
	public function load_event()
	{
		$this->layout = '';
		$json_data = array();;
		$arr_events = array();
		$key = null;
		$user = $this->Session->read('loginUser');
		if ($user['User']['user_type_id'] == 2) {
			$jobs = $this->Job->find('all', array(
				'conditions' => array(
					'job_status_id' => array('2', '3'),
					'engineer_id' => $user['User']['id'],
				),
				'order' => 'start_time ASC'
			));
		} else {
			$jobs = $this->Job->find('all', array(
				'conditions' => array(
					'Job.active' => '1',
				),
				'order' => 'start_time ASC'
			));
		}
		$this->set(compact('jobs'));
		foreach ($jobs as $job) {
			$demo_start_date = date("Y-m-01");
			if (is_null($key)) {
				$key = 0;
			} else {
				$key++;
			}
			if ($job['Job']['job_status_id'] == 1) {
				$sColor = 'black';
			} elseif ($job['Job']['job_status_id'] == 2) {
				$sColor = 'red';
			} elseif ($job['Job']['job_status_id'] == 3) {
				$sColor = 'green';
			} elseif ($job['Job']['job_status_id'] == 4) {
				$sColor = 'lightgray';
			}
			$json_data[$key] = array(
				"id" => $job['Job']['id'],
				"groupId" => '',
				"start" => '',
				"title" => "test",
				"url" =>  '',
				"textColor" => '#FFFFFF',
				"backgroundColor" => $sColor,
				"borderColor" => "transparent",
			);
			$json_data[$key]["start"] = date("Y-m-d", strtotime($job['Job']['job_date']));
			$timeFormat = date('H:i', strtotime($job['Job']['start_time'])) . ' - ' . date('H:i', strtotime($job['Job']['end_time'])) . ' | ' . substr($job['Job']['job_num'], 0, 5);
			$json_data[$key]["title"] = $timeFormat;
		}
		if (isset($json_data)) {
			$json = json_encode($json_data);

			if (isset($_GET['callback']) && $_GET['callback'] != "") {
				echo $_GET['callback'] . "(" . $json . ");";
			} else {
				echo $json;
			}
		}
	}

	public function add_job($jobId = null)
	{
		//$user = $this->Session->read('loginUser');
		$user = $this->Session->read('loginUser');
		$itemTypes = $this->ItemType->find('list', array(
			'fields' => array('id', 'name'),
			'conditions' => array(
				'active' => 1
			),
			'order' => 'order_id'
		));
		$this->set(compact('itemTypes'));

		$jobStatuses = $this->JobStatus->find('list', array(
			'fields' => array('id', 'name')
		));
		$this->set(compact('jobStatuses'));

		$engineers = $this->User->find('list', array(
			'fields' => array('id', 'name'),
			'conditions' => array('User.active' => 1, 'user_type_id' => 2)
		));
		$this->set(compact('engineers'));

		$defaultDate = $this->DateThai(date('Y-m-d'));
		$dateFromSession = $this->Session->read('adminCurDate');
		if ($dateFromSession != null) {
			$defaultDate = $this->DateThai($dateFromSession);
		}
		$this->set(compact('defaultDate'));

		$defaultTime = date('H:i');
		//$defaultTime = $this->timeToSeconds($defaultTime);
		$this->set(compact('defaultTime'));

		if ($this->request->data) {

			// time format change second to hour
			// $this->request->data['Job']['start_time'] = gmdate('H:i:s', $this->request->data['Job']['start_time']);
			// $this->request->data['Job']['end_time'] = gmdate('H:i:s', $this->request->data['Job']['end_time']);

			$this->request->data['Job']['start_time'] = date(
				"H:i:s",
				strtotime($this->request->data['Job']['start_time_h'] . ':' .
					$this->request->data['Job']['start_time_m'] . ':00')
			);
			$this->request->data['Job']['end_time'] = date(
				"H:i:s",
				strtotime($this->request->data['Job']['end_time_h'] . ':' .
					$this->request->data['Job']['end_time_m'] . ':00')
			);

			if ($this->request->data['Job']['job_date'] != '') {
				$this->request->data['Job']['job_date'] = $this->DateEng($this->request->data['Job']['job_date']);
			}

			if ($jobId == null || $jobId == 0) {
				$this->request->data['Job']['create_user']	= $user['User']['id'];

				//generate number
				$jobCount = $this->Job->find('count', array(
					'conditions' => array(
						'YEAR(Job.job_date)' => (date('Y', strtotime(
							$this->request->data['Job']['job_date']
						)))
					)
				));
				$this->request->data['Job']['job_num'] = sprintf("%05s", ($jobCount + 1)) . '/' . (date('Y', strtotime($this->request->data['Job']['job_date'])) + 543);
			} else {
				$this->request->data['Job']['update_user']	= $user['User']['id'];
			}
			if ($this->Job->save($this->request->data['Job'])) {
				if ($jobId == null || $jobId == 0) {
					$jobId = $this->Job->getLastInsertId();
					$this->request->data['Job']['id'] = $jobId;
				}

				$newJobItem = $this->JobItem->findById(0);

				$id = $this->request->data['Job']['JobItem']['id'];
				$name = $this->request->data['Job']['JobItem']['item_other'];
				$unit_price = $this->request->data['Job']['JobItem']['unit_price'];
				$qty = $this->request->data['Job']['JobItem']['qty'];
				//$price = $this->request->data['Job']['JobItem']['price'];
				$itemId = 0;
				for ($i = 0; $i < count($name); $i++) {
					if ($name[$i] != '') {
						$newJobItem['JobItem']['id'] = $id[$i];
						$newJobItem['JobItem']['item_other'] = $name[$i];
						$newJobItem['JobItem']['unit_price'] = $unit_price[$i];
						$newJobItem['JobItem']['qty'] = $qty[$i];
						$newJobItem['JobItem']['price'] =  $unit_price[$i] * $qty[$i];
						$newJobItem['JobItem']['item_id'] = $itemId;
						$newJobItem['JobItem']['job_id'] = $jobId;
						$this->JobItem->clear();
						$this->JobItem->save($newJobItem);
					}
				}

				//คำนวน vat
				$this->calTotal($jobId);

				//อัพรูป
				foreach ($this->request->data['Job']['photo_tmp'] as $index => $img) {
					$folderToSaveFiles = WWW_ROOT . 'img/job_img/';
					$ext = pathinfo($img['name'], PATHINFO_EXTENSION);
					$arr_ext = array('jpg', 'jpeg', 'gif', 'png', 'JPG', 'JPEG', 'GIF', 'PNG');

					//check file type
					if (in_array($ext, $arr_ext)) {

						$newJobImg = $this->JobImg->findById(0);
						$newJobImg['JobImg']['job_id'] = $jobId;
						//$newJobImg['JobImg']['img'] = $newName;
						$this->JobImg->clear();
						$this->JobImg->save($newJobImg);
						$jobImgId = $this->JobImg->getLastInsertId();

						$newName = 	$this->request->data['Job']['item_type_id'] . '/' . $jobId . '_' . $jobImgId . '.' . $ext;
						if (move_uploaded_file($img['tmp_name'], $folderToSaveFiles . $newName)) {
							$this->resizeImage(800, $folderToSaveFiles . $newName, $folderToSaveFiles . $newName);


							$newJobImg['JobImg']['img'] = $newName;
							$newJobImg['JobImg']['id'] = $jobImgId;
							//$this->JobImg->clear();
							$this->JobImg->save($newJobImg);
							// $this->request->data['MisEmployee']['photo'] = $newName;
							// $this->MisEmployee->save($this->request->data);
						}
					}
				}
				$this->Session->write('alertType', 'success');
				$this->Session->setFlash(' บันทึกข้อมูลสำเร็จ ');
				// $this->redirect(array('controller' => 'admin', 'action' => 'add_item', $jobId));
				$this->redirect(array('controller' => 'admin', 'action' => 'work_management'));
			}
		} else {
			if ($jobId != null || $jobId != 0) {
				$this->request->data = $this->Job->findById($jobId);
				// $this->request->data['Job']['start_time'] =  $this->timeToSeconds($this->request->data['Job']['start_time']);
				// $this->request->data['Job']['end_time'] =  $this->timeToSeconds($this->request->data['Job']['end_time']);
				if ($this->request->data['Job']['job_date'] != '') {
					$this->request->data['Job']['job_date'] = $this->DateThai($this->request->data['Job']['job_date']);
				}
				$jobImgs = $this->JobImg->find('all', array('conditions' => array('job_id' => $jobId)));
				$this->set(compact('jobImgs'));
				$jobItems = $this->JobItem->find('all', array('conditions' => array('job_id' => $jobId)));
				$this->set(compact('jobItems'));
			}
		}
	}
	public function add_item($jobId = null)
	{
		$this->set(compact('jobId'));
		$itemTypes = $this->ItemType->find('list', array(
			'fields' => array('id', 'name'),
			'conditions' => array(
				'active' => 1
			),
			'order' => 'order_id'
		));
		$this->set(compact('itemTypes'));

		$items = $this->Item->find('list', array(
			'fields' => array('id', 'name'),
			'conditions' => array(
				'active' => 1
			)
		));
		$this->set(compact('items'));
	}
	public function deleteJob($jobId = null, $date = null)
	{
		$this->Job->id = $jobId;
		$this->Job->set(array('active' => 0));

		if ($this->Job->save()) {
			$this->redirect(array('action' => 'work_management', $date));
		}
	}
	public function deleteJobItem($id = null)
	{
		$jobItem = $this->JobItem->findById($id);
		$jobId = $jobItem['JobItem']['job_id'];

		if ($this->JobItem->delete($id)) {
			if ($this->calTotal($jobId)) {
				$this->redirect(array('controller' => 'admin', 'action' => 'add_job', $jobId));
			}
		}
	}
	public function deleteImg($id = null)
	{

		$jobImg = $this->JobImg->findById($id);

		$jobId = $jobImg['JobImg']['job_id'];

		$file = new File(WWW_ROOT . 'img/job_img/' . $jobImg['JobImg']['img']);
		if ($file->exists()) {
			$file->delete();
		}

		if ($this->JobImg->delete($id)) {
			$this->redirect(array('controller' => 'admin', 'action' => 'add_job', $jobId));
		}
	}

	public function edit_work()
	{
	}
	public function print_doc($jobId = null)
	{
		$this->layout = '';

		$job = $this->Job->findById($jobId);
		if ($job['Job']['job_date'] != '') {
			$job['Job']['job_date'] = $this->DateThai($job['Job']['job_date']);
		}
		$this->set(compact('job'));

		$jobItems = $this->JobItem->find('all', array('conditions' => array('job_id' => $jobId)));
		$this->set(compact('jobItems'));

		$taxP = $this->taxPercent;
		$this->set(compact('taxP'));
	}

	public function print_receipt($jobId = null)
	{
		$this->layout = '';

		$job = $this->Job->findById($jobId);
		if ($job['Job']['job_date'] != '') {
			$job['Job']['job_date'] = $this->DateThai($job['Job']['job_date']);
		}
		$this->set(compact('job'));

		$jobItems = $this->JobItem->find('all', array('conditions' => array('job_id' => $jobId)));
		$this->set(compact('jobItems'));

		$taxP = $this->taxPercent;
		$this->set(compact('taxP'));
	}

	public function calTotal($jobId = null)
	{
		$total = 0;
		$subTotal = 0;
		$tax = 0;
		$jobItems = $this->JobItem->find('all', array('conditions' => array('job_id' => $jobId)));
		if (isset($jobItems) && count($jobItems) > 0) {
			foreach ($jobItems as $item) {
				$total += $item['JobItem']['price'];
			}

			$job = $this->Job->findById($jobId);
			$job['Job']['total'] = $total;
			if ($total > 0) {
				//คิด vat ย้อนจากสูตร
				$subTotal = $total - (($total * $this->taxPercent) / (100 + $this->taxPercent));
				$tax = $total - $subTotal;
			}
			$job['Job']['sub_total'] = $subTotal;
			$job['Job']['tax'] = $tax;
			$this->Job->save($job);

			return true;
		}

		return false;
	}

	public function work_map()
	{
		//key
		//Aoy02u0QzfeL-v1a2iiozc05VWoLxR7H0gk9gmu16GbaaAOy6eHFYSCXGWK51C_G
		//ทำ search
		//docs.microsoft.com/en-us/bingmaps/v8-web-control/map-control-concepts/search-module-examples/user-input-geocode-example
		$this->layout = '';
		$dateFromSession = $this->Session->read('adminCurDate');
		if ($dateFromSession != null) {
			$date = $dateFromSession;
		} else {
			$date = date("Y-m-d");
		}

		$jobs = $this->Job->find('all', array(
			'conditions' => array(
				'Job.active' => '1',
				'Job.job_date' => $date,
				'Job.job_status_id' => array(1, 2)
			),
			'order' => 'start_time ASC'
		));
		$this->set(compact('jobs'));
	}

	public function job_from_web()
	{
		$jobs = $this->JobFromWeb->find('all', array(
			'order' => 'JobFromWeb.id ASC'
		));

		$this->set(compact('jobs'));
	}
	public function choose_photo($itemTypeId = null)
	{
		if ($this->request->data) {
			if (isset($this->request->data['WorkPhoto']['item_type_id']) && $this->request->data['WorkPhoto']['item_type_id'] != '') {
				//gallery
				$itemTypeId = $this->request->data['WorkPhoto']['item_type_id'];

				$isUploaded = false;
				//อัพรูป
				foreach ($this->request->data['WorkPhoto']['photo_tmp'] as $img) {
					$folderToSaveFiles = WWW_ROOT . 'img/gallery/';

					//เช็ค folder
					if (!file_exists($folderToSaveFiles . '/' . $itemTypeId)) {
						mkdir($folderToSaveFiles . '/' . $itemTypeId);
					}

					$ext = pathinfo($img['name'], PATHINFO_EXTENSION);
					$arr_ext = array('jpg', 'jpeg', 'gif', 'png', 'JPG', 'JPEG', 'GIF', 'PNG');

					//check file type
					if (in_array($ext, $arr_ext)) {
						$newWorkPhoto = $this->WorkPhoto->findById(0);
						$newWorkPhoto['WorkPhoto']['item_type_id'] = $itemTypeId;
						//$newJobImg['JobImg']['img'] = $newName;
						$this->WorkPhoto->clear();
						$this->WorkPhoto->save($newWorkPhoto);
						$workPhotoId = $this->WorkPhoto->getLastInsertId();

						$newName = 	$itemTypeId . '/' . $workPhotoId . '.' .  $ext;
						if (move_uploaded_file($img['tmp_name'], $folderToSaveFiles . $newName)) {
							$this->resizeImage(800, $folderToSaveFiles . $newName, $folderToSaveFiles . $newName);


							$newWorkPhoto['WorkPhoto']['img'] = $newName;
							$newWorkPhoto['WorkPhoto']['id'] = $workPhotoId;
							//$this->JobImg->clear();
							$this->WorkPhoto->save($newWorkPhoto);
							// $this->request->data['MisEmployee']['photo'] = $newName;
							// $this->MisEmployee->save($this->request->data);
							$isUploaded = true;
						}
					}
				}

				if ($isUploaded) {
					$this->Session->write('alertType', 'success');
					$this->Session->setFlash(' บันทึกข้อมูลสำเร็จ ');
					// $this->redirect(array('controller' => 'admin', 'action' => 'add_item', $jobId));
				}
				$this->redirect(array('controller' => 'admin', 'action' => 'choose_photo', $itemTypeId));
			} else {
				//slide
				$isUploaded = false;
				//debug($this->request->data['Slide']['photo_slide']);
				foreach ($this->request->data['SlidePhoto']['photo_slide'] as $img) {
					$folderToSaveFiles = WWW_ROOT . 'img/slide/';
					$ext = pathinfo($img['name'], PATHINFO_EXTENSION);
					$arr_ext = array('jpg', 'jpeg', 'gif', 'png', 'JPG', 'JPEG', 'GIF', 'PNG');

					//check file type
					if (in_array($ext, $arr_ext)) {
						//$newJobImg['JobImg']['img'] = $newName;
						$newPhoto = $this->SlidePhoto->findById(0);
						$newPhoto['SlidePhoto']['detail'] = '';
						$this->SlidePhoto->clear();
						if ($this->SlidePhoto->save($newPhoto)) {
							$newPhotoId = $this->SlidePhoto->getLastInsertId();
							//debug($newPhotoId);
							$newName = $newPhotoId . '.' . $ext;
							if (move_uploaded_file($img['tmp_name'], $folderToSaveFiles . $newName)) {
								//$this->resizeImage(800, $folderToSaveFiles . $newName, $folderToSaveFiles . $newName);
								$newPhoto['SlidePhoto']['img'] = $newName;
								$newPhoto['SlidePhoto']['id'] = $newPhotoId;
								//$this->JobImg->clear();
								$this->SlidePhoto->save($newPhoto);
								// $this->request->data['MisEmployee']['photo'] = $newName;
								// $this->MisEmployee->save($this->request->data);
								$isUploaded = true;
							}
						}
					}
				}

				if ($isUploaded) {
					$this->Session->write('alertType', 'success');
					$this->Session->setFlash(' บันทึกข้อมูลสำเร็จ ');
					// $this->redirect(array('controller' => 'admin', 'action' => 'add_item', $jobId));
					$this->redirect(array('controller' => 'admin', 'action' => 'choose_photo', $itemTypeId));
				}
			}
		}
		if ($itemTypeId == null || $itemTypeId == 0) {
			$itemTypeId = 1;
		}
		$itemTypes = $this->ItemType->find('list', array(
			'fields' => array('id', 'name'),
			'conditions' => array(
				'active' => 1
			),
			'order' => 'order_id ASC'
		));
		$this->set(compact('itemTypes'));

		$workPhotos = $this->WorkPhoto->find('all', array(
			'conditions' => array(
				'item_type_id' => $itemTypeId
			),
			'order' => 'id DESC'
		));
		$this->set(compact('workPhotos'));

		$slides = $this->SlidePhoto->find('all', array(
			'order' => 'id DESC'
		));
		$this->set(compact('slides'));

		$this->set(compact('itemTypeId'));
	}

	public function deleteWorkPhoto($id = null)
	{
		$wImg = $this->WorkPhoto->findById($id);

		$itemTypeId = $wImg['WorkPhoto']['item_type_id'];

		$file = new File(WWW_ROOT . 'img/gallery/' . $wImg['WorkPhoto']['img']);
		if ($file->exists()) {
			$file->delete();
		}

		if ($this->WorkPhoto->delete($id)) {
			$this->redirect(array('controller' => 'admin', 'action' => 'choose_photo', $itemTypeId));
		}
	}
	public function deleteJWF($id = null)
	{
		if ($this->JobFromWeb->delete($id)) {
			$this->redirect(array('controller' => 'admin', 'action' => 'job_from_web'));
		}
	}
	public function map_jfw($id = null)
	{
		$this->layout = '';

		$job = $this->JobFromWeb->findById($id);
		$this->set(compact('job'));
	}

	public function deleteSlide($id = null)
	{
		$sImg = $this->SlidePhoto->findById($id);

		$file = new File(WWW_ROOT . 'img/slide/' . $sImg['SlidePhoto']['img']);
		if ($file->exists()) {
			$file->delete();
		}

		if ($this->SlidePhoto->delete($id)) {
			$this->redirect(array('controller' => 'admin', 'action' => 'choose_photo'));
		}
	}
	public function clearOldPhoto()
	{
		$deleteDate = date('Y-m-d H:i:s', strtotime('-30 day', time()));
		$jobImgs = $this->JobImg->find(
			'all',
			array('conditions' => array('JobImg.created <' => $deleteDate))
		);

		if (isset($jobImgs) && count($jobImgs) > 0) {
			foreach ($jobImgs as $jobImg) {
				$file = new File(WWW_ROOT . 'img/job_img/' . $jobImg['JobImg']['img']);
				if ($file->exists()) {
					$file->delete();
				}
				$this->JobImg->delete($jobImg['JobImg']['id']);
			}
		}
	}
	public function customer_search($phoneNumber = '')
	{
		$defaultYear = date('Y') + 543;

		if ($this->request->data) {
			if (isset($this->request->data['SearchCondition']) != null) {
				$defaultYear =  $this->request->data['SearchCondition']['job_year']['year'];
			}
		}

		$this->set(compact('defaultYear'));

		$aryCondition = array();
		if ($phoneNumber != null) {
			$aryCondition += array('customer_phone' => $phoneNumber);
		}

		if ($defaultYear != '') {
			$aryCondition += array('YEAR(job_date)' => $defaultYear - 543);
		}

		$jobs = $this->Job->find('all', array(
			'conditions' => array(
				'Job.active' => '1',
				$aryCondition
			),
			'limit' => 300,
			'order' => 'Job.id DESC'
		));
		$this->set(compact('jobs'));
	}

	public function manage_content()
	{
		$itemTypes = $this->ItemType->find('all', array(
			'conditions' => array('ItemType.active' => 1),
			'order' => 'id ASC'
		));
		$this->set(compact('itemTypes'));

		if ($this->request->is('post')) {

			$type_button = $this->request->data['ItemType']['button'];

			if ($type_button == 1) {
				//debug($this->request->data);
				$this->request->data['ItemType']['active'] = 1;

				$order_id = 1;
				$orderItemType = $this->ItemType->find('first', array(
					'fields' => 'MAX(ItemType.order_id) as order_id'
				));
				if (isset($orderItemType) && count($orderItemType) > 0) {
					$order_id = $orderItemType[0]['order_id'] + 1;
				}
				$this->ItemType->data['ItemType']['order_id'] = $order_id;

				$this->ItemType->save($this->request->data);

				$last_id = $this->ItemType->getLastInsertId();

				$folderToSaveFiles = WWW_ROOT . 'img/item_types/';

				$ext = pathinfo($this->request->data['ItemType']['file_raw']['name'], PATHINFO_EXTENSION);
				$result = move_uploaded_file($this->request->data['ItemType']['file_raw']['tmp_name'], $folderToSaveFiles . 'pw_' . $last_id . '.' . $ext);

				//เช็ค folder ไว้เก็บรูป
				$folderGallery = WWW_ROOT . 'img/gallery/';
				$folderJob = WWW_ROOT . 'img/job_img/';
				if (!file_exists($folderGallery . '/' . $last_id)) {
					mkdir($folderGallery . '/' . $last_id);
				}
				if (!file_exists($folderJob . '/' . $last_id)) {
					mkdir($folderJob . '/' . $last_id);
				}

				$this->ItemType->data['ItemType']['id'] = $last_id;
				$this->ItemType->data['ItemType']['photo'] = 'pw_' . $last_id . '.' . $ext;

				$this->ItemType->save($this->ItemType->data);

				$this->Session->setFlash('ทำรายการสำเร็จ !!!');
				$this->Session->write('alertType', 'success');

				$this->redirect(array('controller' => 'admin', 'action' => 'manage_content'));
			} elseif ($type_button == 2) {

				$last_id = $this->request->data['ItemType']['id'];

				if ($this->request->data['ItemType']['file_raw']['name'] != '') {
					$folderToSaveFiles = WWW_ROOT . 'img/item_types/';

					$ext = pathinfo($this->request->data['ItemType']['file_raw']['name'], PATHINFO_EXTENSION);
					$result = move_uploaded_file($this->request->data['ItemType']['file_raw']['tmp_name'], $folderToSaveFiles . 'pw_' . $last_id . '.' . $ext);
				} //files

				//เช็ค folder ไว้เก็บรูป
				$folderGallery = WWW_ROOT . 'img/gallery/';
				$folderJob = WWW_ROOT . 'img/job_img/';
				if (!file_exists($folderGallery . '/' . $last_id)) {
					mkdir($folderGallery . '/' . $last_id);
				}
				if (!file_exists($folderJob . '/' . $last_id)) {
					mkdir($folderJob . '/' . $last_id);
				}

				$this->Session->setFlash('ทำรายการสำเร็จ !!!');
				$this->Session->write('alertType', 'success');

				$this->ItemType->save($this->request->data);
				$this->redirect(array('controller' => 'admin', 'action' => 'manage_content'));
			} elseif ($type_button == 3) {
			} else {
			}
		}
	} // fn manage_content

	public function delete_item_type($id)
	{
		$this->Session->setFlash('ทำรายการสำเร็จ !!!');
		$this->Session->write('alertType', 'success');
		$this->ItemType->delete($id);
		$this->redirect(array('controller' => 'admin', 'action' => 'manage_content'));
	} // fn manage_content

	public function manage_sub_content($item_type_id = null)
	{
		$subItemTypes = $this->SubItemType->find('all', array(
			'conditions' => array('SubItemType.item_type_id' => $item_type_id),
			'order' => 'SubItemType.id ASC'
		));
		$this->set(compact('subItemTypes', 'item_type_id'));

		if ($this->request->is('post')) {

			$type_button = $this->request->data['SubItemType']['button'];

			if ($type_button == 1) {
				//debug($this->request->data);
				$this->SubItemType->save($this->request->data);
				$this->Session->setFlash('ทำรายการสำเร็จ !!!');
				$this->Session->write('alertType', 'success');
				$this->redirect(array('controller' => 'admin', 'action' => 'manage_sub_content', $item_type_id));
			} elseif ($type_button == 2) {
				$this->SubItemType->save($this->request->data);
				$this->Session->setFlash('ทำรายการสำเร็จ !!!');
				$this->Session->write('alertType', 'success');
				$this->redirect(array('controller' => 'admin', 'action' => 'manage_sub_content', $item_type_id));
			} elseif ($type_button == 3) {
			} else {
			}
		}
	} // fn manage_sub_content

	public function delete_sub_item_type($id = null, $type = null)
	{
		$this->Session->setFlash('ทำรายการสำเร็จ !!!');
		$this->Session->write('alertType', 'success');
		$this->SubItemType->delete($id);
		$this->redirect(array('controller' => 'admin', 'action' => 'manage_sub_content', $type));
	} // fn delete_sub_item_type

	public function manage_item_content($sub_item_type_id = null)
	{
		$items = $this->Item->find('all', array(
			'conditions' => array('Item.sub_item_type_id' => $sub_item_type_id),
			'order' => 'Item.id ASC'
		));
		$this->set(compact('items', 'sub_item_type_id'));

		if ($this->request->is('post')) {

			$type_button = $this->request->data['Item']['button'];

			if ($type_button == 1) {
				//debug($this->request->data);
				$this->request->data['Item']['active'] = 1;
				$this->Item->save($this->request->data);
				$this->Session->setFlash('ทำรายการสำเร็จ !!!');
				$this->Session->write('alertType', 'success');
				$this->redirect(array('controller' => 'admin', 'action' => 'manage_item_content', $sub_item_type_id));
			} elseif ($type_button == 2) {
				$this->Item->save($this->request->data);
				$this->Session->setFlash('ทำรายการสำเร็จ !!!');
				$this->Session->write('alertType', 'success');
				$this->redirect(array('controller' => 'admin', 'action' => 'manage_item_content', $sub_item_type_id));
			} elseif ($type_button == 3) {
			} else {
			}
		}
	} // fn manage_item_content

	public function delete_item($id = null, $type = null)
	{
		$this->Session->setFlash('ทำรายการสำเร็จ !!!');
		$this->Session->write('alertType', 'success');
		$this->Item->delete($id);
		$this->redirect(array('controller' => 'admin', 'action' => 'manage_item_content', $type));
	} // fn delete_sub_item_type

	public function add_content()
	{
		$valueStreet = $this->request->data['valueStreet'];
		$valueArea = $this->request->data['valueArea'];
		///edit
		if (!empty($this->request->data['id'])) {
			$id = $this->request->data['id'];
			$this->request->data['ServiceArea']['id'] = $id;
			$this->request->data['ServiceArea']['street'] = $valueStreet;
			$this->request->data['ServiceArea']['name'] = $valueArea;
			if ($this->ServiceArea->save($this->request->data)) {
				$msg = true;
			} else {
				$msg = false;
			}
		}
		//// add
		else {
			$this->request->data['ServiceArea']['name'] = $valueArea;
			$this->request->data['ServiceArea']['street'] = $valueStreet;
			if ($this->ServiceArea->save($this->request->data)) {
				$msg = true;
			} else {
				$msg = false;
			}
		}
		if ($this->request->is(array('ajax'))) {
			// the order of these three lines is very important !!!
			$resultJ = json_encode(array('result' => array(
				'msg' => $msg,
			)));
			$this->response->type('json');
			$this->response->body($resultJ);
			return $this->response;
		}
	}
	public function delete_content()
	{
		$id = $this->request->data['id'];
		if ($this->ServiceArea->delete($id)) {
			$msg = true;
		} else {
			$msg = false;
		}
		if ($this->request->is(array('ajax'))) {
			// the order of these three lines is very important !!!
			$resultJ = json_encode(array('result' => array(
				'msg' => $msg,
			)));
			$this->response->type('json');
			$this->response->body($resultJ);
			return $this->response;
		}
	}
	public function manage_area()
	{

		$Areas = $this->ServiceArea->find('all', array());
		$this->set(compact('Areas'));
	}
} // Class
