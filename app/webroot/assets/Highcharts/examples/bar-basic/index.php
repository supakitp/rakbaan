<?php extract($_REQUEST); ?>
<?

				$cateArr = explode(',',$_GET['categories']);
				for($i=0;$i<count($cateArr);$i++){
					if($cateArr[$i]!=''){
						if($i==0){
							$cate .= "'".$cateArr[$i]."'";
						}else{
							$cate .= ",'".$cateArr[$i]."'";
						}
					}
				}
				$seriesArr = explode(",",$_GET['series']);
				for($j=0;$j<count($seriesArr);$j++){
					$arrData = ${'data'.($j+1)};
					$data = explode(',',$arrData);
					if($j>0){ $text .= ','; }
					$text .=  "{
									name: '".$seriesArr[$j]."',
									data: [";
					for($k=0;$k<count($data);$k++){
						if($data[$k]!=''){
							if($k==0){
								$text .= $data[$k];
							}else{
								$text .= ','.$data[$k];
							}
						}
					}		
					$text .=				"]
								}";
					
				}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
		<title>Highcharts Example</title>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<style type="text/css">
${demo.css}
		</style>
		<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: '<?=$_GET['title']?>'
        },
       
        xAxis: {
            categories: [<?=$cate?>],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
          
            labels: {
                overflow: 'justify'
            }
        },
       colors: ['#66CC33','#0099FF', '#9900CC', '#CCCC00', '#CC66CC','#FFFF99', '#FF3300', '#FFCC00', '#33CC33', '#CC9933', '#FF66CC', '#6600FF', '#00CCCC', '#99FF99', '#669966', '#CCFF00'],
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
		credits: {
				  enabled: false
			  },
		exporting: { enabled: false },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [<?=$text?>]
    });
});
		</script>
	</head>
	<body>
<script src="../../js/highcharts.js"></script>
<script src="../../js/modules/exporting.js"></script>

<div id="container" style="min-width: <?=$_GET['min-width']?>px; height: <?=$_GET['height']?>px; max-width: <?=$_GET['max-width']?>px; margin: 0 auto"></div>


	</body>
</html>
