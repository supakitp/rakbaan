<?
	$thisarr =	unserialize(base64_decode($_GET['series']));
	//print_r($thisarr);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
		<title>Highcharts Example</title>

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<style type="text/css">
${demo.css}
		</style>
		<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column',
			backgroundColor: '#FFFFF0',
			options3d: {
                enabled: false,
     
            }
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
		lang: {
			thousandsSep: ','
		},
		exporting: { enabled: false },
					credits: {
				  enabled: false
			  },

        xAxis: {
			gridLineWidth: 2,
			labels: {
				rotation: <?=$_GET['rotation']?>,
                style: {
					fontSize: '<?=$_GET['fontsizex']?>px',
					fontWeight: 'bold'
				}
				
                
            },
            categories: [
                <?
					$ecate = explode(",",$_GET['cate']);
					for($e=0;$e<count($ecate);$e++){
						if($ecate[$e]!=''){
							if($e==0){
								echo "'".$ecate[$e]."'";
							}else{
								echo ",'".$ecate[$e]."'";
							}
							
						}
					}
					
				?>
            ]
        },
        yAxis: {
			//gridLineColor: 'black',
			gridLineWidth: 2,
            title: {
                text: '<?=$_GET['titley']?>',
				style: {
					fontSize: '17px',
					fontWeight: 'bold'
				}
				
            },
			labels: {
                style: {
					fontSize: '14px',
					fontWeight: 'bold'
				}
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px"></span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0"></td>' +
                '<td style="padding:0"><b>{point.y:,.2f} </b></td></tr>',
            footerFormat: '</table>',
            shared: false,
            useHTML: true,
			followPointer:true
        },
		colors: ['#9ccc00', '#ffb496','#6699CC','#FFCC00', '#0099FF', '#CC66CC','#FFFF99', '#FF3300', '#FFCC00', '#33CC33', '#CC9933', '#FF66CC', '#6600FF', '#00CCCC', '#99FF99', '#669966', '#CCFF00'],
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
			/*-series: {
				dataLabels: {
					enabled: true,
					align: 'right',
					rotation: <?=$_GET['rotation']?>,
					style: {
						//fontSize: '16px',
						fontWeight: 'bold'
					}
				}
			}-*/
        },
        series: [
		{
			//showInLegend: false,
            name: '����ҳ���������駧�����ҳ���',
            data: [
			<?
					$ecate = explode(",",$_GET['data3']);
					for($e=0;$e<count($ecate);$e++){
						if($ecate[$e]!=''){
							if($e==0){
								echo "".$ecate[$e]."";
							}else{
								echo ",".$ecate[$e]."";
							}
							
						}
					}
					
				?>
			]			
        },
		{
			//showInLegend: false,
            name: '�Ӣͧ�����ҳ���',
            data: [
			<?
					$ecate = explode(",",$_GET['data6']);
					for($e=0;$e<count($ecate);$e++){
						if($ecate[$e]!=''){
							if($e==0){
								echo "".$ecate[$e]."";
							}else{
								echo ",".$ecate[$e]."";
							}
							
						}
					}
					
				?>
			]			
        }]
    });
});
		</script>
	</head>
	<body>
<script src="../../js/highcharts.js"></script>
<script src="../../js/highcharts-3d.js"></script>
<script src="../../js/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; height: <?=$_GET['height']?>px; margin: 0 auto"></div>


	</body>
</html>
