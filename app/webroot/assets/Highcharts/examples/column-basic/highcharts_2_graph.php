<?
	$thisarr =	unserialize(base64_decode($_GET['series']));
	//print_r($thisarr);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
		<title>Highcharts Example</title>

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<style type="text/css">
${demo.css}
		</style>
		<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: '<?=$_GET['chartType']?>',
			backgroundColor: '#fffff0',
			options3d: {
                enabled: false,
     
            }
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
		lang: {
			thousandsSep: ','
		},
		exporting: { enabled: false },
					credits: {
				  enabled: false
			  },

        xAxis: {
			gridLineWidth: 2,
			labels: {
                rotation: <?=$_GET['rotation']?>,
				style: {
					fontSize: '<?=$_GET['fontSizeX']?>px',
					fontWeight: 'bold'
				}
                
            },
            categories: [
                <?
					$ecate = explode(",",$_GET['cate']);
					for($e=0;$e<count($ecate);$e++){
						if($ecate[$e]!=''){
							if($e==0){
								echo "'".$ecate[$e]."'";
							}else{
								echo ",'".$ecate[$e]."'";
							}
							
						}
					}
					
				?>
            ]
        },
        yAxis: {
			gridLineWidth: 2,
            title: {
                text: '<?=$_GET['titley']?>',
				style: {
					fontSize: '15px',
					fontWeight: 'bold'
				}
				
            },
			labels: {
                style: {
					fontSize: '12px',
					fontWeight: 'bold'
				}
            }
        },
		<?
		if($_GET['chartType'] != 'line'){
		?>
        tooltip: {
            /*headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.,1f} </b></td></tr>',*/
            headerFormat: '<table>',
			pointFormat:  '<td style="padding:0"><b>{point.y:,.2f} </b></td></tr>',
            footerFormat: '</table>',
            shared: false,
            useHTML: true,
			followPointer:true
        },
		<?
		}else{
		?>
		plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            },
			column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
			series: {
				dataLabels: {
					enabled: true,
					style: {
						fontSize: '17px',
						fontWeight: 'bold'
					}
				}
			}
        },
		<?
		}
		?>
		<?php			
			switch ($_GET['chart_name']) 
			{
				case "CompareReport":
					echo("colors: ['#9ccc00', '#ffb496','#ff3232','#64e682', '#ffe678', '#CC66CC','#FFFF99'
					, '#FF3300', '#FFCC00', '#33CC33', '#CC9933', '#FF66CC', '#6600FF', '#00CCCC', '#99FF99'
					, '#669966', '#CCFF00']");
					break;
				case "BudgetBalance":
					echo("colors: ['#64e682', '#ff3232','#6699CC','#FFCC00', '#0099FF', '#CC66CC','#FFFF99'
					, '#FF3300', '#FFCC00', '#33CC33', '#CC9933', '#FF66CC', '#6600FF', '#00CCCC', '#99FF99'
					, '#669966', '#CCFF00']");
					break;
				case "PlanIncome":
					echo("colors: ['#9ccc00', '#64e682','#00cc99','#FFCC00', '#0099FF', '#CC66CC','#FFFF99'
					, '#FF3300', '#FFCC00', '#33CC33', '#CC9933', '#FF66CC', '#6600FF', '#00CCCC', '#99FF99'
					, '#669966', '#CCFF00']");
					break;
				default:
					echo("colors: ['#66CC00', '#E08BF0','#6699CC','#FFCC00', '#0099FF', '#CC66CC','#FFFF99'
					, '#FF3300', '#FFCC00', '#33CC33', '#CC9933', '#FF66CC', '#6600FF', '#00CCCC', '#99FF99'
					, '#669966', '#CCFF00']");
			}
		?>
		,
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }/*,
			series: {
				dataLabels: {
					enabled: true,
					rotation: -20,
					align: 'left',
					style: {
						fontWeight: 'bold',
						fontSize: '10px'
					}
				}
			}*/
        },
        series: [{
            name: '<?=$_GET['name1']?>',
            data: [
			<?
					$ecate = explode(",",$_GET['data1']);
					for($e=0;$e<count($ecate);$e++){
						if($ecate[$e]!=''){
							if($e==0){
								echo "".$ecate[$e]."";
							}else{
								echo ",".$ecate[$e]."";
							}
							
						}
					}
					
				?>
			],
		},
		{
            name: '<?=$_GET['name2']?>',
            data: [
			<?
					$ecate = explode(",",$_GET['data2']);
					for($e=0;$e<count($ecate);$e++){
						if($ecate[$e]!=''){
							if($e==0){
								echo "".$ecate[$e]."";
							}else{
								echo ",".$ecate[$e]."";
							}
							
						}
					}
					
				?>
			]			
        }
		<?
		if($_GET['name3'] && $_GET['data3']){
		?>
		,
		{
            name: '<?=$_GET['name3']?>',
            data: [
			<?
					$ecate = explode(",",$_GET['data3']);
					for($e=0;$e<count($ecate);$e++){
						if($ecate[$e]!=''){
							if($e==0){
								echo "".$ecate[$e]."";
							}else{
								echo ",".$ecate[$e]."";
							}
							
						}
					}
					
				?>
			]			
        }
		<?
		}
		?>
		<?
		if($_GET['name4'] && $_GET['data4']){
		?>
		,
		{
            name: '<?=$_GET['name4']?>',
            data: [
			<?
					$ecate = explode(",",$_GET['data4']);
					for($e=0;$e<count($ecate);$e++){
						if($ecate[$e]!=''){
							if($e==0){
								echo "".$ecate[$e]."";
							}else{
								echo ",".$ecate[$e]."";
							}
							
						}
					}
					
				?>
			]			
        }
		<?
		}
		?>
		]
    });
});
		</script>
	</head>
	<body>
<script src="../../js/highcharts.js"></script>
<script src="../../js/highcharts-3d.js"></script>
<script src="../../js/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; height: <?=$_GET['height']?>px; margin: 0 auto"></div>


	</body>
</html>
