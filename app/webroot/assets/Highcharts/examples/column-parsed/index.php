<?

	
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
		<title>Highcharts Example</title>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<style type="text/css">
${demo.css}
		</style>
		<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        data: {
            table: document.getElementById('datatable')
        },
        chart: {
            type: 'column',
			renderTo: 'container'
        },
        title: {
            text: '<?=$_GET['title']?>'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Units'
            }
        },
		dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toLowerCase();
            }
        }
    });
});
		</script>
	</head>
	<body>
<script src="../../js/highcharts.js"></script>
<script src="../../js/modules/data.js"></script>
<script src="../../js/modules/exporting.js"></script>

<div id="container" style="min-width: <?=$_GET['min-width']?>px; height: <?=$_GET['height']?>px; max-width: <?=$_GET['max-width']?>px; margin: 0 auto"></div>

<table id="datatable" style="display:none">
	<thead>
		<tr>
			<th></th>
            <?
				$seriesArr = explode(",",$_GET['series']);
				for($i=0;$i<count($seriesArr);$i++){
			?>
			<th><?=$seriesArr[$i]?></th>
            <? } ?>
		</tr>
	</thead>
	<tbody>
    <?
		$cateArr = explode(',',$_GET['cate']);
		for($i=0;$i<count($cateArr);$i++){
	?>
		<tr>
			<th><?=$cateArr[$i]?></th>
            <?
				$seriesArr = explode(",",$_GET['series']);
				for($j=0;$j<count($seriesArr);$j++){
					$arrData = ${'data'.($j+1)};
					$data = explode(',',$arrData);
					//print_r($data);
			?>
			<td><?=$data[$i]?></td>
			<? } ?>
		</tr>
	<? } ?>
	</tbody>
</table>
	</body>
</html>
