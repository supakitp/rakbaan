<?
	$exCo = explode(',',$_GET['cate']);
	
	$exData = explode(',',$_GET['data']);
	//$i=0;
	for($k=0;$k<count($exCo);$k++){
	//$i++;
		if($exCo[$k]!=''){
		if($k>0){ $dataloop .= ','; }
		 $dataloop .= "['".$exCo[$k]."',   ".$exData[$k]."]";
		}
	}
	//echo $dataloop;
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<style type="text/css">
${demo.css}
		</style>
		<script type="text/javascript">
			$(function () {
				$('#container').highcharts({
					chart: {
						plotBackgroundColor: null,
						plotBorderWidth: null,//null,
						plotShadow: false
					},
					title: {
						text: '',
						 style: {
					display: 'none'
				}
					},
					colors: ['#66CC33','#0099FF', '#9900CC', '#CCCC00', '#CC66CC','#FFFF99', '#FF3300', '#FFCC00', '#33CC33', '#CC9933', '#FF66CC', '#6600FF', '#00CCCC', '#99FF99', '#669966', '#CCFF00'],
					tooltip: {
						pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: false,
								format: '<b>{point.name}</b>: {point.percentage:.1f} %',
								style: {
									color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
								}
							},
							legend: {
				  enabled: true,
				  labelFormatter: function() {
					var legendIndex = this.index;
					var legendName = this.series.chart.axes[0].categories[legendIndex];

					return legendName;
  }
						}
					},
					exporting: { enabled: false },
					credits: {
				  enabled: false
			  },
					series: [{
						type: 'pie',
						name: false,
						data: [
							<?=iconv("TIS-620","UTF-8",$dataloop)?>
						]
					}]
				});
			});


		</script>
	</head>
	<body>
<script src="../../js/highcharts.js"></script>
<script src="../../js/modules/exporting.js"></script>

<div id="container" style="min-width: <?=$_GET['min-width']?>px; height: <?=$_GET['height']?>px; max-width: <?=$_GET['max-width']?>px; margin: 0 auto"></div>

	</body>
</html>
