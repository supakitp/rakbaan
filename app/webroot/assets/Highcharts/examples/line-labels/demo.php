<?
		require_once("lib/nusoap.php");
        $client = new nusoap_client("http://119.59.122.107/itrend/DataService.asmx?WSDL",true); 
        $params = array(
                   'nReportId' => "2",
				   'nReportType' => "1",
				   'dFromDate' => "2015-06-01",
				   'dToDate' => "2015-06-01"
        );
       $data = $client->call("GetReportData",$params); 
	  // echo '<pre>';
       //print_r($data['GetReportDataResult']['diffgram']['NewDataSet']['Table']);
				foreach( $data['GetReportDataResult']['diffgram']['NewDataSet']['Table'] as $key => $value ){
					$times = str_replace("+07:00","",$value['InsertDate']);
					$tm = explode("T",$times);
					$InsertDateArr = explode(":",$tm[1]);
					$InsertDate = $InsertDateArr[0].":".$InsertDateArr[1];
					//echo $InsertDate;
					$arrTime[$InsertDate] = $InsertDate;
					$arrSeriesName[$value['SeriesName']][$InsertDate] = $value['Value'];
					//echo '<br>';
					
				}
				//print_r($arrSeriesName);
				//print_r($arrTime);
				$i=0;
				foreach( $arrTime as $tkey => $tvalue ){
					$i++;
					if($i==1){
						$cate = "'".$tkey."'";
					}else{
						$cate .= ",'".$tkey."'";
					}
				}
				//echo $cate.'<br>';
				$i=0;
				foreach($arrSeriesName as  $SeriesName => $val ){
					$i++;
					//print_r($val);
					if($i==1){
						$data = "{
								name: '".$SeriesName."',
								data: [";
					}else{
						$data .= ",{
								name: '".$SeriesName."',
								data: [";
					}
						$j=0;
						foreach( $arrTime as $tkey => $tvalue ){
							$j++;
							
								$vs = $val[$tkey];
							if($j==1){
								$data .= $vs;
							}else{
								$data .= ','.$vs;
							}
						}
						$data .= "]}";
				}
				
				//echo $data;			
?>
	
       
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Highcharts Example</title>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<style type="text/css">
${demo.css}
		</style>
		<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Monthly Average Temperature'
        },
        subtitle: {
            text: 'Source: WorldClimate.com'
        },
        xAxis: {
            categories: [<?=$cate?>]
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [<?=$data?>]
    });
});
		</script>
	</head>
	<body>
<script src="../../js/highcharts.js"></script>
<script src="../../js/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

	</body>
</html>
