<?
	function colorPercent($var){
		if($var<=33.33){
			echo '#DF5353';
		}else if($var>33.33 and $var<=66.66){
			echo '#DDDF0D';
		}else{
			echo '#55BF3B';
		}
	}
	if($_GET['data']!=''){
		$ex = explode(",",$_GET['data']);
		for($i=0;$i<count($ex);$i++){
			$v = explode("|",$ex[$i]);
			$value[$v[0]] = $v[1]; 
		}
	}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
		<title>Highcharts Example</title>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<style type="text/css">
${demo.css}
		</style>
		<script type="text/javascript">
$(function () {

    var gaugeOptions = {

        chart: {
            type: 'solidgauge'
        },

        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };
	
	<?
		$k=0;
		foreach( $value as $key => $var ){
			$k++;
	?>
    // The speed gauge
    $('#container-<?=$k?>').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: '<?=$key?>'
            },
			stops: [
                [<?=$var?>, '<?=colorPercent($var)?>']
				]
        },

        credits: {
            enabled: false
        },

        series: [{
            name: '<?=$key?>',
            data: [<?=$var?>]
        }]

    }));
	<?
		}
	?>




});
		</script>
	</head>
	<body>
<script src="../../js/highcharts.js"></script>
<script src="../../js/highcharts-more.js"></script>

<script src="../../js/modules/solid-gauge.src.js"></script>
<div align="center">
<?
$k=0;
		foreach( $value as $key => $var ){
			$k++;
?>
	<div id="container-<?=$k?>" style="width: 300px; height: 200px; float: left" align="center"></div>
	
<?
		}
?>
</div>


	</body>
</html>
